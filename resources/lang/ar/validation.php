<?php

return array (
  'accepted' => '다음을 수락해야합니다. attribute',
  'active_url' => ': 속성이 유효한 링크가 아닙니다',
  'after' => '속성은 date : date 이후 날짜 여야합니다.',
  'after_or_equal' => '속성 : 최신 날짜이거나 날짜 : 날짜와 일치해야합니다.',
  'alpha' => ': 속성은 문자 만 포함해야합니다',
  'alpha_dash' => '속성은 문자, 숫자 및 문자열을 포함하지 않아야합니다.',
  'alpha_num' => '속성은 문자와 숫자 만 포함해야합니다',
  'array' => '속성이어야합니다.',
  'before' => '속성은 날짜 : 날짜보다 이전 날짜 여야합니다.',
  'before_or_equal' => '속성은 이전 날짜이거나 동일한 날짜 여야합니다',
  'between' => 
  array (
    'numeric' => '속성 값은 min과 max 사이 여야합니다.',
    'file' => '파일 크기는 다음과 같아야합니다. 속성 : 최소 및 최대 KB.',
    'string' => '텍스트 문자 수는 다음과 같아야합니다. 속성 : 최소와 최대 :',
    'array' => '이 속성은 min과 max 사이에 많은 요소를 포함해야합니다.',
  ),
  'boolean' => '값 : 속성은 true 또는 false 여야합니다.',
  'confirmed' => '확인 필드가 다음 필드와 일치하지 않습니다 : 속성',
  'date' => ': 속성이 유효한 날짜가 아닙니다',
  'date_equals' => ': 속성은 date와 같은 날짜 여야합니다.',
  'date_format' => ': 속성이 형식 : 형식과 일치하지 않습니다.',
  'different' => '필드 : attribute 및 : other는 달라야합니다',
  'digits' => '속성은 숫자를 포함해야합니다.',
  'digits_between' => '포함해야합니다 : 속성 : 최소와 최대 자릿수 사이',
  'dimensions' => '속성에 잘못된 이미지 크기가 있습니다.',
  'distinct' => '필드의 경우 : 속성은 중복 값입니다.',
  'email' => '속성은 구조화 된 전자 우편 주소입니다.',
  'exists' => '선택한 값 : 속성을 찾을 수 없습니다',
  'file' => '속성은 파일이어야합니다.',
  'filled' => ': 필수 속성',
  'gt' => 
  array (
    'numeric' => 'The :attribute must be greater than :value.',
    'file' => 'The :attribute must be greater than :value kilobytes.',
    'string' => 'The :attribute must be greater than :value characters.',
    'array' => 'The :attribute must have more than :value items.',
  ),
  'gte' => 
  array (
    'numeric' => 'The :attribute must be greater than or equal :value.',
    'file' => 'The :attribute must be greater than or equal :value kilobytes.',
    'string' => 'The :attribute must be greater than or equal :value characters.',
    'array' => 'The :attribute must have :value items or more.',
  ),
  'image' => '속성은 이미지 여야합니다',
  'in' => '속성 : null',
  'in_array' => ': 속성이 존재하지 않습니다 : other.',
  'integer' => '속성은 정수 여야합니다',
  'ip' => '속성은 유효한 IP 주소 여야합니다',
  'ipv4' => '속성은 유효한 IPv4 주소 여야합니다.',
  'ipv6' => '속성은 유효한 IPv6 주소 여야합니다.',
  'json' => '속성은 JSON 텍스트 여야합니다.',
  'lt' => 
  array (
    'numeric' => 'The :attribute must be less than :value.',
    'file' => 'The :attribute must be less than :value kilobytes.',
    'string' => 'The :attribute must be less than :value characters.',
    'array' => 'The :attribute must have less than :value items.',
  ),
  'lte' => 
  array (
    'numeric' => 'The :attribute must be less than or equal :value.',
    'file' => 'The :attribute must be less than or equal :value kilobytes.',
    'string' => 'The :attribute must be less than or equal :value characters.',
    'array' => 'The :attribute must not have more than :value items.',
  ),
  'max' => 
  array (
    'numeric' => '속성 값은 max와 같거나 작아야합니다.',
    'file' => '파일 크기는 다음을 초과하지 않아야합니다. 속성 : max KB',
    'string' => '텍스트 길이 : 속성 : 최대 문자를 초과해서는 안됩니다',
    'array' => ': 속성은 max 요소 / 요소를 초과 할 수 없습니다.',
  ),
  'mimes' => '유형 :: 값의 파일이어야합니다.',
  'mimetypes' => '유형 :: 값의 파일이어야합니다.',
  'min' => 
  array (
    'numeric' => '속성 값은 min 이상이어야합니다.',
    'file' => '파일 크기는 다음과 같아야합니다. 속성 : 최소 KB',
    'string' => '텍스트 길이는 최소 : 속성 최소 문자 여야합니다.',
    'array' => '속성은 최소한 min items을 포함해야합니다.',
  ),
  'not_in' => '속성 : null',
  'not_regex' => '공식 : 잘못된 속성입니다.',
  'numeric' => '속성은 숫자 여야합니다',
  'present' => '다음을 제공해야합니다. 속성',
  'regex' => '수식 : 특성이 잘못되었습니다',
  'required' => ': 속성 필수입니다.',
  'required_if' => ': attribute 필요한 경우 : other equals : value.',
  'required_unless' => ': attribute 그렇지 않은 경우 필수 : ​​other equals : values.',
  'required_with' => ': 속성 사용 가능한 경우 필수 : ​​값.',
  'required_with_all' => ': 속성 사용 가능한 경우 필수 : ​​값.',
  'required_without' => ': attribute 필수 인 경우 : 값을 사용할 수 없습니다.',
  'required_without_all' => ': attribute 필수 인 경우 : 값을 사용할 수 없습니다.',
  'same' => ': 속성이 다음과 일치해야합니다. other',
  'size' => 
  array (
    'numeric' => '속성 값은 size와 같아야합니다.',
    'file' => '파일 크기는 다음과 같아야합니다. 속성 : size KB',
    'string' => '텍스트 : 속성은 다음을 포함해야합니다. 정확히 문자 / 문자 크기',
    'array' => '속성은 다음을 포함해야합니다 : size Exactly',
  ),
  'starts_with' => 'The :attribute must start with one of the following: :values',
  'string' => '속성 텍스트 여야합니다.',
  'timezone' => '속성은 유효한 날짜 범위 여야합니다',
  'unique' => '값 : 이미 사용 된 속성',
  'uploaded' => '속성을로드하지 못했습니다',
  'url' => '링크 형식 : 잘못된 속성',
  'uuid' => 'The :attribute must be a valid UUID.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'custom-message',
    ),
  ),
  'attributes' => 
  array (
    'name' => '이름',
    'username' => '사용자 이름',
    'email' => '이메일',
    'first_name' => '이름',
    'last_name' => '성',
    'password' => '비밀번호',
    'password_confirmation' => '비밀번호 확인',
    'city' => '도시',
    'country' => '주',
    'address' => '숙소 주소',
    'phone' => '전화',
    'mobile' => '모바일',
    'age' => '나이',
    'sex' => '섹스',
    'gender' => '타입',
    'day' => '오늘',
    'month' => '월',
    'year' => '년',
    'hour' => '시간',
    'minute' => '최소',
    'second' => '초',
    'title' => '주소',
    'content' => '내용',
    'description' => '설명',
    'excerpt' => '요약',
    'date' => '연혁',
    'time' => '시간',
    'available' => '가능',
    'size' => '사이즈',
  ),
);
