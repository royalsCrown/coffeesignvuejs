<?php

return array (
  'failed' => '이 자격 증명이 데이터와 일치하지 않습니다.',
  'throttle' => '액세스 시도가 너무 많습니다. 초 후에 다시 시도하십시오.',
);
