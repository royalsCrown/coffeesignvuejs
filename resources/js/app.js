/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import axios from 'axios'
import router from './router'
const API_URL = 'http://40.122.39.187:3000'

Vue.prototype.$http = axios
Vue.prototype.$api_url = API_URL

var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
  };
  
Vue.filter('truncate', filter)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('home', require('./components/Home.vue').default);
Vue.component('casestudy', require('./components/CaseStudy.vue').default);
Vue.component('casestudypost', require('./components/CaseStudyPost.vue').default);
Vue.component('legality', require('./components/Legality.vue').default);
Vue.component('legalitycountry', require('./components/LegalityCountry.vue').default);
Vue.component('news', require('./components/News.vue').default);
Vue.component('newspost', require('./components/NewsPost.vue').default);
Vue.component('testimonials', require('./components/Testimonials.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
