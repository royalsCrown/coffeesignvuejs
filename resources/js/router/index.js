import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import CaseStudy from '../components/CaseStudy'
import Legality from '../components/Legality'
import News from '../components/News'
import LegalityCountry from '../components/LegalityCountry'
import NewsPost from '../components/NewsPost'
import CaseStudyPost from '../components/CaseStudyPost'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/casestudy', name: 'CaseStudy', component: CaseStudy },
    { path: '/legality', name: 'Legality', component: Legality },
    { path: '/news', name: 'News', component: News },
    { path: '/legalitycountry/:id', name: 'LegalityCountry', component: LegalityCountry },
    { path: '/newspost/:id', name: 'NewsPost', component: NewsPost },
    { path: '/casestudypost/:id', name: 'CaseStudyPost', component: CaseStudyPost }
  ],
  scrollBehavior (to, from, savedPosition) {
    // return desired position, page will open at the top when clicking a route.
    return { x: 0, y: 0 }
  }
})
