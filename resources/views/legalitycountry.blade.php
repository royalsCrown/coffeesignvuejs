<!doctype html>
    <html lang="{{ app()->getLocale() }}">
        <head>
            <meta charset="utf-8">
            <!-- <base href="/"> -->
            <title>Legality</title>
            <meta name="description" content="">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <!-- Custom Browsers Color Start -->
            <meta name="theme-color" content="#000">
            <!-- Custom Browsers Color End -->
            <!-- Template Basic Images Start -->
            <meta property="og:image" content="path/to/image.jpg">
            <link rel="icon" href="static/img/favicon/favicon.ico">
            <!-- Template Basic Images End -->
            <!-- Swiper (slider) styles -->
            <link rel="stylesheet" href="static/libs/swiper/swiper.min.css">
            <!-- Swiper (slider) styles -->
            <link rel="stylesheet" href="static/css/main.min.css">
        </head>
        <body>
            <div id="app">
                <legalitycountry :routeid={{ app('request')->input('id') }}></legalitycountry>
            </div>
            <script type="text/javascript" src="js/app.js"></script>
            <script src="static/js/scripts.min.js"></script>
        </body>
    </html>