<!doctype html>
    <html lang="{{ app()->getLocale() }}">
        <head>
            <meta charset="utf-8">
            <!-- <base href="/"> -->
            <title>CaseStudy</title>
            <meta name="description" content="">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <!-- Custom Browsers Color Start -->
            <meta name="theme-color" content="#000">
            <!-- Custom Browsers Color End -->
            <!-- Template Basic Images Start -->
            <meta property="og:image" content="path/to/image.jpg">
            <link rel="icon" href="static/img/favicon/favicon.ico">
            <!-- Template Basic Images End -->
            <!-- Swiper (slider) styles -->
            <link rel="stylesheet" href="static/libs/swiper/swiper.min.css">
            <!-- Swiper (slider) styles -->
            <link rel="stylesheet" href="static/css/main.min.css">
            <!-- BootStrap-3 JS/CSS -->
            <!--
            <link rel="stylesheet" href="static/bootstrap-3/css/bootstrap.min.css">
            <script src="static/js/jquery-3.4.1.min.js"></script>
            <script src="static/bootstrap-3/js/bootstrap.min.js"></script> -->
        </head>
        <body>
            <div id="app">
                <casestudy></casestudy>
            </div>
            <script type="text/javascript" src="js/app.js"></script>
            <script src="static/js/scripts.min.js"></script>
        </body>
    </html>