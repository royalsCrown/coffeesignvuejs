@extends('backEnd.layout')

@section('headerInclude')
    <link href="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css") }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
@endsection
@section('content')
    <div class="padding">
        <div class="box">
            <div class="box-header dker">
                <h3><i class="material-icons">&#xe02e;</i> Add Tags</h3>
                <small>
                    <a href="{{ route('adminHome') }}">Home</a> /
                    <a href="">Add Tags</a>
                </small>
            </div>
            <div class="box-tool">
                <ul class="nav">
                    <li class="nav-item inline">
                        <a class="nav-link" href="{{route("Banners")}}">
                            <i class="material-icons md-18">×</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-body">
                {{Form::open(['route'=>['TagsStore'],'method'=>'POST','id'=>'myform1',  'files' => true ])}}
				
				<div id="monthly">
                @if(Helper::GeneralWebmasterSettings("ar_box_status"))
                    <div class="form-group row">
                        <label for="title_ar"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Tag Title [ Korean ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_ar','', array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_ar', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
                @if(Helper::GeneralWebmasterSettings("en_box_status"))
                    <div class="form-group row">
                        <label for="title_en"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Tag Title [ English ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_en','', array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_en', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
				@if(Helper::GeneralWebmasterSettings("jp_box_status"))
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("jp_box_status") && Helper::GeneralWebmasterSettings("jp_box_status"))  Tag Title [ Japanese ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_jp','', array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_jp', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
                 
				</div>
				
                <div class="form-group row m-t-md addcan">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary m-t" onclick="return validate()"><i class="material-icons">
                                &#xe31b;</i> {!! trans('backLang.add') !!}</button>
                        <a href="{{route("Payments")}}"
                           class="btn btn-default m-t"><i class="material-icons">
                                &#xe5cd;</i> {!! trans('backLang.cancel') !!}</a>
                    </div>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection

@section('footerInclude')

    <script src="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.js") }}"></script>
    <script>
        $(function () {
            $('.icp-auto').iconpicker({placement: '{{ (trans('backLang.direction')=="ltr")?"topLeft":"topRight" }}'});
        });
    </script>
@endsection

