@extends('backEnd.layout')
@section('headerInclude')
    <link href="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css") }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
@endsection
@section('content')
    <div class="padding">
        <div class="box">
            <div class="box-header dker">
                <h3><i class="material-icons">&#xe3c9;</i> Edit Tags</h3>
                <small>
                    <a href="{{ route('adminHome') }}">Home</a> /
                    <a href="">Edit Tags</a>
                </small>
            </div>
            <div class="box-tool">
                <ul class="nav">
                    <li class="nav-item inline">
                        <a class="nav-link" href="{{route("Tags")}}">
                            <i class="material-icons md-18">×</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-body">
                {{Form::open(['route'=>['TagsUpdate',$Banners->id],'method'=>'POST', 'files' => true])}}
				<div id="monthly">
                @if(Helper::GeneralWebmasterSettings("ar_box_status"))
                    <div class="form-group row">
                        <label for="title_ar"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Tag Title [ Korean ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_ar',$Banners->title_ar, array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_ar', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
                @if(Helper::GeneralWebmasterSettings("en_box_status"))
                    <div class="form-group row">
                        <label for="title_en"
                               class="col-sm-2 form-control-label">

                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Tag Title [ English ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_en',$Banners->title_en, array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_en', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
				@if(Helper::GeneralWebmasterSettings("jp_box_status"))
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">

                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("jp_box_status"))  Tag Title [ Japanese ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_jp',$Banners->title_jp, array('placeholder' => '','required'=>'','class' => 'form-control','id'=>'title_jp', 'dir'=>trans('backLang.ltr'))) !!}
                        </div>
                    </div>
                @endif
					
                </div>
                <div class="form-group row">
                    <label for="link_status"
                           class="col-sm-2 form-control-label">{!!  trans('backLang.status') !!}</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label class="ui-check ui-check-md">
                                {!! Form::radio('status','1',($Banners->status==1) ? true : false, array('id' => 'status1','class'=>'has-value')) !!}
                                <i class="dark-white"></i>
                                {{ trans('backLang.active') }}
                            </label>
                            &nbsp; &nbsp;
                            <label class="ui-check ui-check-md">
                                {!! Form::radio('status','0',($Banners->status==0) ? true : false, array('id' => 'status2','class'=>'has-value')) !!}
                                <i class="dark-white"></i>
                                {{ trans('backLang.notActive') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row m-t-md">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary m-t" onclick="return validate()"><i class="material-icons">
                                &#xe31b;</i> {!! trans('backLang.update') !!}</button>
                        <a href="{{route("Banners")}}"
                           class="btn btn-default m-t"><i class="material-icons">
                                &#xe5cd;</i> {!! trans('backLang.cancel') !!}</a>
                    </div>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection

@section('footerInclude')

    <script src="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.js") }}"></script>
    <script>
        $(function () {
            $('.icp-auto').iconpicker({placement: '{{ (trans('backLang.direction')=="ltr")?"topLeft":"topRight" }}'});
        });
    </script>
@endsection
