@extends('backEnd.layout')

@section('headerInclude')
    <link href="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css") }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
@endsection
@section('content')
    <div class="padding">
        <div class="box">
            <div class="box-header dker">
                <h3><i class="material-icons">&#xe02e;</i> Add Payments</h3>
                <small>
                    <a href="{{ route('adminHome') }}">Home</a> /
                    <a href="">Add Payments</a>
                </small>
            </div>
            <div class="box-tool">
                <ul class="nav">
                    <li class="nav-item inline">
                        <a class="nav-link" href="{{route("Banners")}}">
                            <i class="material-icons md-18">×</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="box-body">
                {{Form::open(['route'=>['PaymentsStore'],'method'=>'POST','id'=>'myform1',  'files' => true ])}}
				<div class="form-group row">
					<label for="title_jp"
						   class="col-sm-2 form-control-label">
						Subscription
					</label>
					<div class="col-sm-10">
						Monthly: {!! Form::checkbox('limit_period','yes',true,['onClick' => 'return checkthis(1)'], array('class' => 'form-control','id'=>'limit_period', 'dir'=>trans('backLang.ltr'))) !!}
						Anually: {!! Form::checkbox('limit_period_anual','no',false,['onClick' => 'return checkthis(2)'], array('class' => 'form-control','id'=>'limit_period_anual', 'dir'=>trans('backLang.ltr'))) !!}
					</div>
				</div>
				<div id="monthly">
                @if(Helper::GeneralWebmasterSettings("ar_box_status"))
                    <div class="form-group row">
                        <label for="title_ar"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Monthly Payment Title [ Korean ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_ar','', array('placeholder' => '','class' => 'form-control','id'=>'title_ar', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="title_ar_error">This is required.</span>
                        </div>
                    </div>
                @endif
                @if(Helper::GeneralWebmasterSettings("en_box_status"))
                    <div class="form-group row">
                        <label for="title_en"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Monthly Payment Title [ English ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_en','', array('placeholder' => '','class' => 'form-control','id'=>'title_en', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="title_en_error">This is required.</span>
                        </div>
                    </div>
                @endif
				@if(Helper::GeneralWebmasterSettings("jp_box_status"))
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("jp_box_status") && Helper::GeneralWebmasterSettings("jp_box_status"))  Monthly Payment Title [ Japanese ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('title_jp','', array('placeholder' => '','class' => 'form-control','id'=>'title_jp', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="title_jp_error">This is required.</span>
                        </div>
                    </div>
                @endif
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Monthly Price
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('price','', array('placeholder' => '','class' => 'form-control','id'=>'price', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="price_error">This is required.</span>
                        </div>
                    </div>
					
                    <div class="form-group row">
                    <label for="photo_file"
                           class="col-sm-2 form-control-label">{!!  trans('backLang.topicPhoto') !!}</label>
                    <div class="col-sm-10">
                        {!! Form::file('photo_file', array('class' => 'form-control','id'=>'photo_file','accept'=>'image/*')) !!}
                    </div>
                </div>

                <div class="form-group row m-t-md" style="margin-top: 0 !important;">
                    <div class="col-sm-offset-2 col-sm-10">
                        <small>
                            <i class="material-icons">&#xe8fd;</i>
                            {!!  trans('backLang.imagesTypes') !!}
                        </small>
                    </div>
                </div>
                
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Monthly Signature Limit [ Korea ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('limit_signature_ar','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control limit_signature_ar','id'=>'limit_signature_ar', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="limit_signature_ar_error">This is required.</span>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Monthly Signature Limit [ English ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('limit_signature_en','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control limit_signature_en','id'=>'limit_signature_en', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="limit_signature_en_error">This is required.</span>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Monthly Signature Limit [ Japanese ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('limit_signature_jp','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control limit_signature_jp','id'=>'limit_signature_jp', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="limit_signature_jp_error">This is required.</span>
                        </div>
                    </div>
				</div>
				<div id="anually" style="display:none;">
                @if(Helper::GeneralWebmasterSettings("ar_box_status"))
                    <div class="form-group row">
                        <label for="title_ar"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Anually Payment Title [ Korean ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('anually_title_ar','', array('placeholder' => '','class' => 'form-control','id'=>'anually_title_ar', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="anually_title_ar_error">This is required.</span>
                        </div>
                    </div>
                @endif
                @if(Helper::GeneralWebmasterSettings("en_box_status"))
                    <div class="form-group row">
                        <label for="title_en"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("ar_box_status") && Helper::GeneralWebmasterSettings("en_box_status"))  Anually Payment Title [ English ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('anually_title_en','', array('placeholder' => '','class' => 'form-control','id'=>'anually_title_en', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="anually_title_en_error">This is required.</span>
                        </div>
                    </div>
                @endif
				@if(Helper::GeneralWebmasterSettings("jp_box_status"))
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            @if(Helper::GeneralWebmasterSettings("jp_box_status") && Helper::GeneralWebmasterSettings("jp_box_status"))  Anually Payment Title [ Japanese ] @endif
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('anually_title_jp','', array('placeholder' => '','class' => 'form-control','id'=>'anually_title_jp', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="anually_title_jp_error">This is required.</span>
                        </div>
                    </div>
                @endif
                    <div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Anually Price
                        </label>
                        <div class="col-sm-10">
                            {!! Form::text('anually_price','', array('placeholder' => '','class' => 'form-control','id'=>'anually_price', 'dir'=>trans('backLang.ltr'))) !!}
							<span class="err" id="anually_price_error">This is required.</span>
                        </div>
                    </div>
					<div class="form-group row">
                    <label for="photo_file"
                           class="col-sm-2 form-control-label">{!!  trans('backLang.topicPhoto') !!}</label>
                    <div class="col-sm-10">
                        {!! Form::file('anually_photo_file', array('class' => 'form-control','id'=>'anually_photo_file','accept'=>'image/*')) !!}
                    </div>
                </div>

                <div class="form-group row m-t-md" style="margin-top: 0 !important;">
                    <div class="col-sm-offset-2 col-sm-10">
                        <small>
                            <i class="material-icons">&#xe8fd;</i>
                            {!!  trans('backLang.imagesTypes') !!}
                        </small>
                    </div>
                </div>
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Anually Signature Limit [ Korea ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('anually_limit_signature_ar','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control anually_limit_signature_ar','id'=>'anually_limit_signature_ar', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="anually_limit_signature_ar_error">This is required.</span>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Anually Signature Limit [ English ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('anually_limit_signature_en','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control anually_limit_signature_en','id'=>'anually_limit_signature_en', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="limit_signature_en_error">This is required.</span>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="title_jp"
                               class="col-sm-2 form-control-label">
                            Anually Signature Limit [ Japanese ]
                        </label>
                        <div class="col-sm-10">
							{!! Form::textarea('anually_limit_signature_jp','<div dir=ltr><br></div>', array('ui-jp'=>'summernote','placeholder' => '','class' => 'form-control anually_limit_signature_jp','id'=>'anually_limit_signature_jp', 'dir'=>trans('backLang.ltr'),'ui-options'=>'{height: 300,callbacks: {
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable,0);
        }
    }}')) !!}
							<span class="err" id="limit_signature_jp_error">This is required.</span>
                        </div>
                    </div>
				</div>
                <div class="form-group row m-t-md addcan">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary m-t" onclick="return validate()"><i class="material-icons">
                                &#xe31b;</i> {!! trans('backLang.add') !!}</button>
                        <a href="{{route("Payments")}}"
                           class="btn btn-default m-t"><i class="material-icons">
                                &#xe5cd;</i> {!! trans('backLang.cancel') !!}</a>
                    </div>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection
<script>
function checkthis(id) {
	if(id == 2) {
		if($("input[name=limit_period_anual]").prop('checked') == true) {
			$('#anually').show();
		}else {
			$('#anually').hide();
		}
		$('.addcan').show();
	}else if(id == 1) {
		if($("input[name=limit_period]").prop('checked') == true) {
			$('#monthly').show();
		}else {
			$('#monthly').hide();
		}
		$('.addcan').show();
	}
	if($("input[name=limit_period]").prop('checked') == false && $("input[name=limit_period_anual]").prop('checked') == false) {
		$('.addcan').hide();
	}
}
function validate() {
	var errr = 0;
	$('.err').hide();
	if($("input[name=limit_period]").prop('checked') == true) {
		var title_ar = $('#title_ar').val();
		var title_en = $('#title_en').val();
		var title_jp = $('#title_jp').val();
		var price = $('#price').val();
		/* var limit_signature_ar = $('#limit_signature_ar').val();
		var limit_signature_en = $('#limit_signature_en').val();
		var limit_signature_jp = $('#limit_signature_jp').val(); */
		if(title_ar == '') {
			$('#title_ar_error').show();
			errr = 1;
		}
		if(title_en == '') {
			$('#title_en_error').show();
			errr = 1;
		}
		if(title_jp == '') {
			$('#title_jp_error').show();
			errr = 1;
		}
		if(price == '') {
			$('#price_error').show();
			errr = 1;
		}
		/* if(limit_signature_ar == '') {
			$('#limit_signature_ar_error').show();
			errr = 1;
		}
		if(limit_signature_en == '') {
			$('#limit_signature_en_error').show();
			errr = 1;
		}
		if(limit_signature_jp == '') {
			$('#limit_signature_jp_error').show();
			errr = 1;
		} */
	}
	if($("input[name=limit_period_anual]").prop('checked') == true) {
		var anually_title_ar = $('#anually_title_ar').val();
		var anually_title_en = $('#anually_title_en').val();
		var anually_title_jp = $('#anually_title_jp').val();
		var anually_price = $('#anually_price').val();
		/* var anually_limit_signature_ar = $('#anually_limit_signature_ar').val();
		var anually_limit_signature_jp = $('#anually_limit_signature_jp').val();
		var anually_limit_signature_en = $('#anually_limit_signature_en').val(); */
		if(anually_title_ar == '') {
			$('#anually_title_ar_error').show();
			errr = 1;
		}
		if(anually_title_en == '') {
			$('#anually_title_en_error').show();
			errr = 1;
		}
		if(anually_title_jp == '') {
			$('#anually_title_jp_error').show();
			errr = 1;
		}
		if(anually_price == '') {
			$('#anually_price_error').show();
			errr = 1;
		}
		/* if(anually_limit_signature_ar == '') {
			$('#anually_limit_signature_ar_error').show();
			errr = 1;
		}
		if(anually_limit_signature_en == '') {
			$('#anually_limit_signature_en_error').show();
			errr = 1;
		}
		if(anually_limit_signature_jp == '') {
			$('#anually_limit_signature_jp_error').show();
			errr = 1;
		} */
	}
	if(errr == 1) {
		return false;
	}
}
</script>
<style>
.err{display:none;color:red;}
</style>
@section('footerInclude')

    <script src="{{ URL::to("backEnd/libs/js/iconpicker/fontawesome-iconpicker.js") }}"></script>
    <script>
        $(function () {
            $('.icp-auto').iconpicker({placement: '{{ (trans('backLang.direction')=="ltr")?"topLeft":"topRight" }}'});
        });
    </script>
@endsection

