@extends('backEnd.layout')
@section('headerInclude')
    <link rel="stylesheet" type="text/css" href="{{ URL::to("backEnd/assets/styles/flags.css") }}"/>
@endsection
@section('content')
    <div class="padding p-b-0">
        <div class="margin">
            <h5 class="m-b-0 _300">{{ trans('backLang.hi') }} <span class="text-primary">{{ Auth::user()->name }}</span>, {{ trans('backLang.welcomeBack') }}
            </h5>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-4">
                <div class="row">
                    <?php
                    $data_sections_arr = explode(",", Auth::user()->permissionsGroup->data_sections);
                    $clr_ary = array("info", "danger", "success", "accent",);
                    $ik = 0;
                    ?>
                    @foreach($GeneralWebmasterSections as $headerWebmasterSection)
                        @if(in_array($headerWebmasterSection->id,$data_sections_arr))
                            @if($ik<4)
                                <?php
                                $LiIcon = "&#xe2c8;";
                                if ($headerWebmasterSection->type == 3) {
                                    $LiIcon = "&#xe050;";
                                }
                                if ($headerWebmasterSection->type == 2) {
                                    $LiIcon = "&#xe63a;";
                                }
                                if ($headerWebmasterSection->type == 1) {
                                    $LiIcon = "&#xe251;";
                                }
                                if ($headerWebmasterSection->type == 0) {
                                    $LiIcon = "&#xe2c8;";
                                }
                                if ($headerWebmasterSection->name == "sitePages") {
                                    $LiIcon = "&#xe3e8;";
                                }
                                if ($headerWebmasterSection->name == "articles") {
                                    $LiIcon = "&#xe02f;";
                                }
                                if ($headerWebmasterSection->name == "services") {
                                    $LiIcon = "&#xe540;";
                                }
                                if ($headerWebmasterSection->name == "news") {
                                    $LiIcon = "&#xe307;";
                                }
                                if ($headerWebmasterSection->name == "products") {
                                    $LiIcon = "&#xe8f6;";
                                }

                                ?>
                                <div class="col-xs-6">
                                    <div class="box p-a" style="cursor: pointer"
                                         onclick="location.href='{{ route('topics',$headerWebmasterSection->id) }}'">
                                        <a href="{{ route('topics',$headerWebmasterSection->id) }}">
                                            <div class="pull-left m-r">
                                                <i class="material-icons  text-2x text-{{$clr_ary[$ik]}} m-y-sm">{!! $LiIcon !!}</i>
                                            </div>
                                            <div class="clear">
                                                <div class="text-muted">{{ trans('backLang.'.$headerWebmasterSection->name) }}</div>
                                                <h4 class="m-a-0 text-md _600">{{ $headerWebmasterSection->topics->count() }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <?php
                                $ik++
                                ?>
                            @endif
                        @endif
                    @endforeach
                    <div class="col-xs-12">
                        <div class="row-col box-color text-center primary">
                            <div class="row-cell p-a">
                                {{ trans('backLang.visitors') }}
                                <h4 class="m-a-0 text-md _600"><a href>{{$TodayVisitors}}</a></h4>
                            </div>
                            <div class="row-cell p-a dker">
                                {{ trans('backLang.pageViews') }}
                                <h4 class="m-a-0 text-md _600"><a href>{{$TodayPages}}</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-8">
                <div class="row-col box bg">
                    <div class="col-sm-8">
                        <div class="box-header">
                            <h3>{{ trans('backLang.visitors') }}</h3>
                            <small>{{ trans('backLang.lastFor7Days') }}</small>
                        </div>
                        <div class="box-body">
                            <div ui-jp="plot" ui-refresh="app.setting.color" ui-options="
			              [
			                {
			                  data: [
                  <?php
                            $ii = 1;
                            ?>
                            @foreach($Last7DaysVisitors as $id)

                            @if($ii<=10)
                            @if($ii!=1)
                                    ,
                                    @endif
                            <?php
                            $i2 = 0;
                            ?>
                            @foreach($id as $key => $val)
                            <?php
                            if ($i2 == 1) {
                            ?>
                                    [{{ $ii }}, {{$val}}]
                                <?php
                            }
                            $i2++;
                            ?>
                            @endforeach
                            @endif
                            <?php $ii++;?>
                            @endforeach
                                    ],
                                  points: { show: true, radius: 0},
                                  splines: { show: true, tension: 0.45, lineWidth: 2, fill: 0 }
                                },
                                {
                                  data: [
                                                                                  <?php
                            $ii = 1;
                            ?>
                            @foreach($Last7DaysVisitors as $id)

                            @if($ii<=10)
                            @if($ii!=1)
                                    ,
                                    @endif
                            <?php
                            $i2 = 0;
                            ?>
                            @foreach($id as $key => $val)
                            <?php
                            if ($i2 == 2) {
                            ?>
                                    [{{ $ii }}, {{$val}}]
                                <?php
                            }
                            $i2++;
                            ?>
                            @endforeach
                            @endif
                            <?php $ii++;?>
                            @endforeach
                                    ],
      points: { show: true, radius: 0},
      splines: { show: true, tension: 0.45, lineWidth: 2, fill: 0 }
    }
  ],
  {
    colors: ['#0cc2aa','#fcc100'],
    series: { shadowSize: 3 },
    xaxis: { show: true, font: { color: '#ccc' }, position: 'bottom' },
    yaxis:{ show: true, font: { color: '#ccc' }},
    grid: { hoverable: true, clickable: true, borderWidth: 0, color: 'rgba(120,120,120,0.5)' },
    tooltip: true,
    tooltipOpts: { content: '%x.0 is %y.4',  defaultTheme: false, shifts: { x: 0, y: -40 } }
  }
" style="height:162px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 dker">
                        <div class="box-header">
                            <h3>{{ trans('backLang.reports') }}</h3>
                        </div>
                        <div class="box-body">
                            <p class="text-muted">
                                {{ trans('backLang.reportsDetails') }} : <br>
                                <a href="{{ route('analytics', 'date') }}">{{ trans('backLang.visitorsAnalyticsBydate') }}</a>,
                                <a href="{{ route('analytics', 'country') }}">{{ trans('backLang.visitorsAnalyticsByCountry') }}</a>,
                                <a href="{{ route('analytics', 'city') }}">{{ trans('backLang.visitorsAnalyticsByCity') }}</a>,
                                <a href="{{ route('analytics', 'os') }}">{{ trans('backLang.visitorsAnalyticsByOperatingSystem') }}</a>,
                                <a href="{{ route('analytics', 'browser') }}">{{ trans('backLang.visitorsAnalyticsByBrowser') }}</a>,
                                <a href="{{ route('analytics', 'referrer') }}">{{ trans('backLang.visitorsAnalyticsByReachWay') }}</a>,
                                <a href="{{ route('analytics', 'hostname') }}">{{ trans('backLang.visitorsAnalyticsByHostName') }}</a>,
                                <a href="{{ route('analytics', 'org') }}">{{ trans('backLang.visitorsAnalyticsByOrganization') }}</a>
                            </p>
                            <a href="{{ route('analytics', 'date') }}" style="margin-bottom: 5px;"
                               class="btn btn-sm btn-outline rounded b-success">{{ trans('backLang.viewMore') }}</a><br>
                            <a href="{{ route('visitors') }}"
                               class="btn btn-sm btn-outline rounded b-info">{{ trans('backLang.visitorsAnalyticsVisitorsHistory') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xl-4">
                <div class="box">
                    <div class="box-header">
                        <h3>{{ trans('backLang.visitorsRate') }}</h3>
                        <small>{{ trans('backLang.visitorsRateToday')." [ ".date('Y-m-d')." ]" }}</small>
                    </div>
                    <div class="box-body">

                        <div ui-jp="plot" ui-options="
              [
                {
                  data: [{!! $TodayVisitorsRate !!}],
                  points: { show: true, radius: 5},
                  splines: { show: true, tension: 0.45, lineWidth: 0, fill: 0.4}
                }
              ],
              {
                colors: ['#0cc2aa'],
                series: { shadowSize: 3 },
                xaxis: { show: true, font: { color: '#ccc' }, position: 'bottom' },
                yaxis:{ show: true, font: { color: '#ccc' }, min:1},
                grid: { hoverable: true, clickable: true, borderWidth: 0, color: 'rgba(120,120,120,0.5)' },
                tooltip: true,
                tooltipOpts: { content: '%x.0 is %y.4',  defaultTheme: false, shifts: { x: 0, y: -40 } }
              }
            " style="height:200px">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-4">
                <div class="box" style="min-height: 300px">
                    <div class="box-header">
                        <h3>{{ trans('backLang.browsers') }}</h3>
                        <small>{{ trans('backLang.browsersCalculated') }}</small>
                    </div>

                    @if($TodayByBrowser1_val >0)
                        <div class="text-center b-t">
                            <div class="row-col">
                                <div class="row-cell p-a">
                                    <div class="inline m-b">
                                        <div ui-jp="easyPieChart" class="easyPieChart" ui-refresh="app.setting.color"
                                             data-redraw='true' data-percent="55" ui-options="{
	                      lineWidth: 8,
	                      trackColor: 'rgba(0,0,0,0.05)',
	                      barColor: '#0cc2aa',
	                      scaleColor: 'transparent',
	                      size: 100,
	                      scaleLength: 0,
	                      animate:{
	                        duration: 3000,
	                        enabled:true
	                      }
	                    }">
                                            <div>
                                                <h5>
                                                    <?php
                                                    echo $perc1 = round(($TodayByBrowser1_val * 100) / ($TodayByBrowser1_val + $TodayByBrowser2_val)) . "%";
                                                    ?>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        {{$TodayByBrowser1}}
                                        <small class="block m-b">{{$TodayByBrowser1_val}}</small>
                                        <a href="{{ route('analytics', 'browser') }}"
                                           class="btn btn-sm white text-u-c rounded">{{ trans('backLang.more') }}</a>
                                    </div>
                                </div>
                                <div class="row-cell p-a dker">
                                    <div class="inline m-b">
                                        <div ui-jp="easyPieChart" class="easyPieChart" ui-refresh="app.setting.color"
                                             data-redraw='true' data-percent="45" ui-options="{
	                      lineWidth: 8,
	                      trackColor: 'rgba(0,0,0,0.05)',
	                      barColor: '#fcc100',
	                      scaleColor: 'transparent',
	                      size: 100,
	                      scaleLength: 0,
	                      animate:{
	                        duration: 3000,
	                        enabled:true
	                      }
	                    }">
                                            <div>
                                                <h5>
                                                    <?php
                                                    echo $perc1 = round(($TodayByBrowser2_val * 100) / ($TodayByBrowser1_val + $TodayByBrowser2_val)) . "%";
                                                    ?>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        {{$TodayByBrowser2}}
                                        <small class="block m-b">{{$TodayByBrowser2_val}}</small>
                                        <a href="{{ route('analytics', 'browser') }}"
                                           class="btn btn-sm white text-u-c rounded">{{ trans('backLang.more') }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
           

        </div>
   
    </div>

@endsection
