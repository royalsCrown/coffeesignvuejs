<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebmasterTag extends Model
{
    //
    public function Tags()
    {

        return $this->hasMany('App\Tag', 'id');
    }
}
