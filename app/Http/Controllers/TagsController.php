<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Banner;
use App\Http\Requests;
use App\WebmasterBanner;
use App\WebmasterPayment;
use App\WebmasterSection;
use Auth;
use File;
use Helper;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;

class TagsController extends Controller
{

    private $uploadPath = "uploads/banners/";

    public function __construct()
    {
        $this->middleware('auth');

       if (!@Auth::user()->permissionsGroup->banners_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
    }

   
    public function index()
    {
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        $Banners = Tag::orderby('id', 'asc')->orderby('id', 'asc')->paginate(env('BACKEND_PAGINATION'));	
        return view("backEnd.tags", compact("Banners","GeneralWebmasterSections"));
    }
	public function create()
    {
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        
        return view("backEnd.tags.create", compact("GeneralWebmasterSections"));
    }
	
	public function store(Request $request)
    { 
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        $Tag = new Tag;
        $Tag->title_ar = $request->title_ar;
        $Tag->title_en = $request->title_en;
        $Tag->title_jp = $request->title_jp;
        $Tag->status = 1;
        $Tag->created_by = Auth::user()->id;
        $Tag->save();

        return redirect()->action('TagsController@index')->with('doneMessage', trans('backLang.addDone'));
    }
	public function edit($id)
    {
        if (!@Auth::user()->permissionsGroup->edit_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        $Banners = Tag::find($id);
        if (!empty($Banners)) {
            return view("backEnd.tags.edit", compact("Banners", "GeneralWebmasterSections"));
        } else {
            return redirect()->action('TagsController@index');
        }
    }
	
	public function update(Request $request, $id)
    {
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        $Tag = Tag::find($id);
        if (!empty($Tag)) {
            $Tag->title_ar = $request->title_ar;
			$Tag->title_en = $request->title_en;
			$Tag->title_jp = $request->title_jp;
            $Tag->status = $request->status;
            $Tag->updated_by = Auth::user()->id;
            $Tag->save();
            return redirect()->action('TagsController@edit', $id)->with('doneMessage', trans('backLang.saveDone'));
        } else {
            return redirect()->action('TagsController@index');
        }
    }
	
		
	 public function destroy($id)
    { 
        if (!@Auth::user()->permissionsGroup->delete_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        if (@Auth::user()->permissionsGroup->view_status) {
            $Tag = Tag::where('created_by', '=', Auth::user()->id)->find($id);
        } else {
            $Tag = Tag::find($id);
        }
        if (!empty($Tag)) {
            $Tag->delete();
            return redirect()->action('TagsController@index')->with('doneMessage', trans('backLang.deleteDone'));
        } else {
            return redirect()->action('TagsController@index');
        }
    }
}
?>