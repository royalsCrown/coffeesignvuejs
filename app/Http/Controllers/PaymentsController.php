<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Banner;
use App\Http\Requests;
use App\WebmasterBanner;
use App\WebmasterPayment;
use App\WebmasterSection;
use Auth;
use File;
use Helper;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;

class PaymentsController extends Controller
{

    private $uploadPath = "uploads/banners/";

    // Define Default Variables

    public function __construct()
    {
        $this->middleware('auth');

        // Check Permissions
       if (!@Auth::user()->permissionsGroup->banners_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        // General END

        //List of Banners Sections
        /* $WebmasterPayments = WebmasterPayment::where('status', '=', '1')->orderby('id', 'asc')->get(); */
        $Banners = Payment::orderby('id', 'asc')->orderby('id', 'asc')->paginate(env('BACKEND_PAGINATION'));
/* echo '<pre>'; print_r($Payments); */		
        return view("backEnd.payments", compact("Banners","GeneralWebmasterSections"));
    }
	public function paymentsby()
    { echo 'here'; die;
        /* $Banners = Payment::orderby('id', 'asc');
		echo json_encode($Banners, JSON_UNESCAPED_UNICODE); */
    }
	public function create()
    {
        // Check Permissions
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        //
        // General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        // General END

        

        return view("backEnd.payments.create", compact("GeneralWebmasterSections"));
    }
	
	public function store(Request $request)
    { 
        // Check Permissions
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
		$this->validate($request, [
            'photo_file' => 'mimes:png,jpeg,jpg,gif|max:3000',
            'anually_photo_file' => 'mimes:png,jpeg,jpg,gif|max:3000'
        ]);
		$formFileName = "photo_file";
        $fileFinalName_ar = "";
        if ($request->$formFileName != "") {
            $fileFinalName_ar = time() . rand(1111,
                    9999) . '.' . $request->file($formFileName)->getClientOriginalExtension();
            $path = $this->getUploadPath();
            $request->file($formFileName)->move($path, $fileFinalName_ar);
        }
        $formFileName = "anually_photo_file";
        $fileFinalName_en = "";
        if ($request->$formFileName != "") {
            $fileFinalName_en = time() . rand(1111,
                    9999) . '.' . $request->file($formFileName)->getClientOriginalExtension();
            $path = $this->getUploadPath();
            $request->file($formFileName)->move($path, $fileFinalName_en);
        } 
        $Payment = new Payment;
		if(isset($request->limit_period)) {
			$Payment->monthly = 'yes';
		}else {
			$Payment->monthly = 'no';
		}
		if(isset($request->limit_period_anual)) {
			$Payment->anually = 'yes';
		}else {
			$Payment->anually = 'no';
		}
        $Payment->title_ar = $request->title_ar;
        $Payment->title_en = $request->title_en;
        $Payment->title_jp = $request->title_jp;
        $Payment->price = $request->price;
        $Payment->limit_signature_ar = $request->limit_signature_ar;
        $Payment->limit_signature_en = $request->limit_signature_en;
        $Payment->limit_signature_jp = $request->limit_signature_jp;
		$Payment->anually_title_ar = $request->anually_title_ar;
        $Payment->anually_title_en = $request->anually_title_en;
        $Payment->anually_title_jp = $request->anually_title_jp;
        $Payment->anually_price = $request->anually_price;
        $Payment->anually_limit_signature_ar = $request->anually_limit_signature_ar;
        $Payment->anually_limit_signature_en = $request->anually_limit_signature_en;
        $Payment->anually_limit_signature_jp = $request->anually_limit_signature_jp;
		if($fileFinalName_ar != '') {
			$Payment->photo_file = $fileFinalName_ar;
		}
		if($fileFinalName_en != '') {
			$Payment->anually_photo_file = $fileFinalName_en;
		}
        $Payment->status = 1;
        $Payment->created_by = Auth::user()->id;
        $Payment->save();

        return redirect()->action('PaymentsController@index')->with('doneMessage', trans('backLang.addDone'));
    }
	public function getUploadPath()
    {
        return $this->uploadPath;
    }

    public function setUploadPath($uploadPath)
    {
        $this->uploadPath = Config::get('app.APP_URL') . $uploadPath;
    }
	public function edit($id)
    {
        // Check Permissions
        if (!@Auth::user()->permissionsGroup->edit_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        //
        // General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        $Banners = Payment::find($id);
        if (!empty($Banners)) {
            //Banner Sections Details
            /* $WebmasterBanner = WebmasterBanner::find($Banners->section_id); */

            return view("backEnd.payments.edit", compact("Banners", "GeneralWebmasterSections"));
        } else {
            return redirect()->action('PaymentsController@index');
        }
    }
	
	public function update(Request $request, $id)
    {
        // Check Permissions
        if (!@Auth::user()->permissionsGroup->add_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        //
        $Payment = Payment::find($id);
        if (!empty($Payment)) {
			$this->validate($request, [
				'photo_file' => 'mimes:png,jpeg,jpg,gif|max:3000',
				'anually_photo_file' => 'mimes:png,jpeg,jpg,gif|max:3000'
			]);
			$formFileName = "photo_file";
			$fileFinalName_ar = "";
			if ($request->$formFileName != "") {
				$fileFinalName_ar = time() . rand(1111,
						9999) . '.' . $request->file($formFileName)->getClientOriginalExtension();
				$path = $this->getUploadPath();
				$request->file($formFileName)->move($path, $fileFinalName_ar);
			}
			$formFileName = "anually_photo_file";
			$fileFinalName_en = "";
			if ($request->$formFileName != "") {
				$fileFinalName_en = time() . rand(1111,
						9999) . '.' . $request->file($formFileName)->getClientOriginalExtension();
				$path = $this->getUploadPath();
				$request->file($formFileName)->move($path, $fileFinalName_en);
			}
			if(isset($request->limit_period)) {
				$Payment->monthly = 'yes';
			}else {
				$Payment->monthly = 'no';
			}
			if(isset($request->limit_period_anual)) {
				$Payment->anually = 'yes';
			}else {
				$Payment->anually = 'no';
			}
            $Payment->title_ar = $request->title_ar;
			$Payment->title_en = $request->title_en;
			$Payment->title_jp = $request->title_jp;
			$Payment->price = $request->price;
			$Payment->limit_signature_ar = $request->limit_signature_ar;
			$Payment->limit_signature_en = $request->limit_signature_en;
			$Payment->limit_signature_jp = $request->limit_signature_jp;
			$Payment->anually_title_ar = $request->anually_title_ar;
			$Payment->anually_title_en = $request->anually_title_en;
			$Payment->anually_title_jp = $request->anually_title_jp;
			$Payment->anually_price = $request->anually_price;
			$Payment->anually_limit_signature_ar = $request->anually_limit_signature_ar;
			$Payment->anually_limit_signature_en = $request->anually_limit_signature_en;
			$Payment->anually_limit_signature_jp = $request->anually_limit_signature_jp;
			if ($fileFinalName_ar != "") {
                if ($Payment->photo_file != "") {
                    File::delete($this->getUploadPath() . $Payment->photo_file);
                }

                $Payment->photo_file = $fileFinalName_ar;
            }
            if ($fileFinalName_en != "") {
                if ($Payment->anually_photo_file != "") {
                    File::delete($this->getUploadPath() . $Payment->anually_photo_file);
                }
                $Payment->anually_photo_file = $fileFinalName_en;
            }
            $Payment->status = $request->status;
            $Payment->updated_by = Auth::user()->id;
            $Payment->save();
            return redirect()->action('PaymentsController@edit', $id)->with('doneMessage', trans('backLang.saveDone'));
        } else {
            return redirect()->action('PaymentsController@index');
        }
    }
	
		
	 public function destroy($id)
    { 
        // Check Permissions
        if (!@Auth::user()->permissionsGroup->delete_status) {
            return Redirect::to(route('NoPermission'))->send();
        }
        //
        if (@Auth::user()->permissionsGroup->view_status) {
            $Payment = Payment::where('created_by', '=', Auth::user()->id)->find($id);
        } else {
            $Payment = Payment::find($id);
        }
        if (!empty($Payment)) {
            // Delete a Payment file
            if ($Payment->file_ar != "") {
                File::delete($this->getUploadPath() . $Payment->file_ar);
            }
            if ($Payment->file_en != "") {
                File::delete($this->getUploadPath() . $Payment->file_en);
            }

            $Payment->delete();
            return redirect()->action('PaymentsController@index')->with('doneMessage', trans('backLang.deleteDone'));
        } else {
            return redirect()->action('PaymentsController@index');
        }
    }

}