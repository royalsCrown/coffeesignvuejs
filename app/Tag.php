<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    public function webmasterTag()
    {

        return $this->belongsTo('App\WebmasterTag', 'id');
    }
}
