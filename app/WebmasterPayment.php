<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebmasterPayment extends Model
{
    //
    public function payments()
    {

        return $this->hasMany('App\Payment', 'id');
    }
}
