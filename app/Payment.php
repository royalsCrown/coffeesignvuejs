<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    public function webmasterPayment()
    {

        return $this->belongsTo('App\WebmasterPayment', 'id');
    }
}
