document.addEventListener('DOMContentLoaded', function () {

  // START APP LINE HIDDEN
  var appline = document.querySelector('.appLine');

  if (appline) {
    appline.addEventListener('click', function (e) {
      var tg = e.target;
  
      if (tg.closest('.appLine__button')) {
        this.classList.remove('active');
        this.style.marginTop =  (- this.offsetHeight - 3) + 'px';
      }
    });
  }
  // END APP LINE HIDDEN

  // START LANGUAGE TOGGLE
  var languageListItem = document.querySelector('.language__list-item');

  if (languageListItem) {
    languageListItem.addEventListener('click', function (e) {
      var tg = e.target;
  
      if (tg.closest('.language')) {
        e.preventDefault();
  
        var language = document.querySelector('.language');
        var languageLinks = document.querySelector('.language__links');
  
        if (languageLinks.classList.contains('active')) {
          languageLinks.classList.add('leave-active');
          setTimeout(function () {
            languageLinks.classList.remove('active', 'leave-active');
            language.classList.remove('active');
          }, 300);
          return;
        }
  
        languageLinks.classList.add('pre-active');
        language.classList.add('active');
        setTimeout(function () {
          languageLinks.classList.add('active');
          languageLinks.classList.remove('pre-active');
        }, 10);
      }
    });
  }
  // END LANGUAGE TOGGLE

  // START LEGALITY GUIDE SWIPER (slider)
  var legalityGuideSlider = document.querySelector('.legalityGuide__items-wrap');
  
  if (legalityGuideSlider) {
    new Swiper ('.legalityGuide__items-wrap', {
      roundLengths: true,
      autoHeight: true,
      spaceBetween: 50,
      grabCursor: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }
  // END LEGALITY GUIDE SWIPER (slider)

  // START TESTIMONIALS SWIPER (slider)
  var testimonialsSlider = document.querySelector('.testimonials__items-wrap');
  
  if (testimonialsSlider) {
    new Swiper ('.testimonials__items-wrap', {
      slidesPerView: 2,
      navigation: {
        nextEl: '.testimonials__buttons-next',
        prevEl: '.testimonials__buttons-prev',
      },
      breakpoints: {
        768: {
          slidesPerView: 1,
          centerInsufficientSlides: true
        },
        992: {
          slidesPerView: 1,
        }
      }
    });
  }
  // END TESTIMONIALS SWIPER (slider)

  // START PRICING TOGGLE
  var pricingToggle = document.querySelector('.pricing__toggle');

  if (pricingToggle) {
    pricingToggle.addEventListener('click', function (e) {
      var tg = e.target;
      var buttons = this.querySelectorAll('.pricing__toggle-button');
      var items = document.querySelectorAll('.pricing__items-wrap');
  
      if (tg.closest('.pricing__toggle-button')) {
        for (var i = 0; i < buttons.length; i++) {
          buttons[i].parentNode.classList.remove('active');
          buttons[i].setAttribute('data-number', i);
          items[i].classList.remove('active');
        }
      }
  
      tg.parentNode.classList.add('active');
      items[tg.getAttribute('data-number')].classList.add('active');
    });
  }
  // END PRICING TOGGLE

  // START MOBILE MENU
  var headerTopline = document.querySelector('.header__topline');
  var showMobileMenuFlag = false;

  if (headerTopline) {
    headerTopline.addEventListener('click', function (e) {
      var tg = e.target;
  
      if (tg.closest('.header__mobile-button')) {
        if (!tg.closest('.header__mobile-button.active')) {
          addClassesMenu();
          showMobileMenuFlag = true;
        } else {
          removeClassesMenu();
          showMobileMenuFlag = false;
        }
      }
    });

    var touchEvent = 'ontouchend' in window ? 'touchend' : 'click';

    document.body.addEventListener(touchEvent, function (e) {
      var tg = e.target;
      if (tg.closest('.header__mobile-button')) return;

      if (showMobileMenuFlag && !tg.closest('.header__nav')) {
        removeClassesMenu();
        showMobileMenuFlag = false;
      }
    })
  }

  function addClassesMenu () {
    headerTopline.querySelector('.header__mobile-button').classList.add('active');
    headerTopline.querySelector('.header__nav').classList.add('active');
    document.querySelector('body').classList.add('mobileMenuActive');
    document.querySelector('.page-wrapper').classList.add('mobileMenuActive');
  }

  function removeClassesMenu () {
    headerTopline.querySelector('.header__mobile-button').classList.remove('active');
    headerTopline.querySelector('.header__nav').classList.remove('active');
    document.querySelector('body').classList.remove('mobileMenuActive');
    document.querySelector('.page-wrapper').classList.remove('mobileMenuActive');
  }
  // END MOBILE MENU

  // START SUBSTRACTION SHOW FORM
  var substractionButton = document.querySelector('.substraction__button');

  if (substractionButton) {
    substractionButton.addEventListener('click', function (e) {
      this.classList.toggle('active');
      document.querySelector('.substraction__wrap').classList.toggle('active');
    });
  }
  // END SUBSTRACTION SHOW FORM

  // START LEGALITY SEARCH INPUT
  var searchInputForm = document.querySelector('.header__form--legality label .form-input');
  var searchList = document.querySelector('.header__form-list');

  if (searchInputForm) {
    searchInputForm.addEventListener('focus', function () {
      this.parentNode.classList.add('active');
      document.querySelector('.header__form-list').classList.add('active');
    })
    searchInputForm.addEventListener('blur', function () {
      var self = this;
      setTimeout(function () {
        self.parentNode.classList.remove('active');
        document.querySelector('.header__form-list').classList.remove('active');
      }, 200)
    })

    searchList.addEventListener('click', function (e) {
      var tg = e.target;

      if (tg.closest('.header__form-list > li')) {
        var result = tg.innerText;
        this.parentNode.querySelector('.form-input').value = result;
      }
    })
  }
  // END LEGALITY SEARCH INPUT

  // START SHOW HINTS
  var hintsSections = document.querySelectorAll('.hintWrap');
  var flagShowMessage = false;

  if (hintsSections) {

    for (var i = 0; i < hintsSections.length; i++) {
      hintsSections[i].addEventListener('click', function (e) {
        showHideText.call(this, e.target);
      })
    }

    function showHideText (tg) {
      var windowWidth = window.innerWidth;
      var leftPosition = this.getBoundingClientRect().left;
      
      // show text hint
      if (tg.closest('.hintButton') && !flagShowMessage) {
        var hintText = document.querySelectorAll('.hintText');

        for (var i = 0; i < hintText.length; i++) {
          hintText[i].classList.remove('active');
        }

        this.querySelector('.hintText').classList.add('active');

        var hintWidth = this.querySelector('.hintText').offsetWidth;
        var offsetHint = (leftPosition + hintWidth / 2) > windowWidth;

        if (offsetHint) {
          var diff = (leftPosition + hintWidth / 2) - windowWidth + 15;
          this.querySelector('.hintText').style.left = (-hintWidth / 2) - diff + 'px';
        }
  
        widthProgressLine.call(this);
      }
  
      // hide text hint
      if (tg.closest('.close')) {
        this.querySelector('.hintText').classList.remove('active');
      }
    }

    function widthProgressLine () {
      flagShowMessage = true;

      function hideHintText () {
        this.querySelector('.hintText').classList.remove('active');
        this.querySelector('.progressLine').classList.remove('active');
        flagShowMessage = false;
      };

      (function (self) {
        self.querySelector('.progressLine').classList.add('active');
        setTimeout(hideHintText.bind(self), 5000)
      })(this);
    }

  }
  // END SHOW HINTS

  // START CASE STUDY OPTIONS LIST
  var optionsList = document.querySelectorAll('.showOptionsList');

  if (optionsList) {
    for (var i = 0; i < optionsList.length; i++) {

      optionsList[i].querySelector('label .form-input').addEventListener('blur', function () {
        var self = this;
  
        setTimeout(function () {
          self.parentNode.classList.remove('active');
          self.parentNode.parentNode.querySelector('.header__options-list--caseStudy').classList.remove('active');
        }, 200)
      })
  
      optionsList[i].querySelector('label .form-input').addEventListener('focus', function () {
        this.parentNode.classList.add('active');
        this.parentNode.parentNode.querySelector('.header__options-list--caseStudy').classList.add('active');
      })
  
      // chose option in list
      optionsList[i].querySelector('.header__options-list--caseStudy').addEventListener('click', function (e) {
        var tg = e.target;
  
        if (tg.closest('li')) {
          this.parentNode.querySelector('label .form-input').value = tg.innerText;
        }
      })
  
    }
  }
  // END CASE STUDY OPTIONS LIST

  // START CASE STUDY MENU
  var contractsToplineButton = document.querySelector('.contracts__topLine-button')

  if (contractsToplineButton) {
    contractsToplineButton.addEventListener('click', function () {
      var self = this;
      this.classList.toggle('active');
  
      if (self.classList.contains('active')) {
        self.parentNode.querySelector('.contracts__links-list').classList.add('active');
      } else {
        self.parentNode.querySelector('.contracts__links-list').classList.remove('active');
      }
    })
  }
  // END CASE STUDY MENU

});



// START POLYFILLS FOR IE AND OTHER BROWSERS
// START POLYFILL FOR MATCHES METHOD
(function() {

  // проверяем поддержку
  if (!Element.prototype.matches) {

    // определяем свойство
    Element.prototype.matches = Element.prototype.matchesSelector ||
                                Element.prototype.webkitMatchesSelector ||
                                Element.prototype.mozMatchesSelector ||
                                Element.prototype.msMatchesSelector;

  }

})();
// END POLYFILL FOR MATCHES METHOD

// START POLYFILL FOR CLOSEST METHOD
(function() {

  // проверяем поддержку
  if (!Element.prototype.closest) {

    // реализуем
    Element.prototype.closest = function(css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }

})();
// END POLYFILL FOR CLOSEST METHOD
// END POLYFILLS FOR IE AND OTHER BROWSERS