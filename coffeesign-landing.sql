-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 11, 2019 at 06:28 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coffeesign-landing`
--

-- --------------------------------------------------------

--
-- Table structure for table `smartend_analytics_pages`
--

CREATE TABLE `smartend_analytics_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `load_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_analytics_pages`
--

INSERT INTO `smartend_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '122.173.132.125', 'Smartend Laravel Site Preview', 'unknown', 'http://royalscrown.com/cofeesign/public/', '2.85376096', '2019-08-27', '07:42:11', '2019-08-27 14:42:11', '2019-08-27 14:42:11'),
(2, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.19617105', '2019-08-27', '07:42:22', '2019-08-27 14:42:22', '2019-08-27 14:42:22'),
(3, 2, '52.114.14.102', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.16370177', '2019-08-27', '07:42:36', '2019-08-27 14:42:36', '2019-08-27 14:42:36'),
(4, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.17089295', '2019-08-27', '07:43:11', '2019-08-27 14:43:11', '2019-08-27 14:43:11'),
(5, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.29924011', '2019-08-27', '07:43:52', '2019-08-27 14:43:52', '2019-08-27 14:43:52'),
(6, 4, '52.114.6.38', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.15788794', '2019-08-27', '07:45:00', '2019-08-27 14:45:00', '2019-08-27 14:45:00'),
(7, 3, '103.41.26.222', 'Smartend Laravel Site Preview', 'unknown', 'http://royalscrown.com/cofeesign/public/', '0.30742121', '2019-08-27', '07:47:21', '2019-08-27 14:47:21', '2019-08-27 14:47:21'),
(8, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', '0.49161816', '2019-08-27', '07:49:02', '2019-08-27 14:49:02', '2019-08-27 14:49:02'),
(9, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster', '0.48666382', '2019-08-27', '07:49:02', '2019-08-27 14:49:02', '2019-08-27 14:49:02'),
(10, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/9/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/9/topics?_pjax=%23view', '0.24638104', '2019-08-27', '07:53:49', '2019-08-27 14:53:49', '2019-08-27 14:53:49'),
(11, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings', '0.3850832', '2019-08-27', '07:55:59', '2019-08-27 14:55:59', '2019-08-27 14:55:59'),
(12, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', '0.70889997', '2019-08-27', '07:55:59', '2019-08-27 14:55:59', '2019-08-27 14:55:59'),
(13, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', '0.23386884', '2019-08-27', '07:56:13', '2019-08-27 14:56:13', '2019-08-27 14:56:13'),
(14, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/menus', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus', '2.84953904', '2019-08-27', '08:47:40', '2019-08-27 15:47:40', '2019-08-27 15:47:40'),
(15, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', '0.20788598', '2019-08-27', '08:50:41', '2019-08-27 15:50:41', '2019-08-27 15:50:41'),
(16, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections', '0.17211604', '2019-08-27', '08:51:04', '2019-08-27 15:51:04', '2019-08-27 15:51:04'),
(17, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/6/edit', '0.33243608', '2019-08-27', '08:51:16', '2019-08-27 15:51:16', '2019-08-27 15:51:16'),
(18, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/5/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/5/topics?_pjax=%23view', '0.2196281', '2019-08-27', '08:51:57', '2019-08-27 15:51:57', '2019-08-27 15:51:57'),
(19, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/4/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/4/topics?_pjax=%23view', '0.23934793', '2019-08-27', '08:52:14', '2019-08-27 15:52:14', '2019-08-27 15:52:14'),
(20, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/4/topics/23/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/4/topics/23/edit', '0.454', '2019-08-27', '08:52:17', '2019-08-27 15:52:17', '2019-08-27 15:52:17'),
(21, 3, '103.41.26.222', 'Photos', 'unknown', 'http://royalscrown.com/cofeesign/public/photos', '0.46121907', '2019-08-27', '08:52:56', '2019-08-27 15:52:56', '2019-08-27 15:52:56'),
(22, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/9/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/9/topics', '0.17812204', '2019-08-27', '08:54:37', '2019-08-27 15:54:37', '2019-08-27 15:54:37'),
(23, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.19788194', '2019-08-27', '09:19:35', '2019-08-27 16:19:35', '2019-08-27 16:19:35'),
(24, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', '0.17418408', '2019-08-27', '09:20:06', '2019-08-27 16:20:06', '2019-08-27 16:20:06'),
(25, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', '0.20287085', '2019-08-27', '09:22:38', '2019-08-27 16:22:38', '2019-08-27 16:22:38'),
(26, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/users?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/users?_pjax=%23view', '0.21800113', '2019-08-27', '09:22:51', '2019-08-27 16:22:51', '2019-08-27 16:22:51'),
(27, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.19986677', '2019-08-27', '09:23:16', '2019-08-27 16:23:16', '2019-08-27 16:23:16'),
(28, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners', '0.20619106', '2019-08-27', '09:23:21', '2019-08-27 16:23:21', '2019-08-27 16:23:21'),
(29, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings', '0.19698787', '2019-08-27', '09:25:28', '2019-08-27 16:25:28', '2019-08-27 16:25:28'),
(30, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', '0.18703103', '2019-08-27', '09:26:24', '2019-08-27 16:26:24', '2019-08-27 16:26:24'),
(31, 3, '103.41.26.222', 'Coffee Sign', 'unknown', 'http://royalscrown.com/cofeesign/public/home', '0.28542995', '2019-08-27', '09:29:46', '2019-08-27 16:29:46', '2019-08-27 16:29:46'),
(32, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.22146297', '2019-08-27', '09:35:11', '2019-08-27 16:35:11', '2019-08-27 16:35:11'),
(33, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.19506192', '2019-08-27', '09:36:35', '2019-08-27 16:36:35', '2019-08-27 16:36:35'),
(34, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.21990609', '2019-08-27', '09:49:41', '2019-08-27 16:49:41', '2019-08-27 16:49:41'),
(35, 4, '52.114.6.38', 'Coffee Sign', 'unknown', 'http://royalscrown.com/cofeesign/public/', '0.29074907', '2019-08-27', '09:50:03', '2019-08-27 16:50:03', '2019-08-27 16:50:03'),
(36, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', '0.3296411', '2019-08-27', '09:51:29', '2019-08-27 16:51:29', '2019-08-27 16:51:29'),
(37, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.15383601', '2019-08-27', '09:52:01', '2019-08-27 16:52:01', '2019-08-27 16:52:01'),
(38, 3, '103.41.26.222', 'Sample Lorem Ipsum Text', 'unknown', 'http://royalscrown.com/cofeesign/public/blog/topic/15', '0.46065092', '2019-08-27', '09:55:52', '2019-08-27 16:55:52', '2019-08-27 16:55:52'),
(39, 3, '103.41.26.222', 'Publications Design', 'unknown', 'http://royalscrown.com/cofeesign/public/blog/5', '0.34324789', '2019-08-27', '09:56:05', '2019-08-27 16:56:05', '2019-08-27 16:56:05'),
(40, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.1748991', '2019-08-27', '09:56:23', '2019-08-27 16:56:23', '2019-08-27 16:56:23'),
(41, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics', '0.178159', '2019-08-27', '09:56:32', '2019-08-27 16:56:32', '2019-08-27 16:56:32'),
(42, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/7/edit', '0.22328305', '2019-08-27', '09:56:34', '2019-08-27 16:56:34', '2019-08-27 16:56:34'),
(43, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.2024951', '2019-08-27', '09:56:48', '2019-08-27 16:56:48', '2019-08-27 16:56:48'),
(44, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/8/edit', '0.24563503', '2019-08-27', '09:56:59', '2019-08-27 16:56:59', '2019-08-27 16:56:59'),
(45, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/3/topics/14/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/14/edit', '0.19399118', '2019-08-27', '09:57:38', '2019-08-27 16:57:38', '2019-08-27 16:57:38'),
(46, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', '0.20413804', '2019-08-27', '09:58:22', '2019-08-27 16:58:22', '2019-08-27 16:58:22'),
(47, 3, '103.41.26.222', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog', '0.3483181', '2019-08-27', '09:58:45', '2019-08-27 16:58:45', '2019-08-27 16:58:45'),
(48, 3, '103.41.26.222', 'News', 'unknown', 'http://royalscrown.com/cofeesign/public/news', '0.29851413', '2019-08-27', '09:59:24', '2019-08-27 16:59:24', '2019-08-27 16:59:24'),
(49, 3, '103.41.26.222', 'About Smartend', 'unknown', 'http://royalscrown.com/cofeesign/public/topic/about', '0.28303289', '2019-08-27', '09:59:54', '2019-08-27 16:59:54', '2019-08-27 16:59:54'),
(50, 3, '103.41.26.222', 'Sample Lorem Text', 'unknown', 'http://royalscrown.com/cofeesign/public/services/topic/6', '0.27827382', '2019-08-27', '10:00:07', '2019-08-27 17:00:07', '2019-08-27 17:00:07'),
(51, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/1/topics/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics/1/edit', '0.20703197', '2019-08-27', '10:01:12', '2019-08-27 17:01:12', '2019-08-27 17:01:12'),
(52, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.25893402', '2019-08-27', '10:07:02', '2019-08-27 17:07:02', '2019-08-27 17:07:02'),
(53, 3, '103.41.26.222', 'Privacy', 'unknown', 'http://royalscrown.com/cofeesign/public/topic/privacy', '0.31560612', '2019-08-27', '10:19:51', '2019-08-27 17:19:51', '2019-08-27 17:19:51'),
(54, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', '0.19319296', '2019-08-27', '10:24:46', '2019-08-27 17:24:46', '2019-08-27 17:24:46'),
(55, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/1/edit', '0.17458415', '2019-08-27', '10:24:51', '2019-08-27 17:24:51', '2019-08-27 17:24:51'),
(56, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/2/edit', '0.20054889', '2019-08-27', '10:25:00', '2019-08-27 17:25:00', '2019-08-27 17:25:00'),
(57, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', '0.16204596', '2019-08-27', '10:25:08', '2019-08-27 17:25:08', '2019-08-27 17:25:08'),
(58, 3, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/8/edit', '0.20629716', '2019-08-27', '11:55:52', '2019-08-27 18:55:52', '2019-08-27 18:55:52'),
(59, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.22276306', '2019-08-27', '12:31:30', '2019-08-27 19:31:30', '2019-08-27 19:31:30'),
(60, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.21721196', '2019-08-27', '12:45:46', '2019-08-27 19:45:46', '2019-08-27 19:45:46'),
(61, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', '0.19456697', '2019-08-27', '12:45:48', '2019-08-27 19:45:48', '2019-08-27 19:45:48'),
(62, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/1/edit', '0.19386411', '2019-08-27', '12:45:55', '2019-08-27 19:45:55', '2019-08-27 19:45:55'),
(63, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.21870399', '2019-08-27', '12:46:11', '2019-08-27 19:46:11', '2019-08-27 19:46:11'),
(64, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', '0.20644689', '2019-08-27', '12:46:13', '2019-08-27 19:46:13', '2019-08-27 19:46:13'),
(65, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', '0.23693204', '2019-08-27', '12:58:10', '2019-08-27 19:58:10', '2019-08-27 19:58:10'),
(66, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', '0.20082903', '2019-08-27', '12:58:38', '2019-08-27 19:58:38', '2019-08-27 19:58:38'),
(67, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/17/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/17/edit', '0.26807094', '2019-08-27', '12:59:15', '2019-08-27 19:59:15', '2019-08-27 19:59:15'),
(68, 1, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog', '0.3940289', '2019-08-27', '13:07:09', '2019-08-27 20:07:09', '2019-08-27 20:07:09'),
(69, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/2/edit', '0.18446803', '2019-08-27', '13:08:42', '2019-08-27 20:08:42', '2019-08-27 20:08:42'),
(70, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections', '0.19893813', '2019-08-27', '13:08:51', '2019-08-27 20:08:51', '2019-08-27 20:08:51'),
(71, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', '0.17596602', '2019-08-27', '13:09:08', '2019-08-27 20:09:08', '2019-08-27 20:09:08'),
(72, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', '0.20002699', '2019-08-27', '13:10:02', '2019-08-27 20:10:02', '2019-08-27 20:10:02'),
(73, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', '0.20191312', '2019-08-27', '13:13:22', '2019-08-27 20:13:22', '2019-08-27 20:13:22'),
(74, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/21/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/21/edit', '0.21118093', '2019-08-27', '13:14:45', '2019-08-27 20:14:45', '2019-08-27 20:14:45'),
(75, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/22/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/22/edit', '0.20510006', '2019-08-27', '13:16:16', '2019-08-27 20:16:16', '2019-08-27 20:16:16'),
(76, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.19173598', '2019-08-27', '13:26:09', '2019-08-27 20:26:09', '2019-08-27 20:26:09'),
(77, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/10/edit', '0.20408297', '2019-08-27', '13:26:15', '2019-08-27 20:26:15', '2019-08-27 20:26:15'),
(78, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.17343593', '2019-08-27', '13:26:22', '2019-08-27 20:26:22', '2019-08-27 20:26:22'),
(79, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.19700193', '2019-08-27', '13:26:25', '2019-08-27 20:26:25', '2019-08-27 20:26:25'),
(80, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.18397689', '2019-08-27', '13:26:36', '2019-08-27 20:26:36', '2019-08-27 20:26:36'),
(81, 1, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/3/edit', '0.26582193', '2019-08-27', '13:26:40', '2019-08-27 20:26:40', '2019-08-27 20:26:40'),
(82, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.23597097', '2019-08-28', '05:01:55', '2019-08-28 12:01:55', '2019-08-28 12:01:55'),
(83, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.31486821', '2019-08-28', '05:02:08', '2019-08-28 12:02:08', '2019-08-28 12:02:08'),
(84, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.27214289', '2019-08-28', '05:02:30', '2019-08-28 12:02:30', '2019-08-28 12:02:30'),
(85, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.24368715', '2019-08-28', '05:04:42', '2019-08-28 12:04:42', '2019-08-28 12:04:42'),
(86, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', '0.18364811', '2019-08-28', '05:32:01', '2019-08-28 12:32:01', '2019-08-28 12:32:01'),
(87, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/create', '0.19404101', '2019-08-28', '05:32:09', '2019-08-28 12:32:09', '2019-08-28 12:32:09'),
(88, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/7/edit', '0.27913308', '2019-08-28', '05:34:06', '2019-08-28 12:34:06', '2019-08-28 12:34:06'),
(89, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.229388', '2019-08-28', '05:34:39', '2019-08-28 12:34:39', '2019-08-28 12:34:39'),
(90, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', '0.29358792', '2019-08-28', '05:34:43', '2019-08-28 12:34:43', '2019-08-28 12:34:43'),
(91, 5, '103.41.26.6', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog', '0.41291714', '2019-08-28', '05:42:06', '2019-08-28 12:42:06', '2019-08-28 12:42:06'),
(92, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.18711591', '2019-08-28', '06:18:55', '2019-08-28 13:18:55', '2019-08-28 13:18:55'),
(93, 5, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.21965003', '2019-08-28', '06:41:46', '2019-08-28 13:41:46', '2019-08-28 13:41:46'),
(94, 6, '122.173.132.125', 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', 'unknown', 'http://royalscrown.com/cofeesign/public/esignature-legality-guide-technically-legally-safe', '0.3649559', '2019-08-28', '07:00:39', '2019-08-28 14:00:39', '2019-08-28 14:00:39'),
(95, 6, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog?cat_id=3', '0.36233401', '2019-08-28', '07:20:40', '2019-08-28 14:20:40', '2019-08-28 14:20:40'),
(96, 6, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog?cat_id=2', '0.33955908', '2019-08-28', '07:25:35', '2019-08-28 14:25:35', '2019-08-28 14:25:35'),
(97, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.26887584', '2019-08-28', '07:35:12', '2019-08-28 14:35:12', '2019-08-28 14:35:12'),
(98, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', '0.21586108', '2019-08-28', '07:35:20', '2019-08-28 14:35:20', '2019-08-28 14:35:20'),
(99, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.19796586', '2019-08-28', '07:36:00', '2019-08-28 14:36:00', '2019-08-28 14:36:00'),
(100, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections', '0.20295811', '2019-08-28', '07:36:23', '2019-08-28 14:36:23', '2019-08-28 14:36:23'),
(101, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', '0.18646812', '2019-08-28', '07:36:30', '2019-08-28 14:36:30', '2019-08-28 14:36:30'),
(102, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', '0.19719195', '2019-08-28', '07:36:44', '2019-08-28 14:36:44', '2019-08-28 14:36:44'),
(103, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', '0.19695902', '2019-08-28', '07:37:46', '2019-08-28 14:37:46', '2019-08-28 14:37:46'),
(104, 6, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.22898698', '2019-08-28', '07:38:58', '2019-08-28 14:38:58', '2019-08-28 14:38:58'),
(105, 6, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog?cat_id=5', '0.29479694', '2019-08-28', '08:05:43', '2019-08-28 15:05:43', '2019-08-28 15:05:43'),
(106, 6, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog?cat=5', '0.30064511', '2019-08-28', '08:07:57', '2019-08-28 15:07:57', '2019-08-28 15:07:57'),
(107, 6, '122.173.132.125', 'Blog', 'unknown', 'http://royalscrown.com/cofeesign/public/blog', '0.30291414', '2019-08-28', '08:08:23', '2019-08-28 15:08:23', '2019-08-28 15:08:23'),
(108, 6, '122.173.132.125', 'Publications Design', 'unknown', 'http://royalscrown.com/cofeesign/public/blog/5', '0.30528212', '2019-08-28', '08:09:57', '2019-08-28 15:09:57', '2019-08-28 15:09:57'),
(109, 6, '122.173.132.125', 'case study', 'unknown', 'http://royalscrown.com/cofeesign/public/blog/3', '0.31886411', '2019-08-28', '08:10:22', '2019-08-28 15:10:22', '2019-08-28 15:10:22'),
(110, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.49301004', '2019-08-28', '09:01:22', '2019-08-28 16:01:22', '2019-08-28 16:01:22'),
(111, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.14646292', '2019-08-28', '09:01:27', '2019-08-28 16:01:27', '2019-08-28 16:01:27'),
(112, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', '0.47316408', '2019-08-28', '10:21:05', '2019-08-28 17:21:05', '2019-08-28 17:21:05'),
(113, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', '0.31693506', '2019-08-28', '10:21:10', '2019-08-28 17:21:10', '2019-08-28 17:21:10'),
(114, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', '0.368613', '2019-08-28', '10:21:22', '2019-08-28 17:21:22', '2019-08-28 17:21:22'),
(115, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster', '0.37512493', '2019-08-28', '10:21:22', '2019-08-28 17:21:22', '2019-08-28 17:21:22'),
(116, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', '0.17188001', '2019-08-28', '10:21:32', '2019-08-28 17:21:32', '2019-08-28 17:21:32'),
(117, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.16996503', '2019-08-28', '10:21:36', '2019-08-28 17:21:36', '2019-08-28 17:21:36'),
(118, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.19560099', '2019-08-28', '10:21:38', '2019-08-28 17:21:38', '2019-08-28 17:21:38'),
(119, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.19201303', '2019-08-28', '10:21:54', '2019-08-28 17:21:54', '2019-08-28 17:21:54'),
(120, 7, '103.41.26.222', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', '0.22380304', '2019-08-28', '10:22:04', '2019-08-28 17:22:04', '2019-08-28 17:22:04'),
(121, 8, '103.41.26.246', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.41140914', '2019-08-28', '12:42:52', '2019-08-28 19:42:52', '2019-08-28 19:42:52'),
(122, 8, '103.41.26.246', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.21497202', '2019-08-28', '12:42:57', '2019-08-28 19:42:57', '2019-08-28 19:42:57'),
(123, 8, '103.41.26.246', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.21627998', '2019-08-28', '12:43:01', '2019-08-28 19:43:01', '2019-08-28 19:43:01'),
(124, 8, '103.41.26.246', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', '0.3242588', '2019-08-28', '12:43:04', '2019-08-28 19:43:04', '2019-08-28 19:43:04'),
(125, 8, '103.41.26.246', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.23976493', '2019-08-28', '13:01:25', '2019-08-28 20:01:25', '2019-08-28 20:01:25'),
(126, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.20818591', '2019-08-29', '04:38:12', '2019-08-29 11:38:12', '2019-08-29 11:38:12'),
(127, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.31482792', '2019-08-29', '04:38:39', '2019-08-29 11:38:39', '2019-08-29 11:38:39'),
(128, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.26404309', '2019-08-29', '04:38:45', '2019-08-29 11:38:45', '2019-08-29 11:38:45'),
(129, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.21348715', '2019-08-29', '04:39:41', '2019-08-29 11:39:41', '2019-08-29 11:39:41'),
(130, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/50/edit', '0.21939301', '2019-08-29', '04:39:48', '2019-08-29 11:39:48', '2019-08-29 11:39:48'),
(131, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', '0.23251104', '2019-08-29', '04:43:39', '2019-08-29 11:43:39', '2019-08-29 11:43:39'),
(132, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.14173102', '2019-08-29', '04:45:23', '2019-08-29 11:45:23', '2019-08-29 11:45:23'),
(133, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.21750402', '2019-08-29', '04:51:23', '2019-08-29 11:51:23', '2019-08-29 11:51:23'),
(134, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.22577095', '2019-08-29', '05:02:22', '2019-08-29 12:02:22', '2019-08-29 12:02:22'),
(135, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/contacts', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/contacts', '0.2330761', '2019-08-29', '05:03:10', '2019-08-29 12:03:10', '2019-08-29 12:03:10'),
(136, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.14642', '2019-08-29', '05:45:55', '2019-08-29 12:45:55', '2019-08-29 12:45:55'),
(137, 11, '52.114.6.38', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.17997694', '2019-08-29', '05:51:01', '2019-08-29 12:51:01', '2019-08-29 12:51:01'),
(138, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', '0.22239614', '2019-08-29', '05:51:17', '2019-08-29 12:51:17', '2019-08-29 12:51:17'),
(139, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/3/edit/1', '0.19162989', '2019-08-29', '05:51:20', '2019-08-29 12:51:20', '2019-08-29 12:51:20'),
(140, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.223382', '2019-08-29', '05:51:30', '2019-08-29 12:51:30', '2019-08-29 12:51:30'),
(141, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', '0.20818305', '2019-08-29', '05:51:33', '2019-08-29 12:51:33', '2019-08-29 12:51:33'),
(142, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', '0.18223', '2019-08-29', '05:52:10', '2019-08-29 12:52:10', '2019-08-29 12:52:10'),
(143, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster', '0.36830306', '2019-08-29', '05:52:14', '2019-08-29 12:52:14', '2019-08-29 12:52:14'),
(144, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', '0.36247301', '2019-08-29', '05:52:14', '2019-08-29 12:52:14', '2019-08-29 12:52:14'),
(145, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.18108702', '2019-08-29', '05:52:56', '2019-08-29 12:52:56', '2019-08-29 12:52:56'),
(146, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster?_pjax=%23view', '0.35223007', '2019-08-29', '05:59:12', '2019-08-29 12:59:12', '2019-08-29 12:59:12'),
(147, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster', '0.39085007', '2019-08-29', '05:59:12', '2019-08-29 12:59:12', '2019-08-29 12:59:12'),
(148, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', '0.20706987', '2019-08-29', '06:34:17', '2019-08-29 13:34:17', '2019-08-29 13:34:17'),
(149, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections', '0.18311906', '2019-08-29', '06:40:07', '2019-08-29 13:40:07', '2019-08-29 13:40:07'),
(150, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/51/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/51/edit', '0.25699902', '2019-08-29', '06:44:30', '2019-08-29 13:44:30', '2019-08-29 13:44:30'),
(151, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/52/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/52/edit', '0.20877814', '2019-08-29', '09:08:18', '2019-08-29 16:08:18', '2019-08-29 16:08:18'),
(152, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.20140195', '2019-08-29', '09:11:15', '2019-08-29 16:11:15', '2019-08-29 16:11:15'),
(153, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.20134687', '2019-08-29', '09:16:00', '2019-08-29 16:16:00', '2019-08-29 16:16:00'),
(154, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', '0.20341492', '2019-08-29', '09:16:17', '2019-08-29 16:16:17', '2019-08-29 16:16:17'),
(155, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', '0.20915604', '2019-08-29', '09:24:49', '2019-08-29 16:24:49', '2019-08-29 16:24:49'),
(156, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections', '0.18993306', '2019-08-29', '09:31:32', '2019-08-29 16:31:32', '2019-08-29 16:31:32'),
(157, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/8/edit', '0.19129801', '2019-08-29', '09:31:38', '2019-08-29 16:31:38', '2019-08-29 16:31:38'),
(158, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', '0.60780597', '2019-08-29', '09:32:28', '2019-08-29 16:32:28', '2019-08-29 16:32:28'),
(159, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings', '0.62353992', '2019-08-29', '09:32:28', '2019-08-29 16:32:28', '2019-08-29 16:32:28'),
(160, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin?_pjax=%23view', '0.38205719', '2019-08-29', '09:32:38', '2019-08-29 16:32:38', '2019-08-29 16:32:38'),
(161, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', '0.17125607', '2019-08-29', '09:32:45', '2019-08-29 16:32:45', '2019-08-29 16:32:45'),
(162, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/create/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/create/1', '0.19435692', '2019-08-29', '09:33:17', '2019-08-29 16:33:17', '2019-08-29 16:33:17'),
(163, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners', '0.17211294', '2019-08-29', '10:09:58', '2019-08-29 17:09:58', '2019-08-29 17:09:58'),
(164, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/9/edit', '0.214293', '2019-08-29', '10:10:05', '2019-08-29 17:10:05', '2019-08-29 17:10:05'),
(165, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/create/2', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/create/2', '0.17927504', '2019-08-29', '10:11:39', '2019-08-29 17:11:39', '2019-08-29 17:11:39'),
(166, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/10/edit', '0.17947507', '2019-08-29', '10:12:53', '2019-08-29 17:12:53', '2019-08-29 17:12:53'),
(167, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/create/3', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/create/3', '0.19955397', '2019-08-29', '10:16:25', '2019-08-29 17:16:25', '2019-08-29 17:16:25'),
(168, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/11/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/11/edit', '0.18235898', '2019-08-29', '10:17:19', '2019-08-29 17:17:19', '2019-08-29 17:17:19'),
(169, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.1852541', '2019-08-29', '10:19:07', '2019-08-29 17:19:07', '2019-08-29 17:19:07'),
(170, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/create', '0.19374394', '2019-08-29', '10:19:13', '2019-08-29 17:19:13', '2019-08-29 17:19:13'),
(171, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics/53/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/53/edit', '0.20877409', '2019-08-29', '10:20:27', '2019-08-29 17:20:27', '2019-08-29 17:20:27'),
(172, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.17273712', '2019-08-29', '10:23:40', '2019-08-29 17:23:40', '2019-08-29 17:23:40'),
(173, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/create', '0.3681848', '2019-08-29', '10:23:43', '2019-08-29 17:23:43', '2019-08-29 17:23:43'),
(174, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics/54/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/54/edit', '0.20705295', '2019-08-29', '10:24:32', '2019-08-29 17:24:32', '2019-08-29 17:24:32'),
(175, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', '0.25230122', '2019-08-29', '10:26:48', '2019-08-29 17:26:48', '2019-08-29 17:26:48'),
(176, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.20757794', '2019-08-29', '10:31:20', '2019-08-29 17:31:20', '2019-08-29 17:31:20'),
(177, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics', '0.1696012', '2019-08-29', '10:33:01', '2019-08-29 17:33:01', '2019-08-29 17:33:01'),
(178, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/visitors', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/visitors', '0.18172908', '2019-08-29', '10:33:12', '2019-08-29 17:33:12', '2019-08-29 17:33:12'),
(179, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/1/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics/create', '0.18487096', '2019-08-29', '10:33:21', '2019-08-29 17:33:21', '2019-08-29 17:33:21'),
(180, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/1/topics/55/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics/55/edit', '0.2943809', '2019-08-29', '10:34:01', '2019-08-29 17:34:01', '2019-08-29 17:34:01'),
(181, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', '0.29145503', '2019-08-29', '11:07:27', '2019-08-29 18:07:27', '2019-08-29 18:07:27'),
(182, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus', '0.19105196', '2019-08-29', '11:09:26', '2019-08-29 18:09:26', '2019-08-29 18:09:26'),
(183, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/create/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/create/1', '0.17920685', '2019-08-29', '11:12:27', '2019-08-29 18:12:27', '2019-08-29 18:12:27'),
(184, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/1', '0.21974897', '2019-08-29', '11:15:35', '2019-08-29 18:15:35', '2019-08-29 18:15:35'),
(185, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/19/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/19/edit/1', '0.18248105', '2019-08-29', '11:15:41', '2019-08-29 18:15:41', '2019-08-29 18:15:41'),
(186, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/1?id=19', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/1?id=19', '0.39948297', '2019-08-29', '11:16:15', '2019-08-29 18:16:15', '2019-08-29 18:16:15'),
(187, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/12/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/12/edit/1', '0.17560291', '2019-08-29', '11:17:49', '2019-08-29 18:17:49', '2019-08-29 18:17:49'),
(188, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/users?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/users?_pjax=%23view', '0.1859479', '2019-08-29', '11:18:38', '2019-08-29 18:18:38', '2019-08-29 18:18:38'),
(189, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/users/permissions/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/users/permissions/create', '0.26491714', '2019-08-29', '11:18:43', '2019-08-29 18:18:43', '2019-08-29 18:18:43'),
(190, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/users/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/users/1/edit', '0.17913294', '2019-08-29', '11:19:05', '2019-08-29 18:19:05', '2019-08-29 18:19:05'),
(191, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/22', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/22', '0.18332601', '2019-08-29', '11:20:48', '2019-08-29 18:20:48', '2019-08-29 18:20:48'),
(192, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/create/22', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/create/22', '0.1870079', '2019-08-29', '11:20:57', '2019-08-29 18:20:57', '2019-08-29 18:20:57'),
(193, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/23', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/23', '0.17612982', '2019-08-29', '11:24:02', '2019-08-29 18:24:02', '2019-08-29 18:24:02'),
(194, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/0?id=23', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/0?id=23', '0.18407512', '2019-08-29', '11:24:19', '2019-08-29 18:24:19', '2019-08-29 18:24:19'),
(195, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections?_pjax=%23view', '0.17952394', '2019-08-29', '11:26:30', '2019-08-29 18:26:30', '2019-08-29 18:26:30'),
(196, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/create', '0.19461799', '2019-08-29', '11:26:35', '2019-08-29 18:26:35', '2019-08-29 18:26:35'),
(197, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/7/edit', '0.19902301', '2019-08-29', '11:27:19', '2019-08-29 18:27:19', '2019-08-29 18:27:19'),
(198, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', '0.19054294', '2019-08-29', '11:27:27', '2019-08-29 18:27:27', '2019-08-29 18:27:27'),
(199, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/create', '0.18430805', '2019-08-29', '11:27:31', '2019-08-29 18:27:31', '2019-08-29 18:27:31'),
(200, 9, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', '0.166888', '2019-08-29', '11:27:51', '2019-08-29 18:27:51', '2019-08-29 18:27:51');
INSERT INTO `smartend_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(201, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.19202423', '2019-08-29', '11:33:57', '2019-08-29 18:33:57', '2019-08-29 18:33:57'),
(202, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', '0.23813581', '2019-08-29', '11:34:01', '2019-08-29 18:34:01', '2019-08-29 18:34:01'),
(203, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners?_pjax=%23view', '0.16268015', '2019-08-29', '11:35:02', '2019-08-29 18:35:02', '2019-08-29 18:35:02'),
(204, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/1/edit', '0.16332102', '2019-08-29', '11:35:06', '2019-08-29 18:35:06', '2019-08-29 18:35:06'),
(205, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/banners/3/edit', '0.16057611', '2019-08-29', '11:35:20', '2019-08-29 18:35:20', '2019-08-29 18:35:20'),
(206, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/3/topics/53/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/53/edit', '0.21551895', '2019-08-29', '11:40:41', '2019-08-29 18:40:41', '2019-08-29 18:40:41'),
(207, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.17711401', '2019-08-29', '11:48:09', '2019-08-29 18:48:09', '2019-08-29 18:48:09'),
(208, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', '0.21231413', '2019-08-29', '11:48:16', '2019-08-29 18:48:16', '2019-08-29 18:48:16'),
(209, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', '0.42428398', '2019-08-29', '11:55:57', '2019-08-29 18:55:57', '2019-08-29 18:55:57'),
(210, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings', '0.40895605', '2019-08-29', '11:55:57', '2019-08-29 18:55:57', '2019-08-29 18:55:57'),
(211, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', '0.17220306', '2019-08-29', '11:57:56', '2019-08-29 18:57:56', '2019-08-29 18:57:56'),
(212, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/1/topics/55/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics/55/edit', '0.22436309', '2019-08-29', '11:58:05', '2019-08-29 18:58:05', '2019-08-29 18:58:05'),
(213, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.17459011', '2019-08-29', '11:58:30', '2019-08-29 18:58:30', '2019-08-29 18:58:30'),
(214, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.22323108', '2019-08-29', '11:58:34', '2019-08-29 18:58:34', '2019-08-29 18:58:34'),
(215, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/topics/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/8/edit', '0.20602512', '2019-08-29', '12:03:29', '2019-08-29 19:03:29', '2019-08-29 19:03:29'),
(216, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster/sections/2/edit', '0.17941117', '2019-08-29', '12:09:28', '2019-08-29 19:09:28', '2019-08-29 19:09:28'),
(217, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics', '0.17523885', '2019-08-29', '12:09:56', '2019-08-29 19:09:56', '2019-08-29 19:09:56'),
(218, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/sections?_pjax=%23view', '0.16609693', '2019-08-29', '12:25:03', '2019-08-29 19:25:03', '2019-08-29 19:25:03'),
(219, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/2/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/sections/create', '0.18663216', '2019-08-29', '12:25:05', '2019-08-29 19:25:05', '2019-08-29 19:25:05'),
(220, 10, '103.41.26.6', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', '0.19974494', '2019-08-29', '12:28:12', '2019-08-29 19:28:12', '2019-08-29 19:28:12'),
(221, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.47471309', '2019-08-29', '16:04:13', '2019-08-29 23:04:13', '2019-08-29 23:04:13'),
(222, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.39929891', '2019-08-29', '16:04:27', '2019-08-29 23:04:27', '2019-08-29 23:04:27'),
(223, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.23909092', '2019-08-29', '16:04:34', '2019-08-29 23:04:34', '2019-08-29 23:04:34'),
(224, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.2246809', '2019-08-29', '16:04:42', '2019-08-29 23:04:42', '2019-08-29 23:04:42'),
(225, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.18276906', '2019-08-29', '16:04:50', '2019-08-29 23:04:50', '2019-08-29 23:04:50'),
(226, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/15/edit', '0.26812005', '2019-08-29', '16:04:59', '2019-08-29 23:04:59', '2019-08-29 23:04:59'),
(227, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.17372108', '2019-08-29', '16:14:17', '2019-08-29 23:14:17', '2019-08-29 23:14:17'),
(228, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.20036793', '2019-08-29', '16:14:22', '2019-08-29 23:14:22', '2019-08-29 23:14:22'),
(229, 12, '125.62.111.163', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', '0.28302598', '2019-08-29', '16:14:30', '2019-08-29 23:14:30', '2019-08-29 23:14:30'),
(230, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/login', 'unknown', 'http://royalscrown.com/cofeesign/public/login', '0.21668983', '2019-08-30', '04:31:15', '2019-08-30 11:31:15', '2019-08-30 11:31:15'),
(231, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/webmaster', '0.30386806', '2019-08-30', '04:31:53', '2019-08-30 11:31:53', '2019-08-30 11:31:53'),
(232, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners?_pjax=%23view', '0.2384522', '2019-08-30', '04:32:08', '2019-08-30 11:32:08', '2019-08-30 11:32:08'),
(233, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners/7/edit', '0.19422507', '2019-08-30', '04:32:11', '2019-08-30 11:32:11', '2019-08-30 11:32:11'),
(234, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/banners', '0.23998213', '2019-08-30', '04:32:21', '2019-08-30 11:32:21', '2019-08-30 11:32:21'),
(235, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.20341897', '2019-08-30', '04:32:49', '2019-08-30 11:32:49', '2019-08-30 11:32:49'),
(236, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/16/edit', '0.27363896', '2019-08-30', '04:32:55', '2019-08-30 11:32:55', '2019-08-30 11:32:55'),
(237, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.22209597', '2019-08-30', '04:32:59', '2019-08-30 11:32:59', '2019-08-30 11:32:59'),
(238, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/5/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/5/edit', '0.23471713', '2019-08-30', '04:33:03', '2019-08-30 11:33:03', '2019-08-30 11:33:03'),
(239, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/18/edit', '0.21352506', '2019-08-30', '04:35:16', '2019-08-30 11:35:16', '2019-08-30 11:35:16'),
(240, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/21/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/21/edit', '0.28346109', '2019-08-30', '04:35:51', '2019-08-30 11:35:51', '2019-08-30 11:35:51'),
(241, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/22/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/22/edit', '0.25181603', '2019-08-30', '04:36:20', '2019-08-30 11:36:20', '2019-08-30 11:36:20'),
(242, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics?_pjax=%23view', '0.17569017', '2019-08-30', '04:40:08', '2019-08-30 11:40:08', '2019-08-30 11:40:08'),
(243, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/1/topics/4/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/1/topics/4/edit', '0.2740531', '2019-08-30', '04:40:30', '2019-08-30 11:40:30', '2019-08-30 11:40:30'),
(244, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/sections?_pjax=%23view', '0.16201711', '2019-08-30', '04:41:30', '2019-08-30 11:41:30', '2019-08-30 11:41:30'),
(245, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics?_pjax=%23view', '0.17200518', '2019-08-30', '04:41:36', '2019-08-30 11:41:36', '2019-08-30 11:41:36'),
(246, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/2/topics/6/edit', '0.21973801', '2019-08-30', '04:41:39', '2019-08-30 11:41:39', '2019-08-30 11:41:39'),
(247, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.23065305', '2019-08-30', '04:59:03', '2019-08-30 11:59:03', '2019-08-30 11:59:03'),
(248, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics?_pjax=%23view', '0.19779515', '2019-08-30', '04:59:07', '2019-08-30 11:59:07', '2019-08-30 11:59:07'),
(249, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics/9/edit', '0.22434402', '2019-08-30', '04:59:16', '2019-08-30 11:59:16', '2019-08-30 11:59:16'),
(250, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus?_pjax=%23view', '0.21718597', '2019-08-30', '05:39:13', '2019-08-30 12:39:13', '2019-08-30 12:39:13'),
(251, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/menus/6/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/menus/6/edit/1', '0.21418309', '2019-08-30', '05:39:16', '2019-08-30 12:39:16', '2019-08-30 12:39:16'),
(252, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings', '0.42488599', '2019-08-30', '05:39:29', '2019-08-30 12:39:29', '2019-08-30 12:39:29'),
(253, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/settings?_pjax=%23view', '0.4130609', '2019-08-30', '05:39:29', '2019-08-30 12:39:29', '2019-08-30 12:39:29'),
(254, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/3/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/3/topics', '0.19295001', '2019-08-30', '06:29:41', '2019-08-30 13:29:41', '2019-08-30 13:29:41'),
(255, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin', 'unknown', 'http://royalscrown.com/cofeesign/public/admin', '0.20129204', '2019-08-30', '06:39:28', '2019-08-30 13:39:28', '2019-08-30 13:39:28'),
(256, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics?_pjax=%23view', '0.22710514', '2019-08-30', '06:39:32', '2019-08-30 13:39:32', '2019-08-30 13:39:32'),
(257, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections?_pjax=%23view', '0.17740202', '2019-08-30', '06:39:56', '2019-08-30 13:39:56', '2019-08-30 13:39:56'),
(258, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/create', '0.16855097', '2019-08-30', '06:41:17', '2019-08-30 13:41:17', '2019-08-30 13:41:17'),
(259, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', '0.19235897', '2019-08-30', '06:41:23', '2019-08-30 13:41:23', '2019-08-30 13:41:23'),
(260, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections/3/edit', '0.18514991', '2019-08-30', '06:42:28', '2019-08-30 13:42:28', '2019-08-30 13:42:28'),
(261, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/sections', '0.18167996', '2019-08-30', '06:43:59', '2019-08-30 13:43:59', '2019-08-30 13:43:59'),
(262, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/create', '0.19240904', '2019-08-30', '06:44:37', '2019-08-30 13:44:37', '2019-08-30 13:44:37'),
(263, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics/56/edit', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics/56/edit', '0.20400381', '2019-08-30', '06:46:07', '2019-08-30 13:46:07', '2019-08-30 13:46:07'),
(264, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.23159719', '2019-08-30', '06:50:21', '2019-08-30 13:50:21', '2019-08-30 13:50:21'),
(265, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/uploads/banners/15668980935062.jpg', 'unknown', 'http://royalscrown.com/cofeesign/public/uploads/banners/15668980935062.jpg', '0.17728686', '2019-08-30', '07:26:40', '2019-08-30 14:26:40', '2019-08-30 14:26:40'),
(266, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669116542709.jpg', 'unknown', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669116542709.jpg', '0.75161195', '2019-08-30', '07:27:41', '2019-08-30 14:27:41', '2019-08-30 14:27:41'),
(267, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669118295314.jpg', 'unknown', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669118295314.jpg', '0.76294303', '2019-08-30', '07:27:41', '2019-08-30 14:27:41', '2019-08-30 14:27:41'),
(268, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669114615175.jpg', 'unknown', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669114615175.jpg', '0.76589394', '2019-08-30', '07:27:41', '2019-08-30 14:27:41', '2019-08-30 14:27:41'),
(269, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669117638319.jpg', 'unknown', 'http://royalscrown.com/cofeesign/public/uploads/topics/15669117638319.jpg', '0.75494504', '2019-08-30', '07:27:41', '2019-08-30 14:27:41', '2019-08-30 14:27:41'),
(270, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.42285824', '2019-08-30', '07:34:32', '2019-08-30 14:34:32', '2019-08-30 14:34:32'),
(271, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.33491397', '2019-08-30', '07:34:39', '2019-08-30 14:34:39', '2019-08-30 14:34:39'),
(272, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', '0.29282212', '2019-08-30', '07:34:43', '2019-08-30 14:34:43', '2019-08-30 14:34:43'),
(273, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics', '0.20381618', '2019-08-30', '07:37:33', '2019-08-30 14:37:33', '2019-08-30 14:37:33'),
(274, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.2194159', '2019-08-30', '07:56:30', '2019-08-30 14:56:30', '2019-08-30 14:56:30'),
(275, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.20620894', '2019-08-30', '07:56:35', '2019-08-30 14:56:35', '2019-08-30 14:56:35'),
(276, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', '0.21586514', '2019-08-30', '07:56:38', '2019-08-30 14:56:38', '2019-08-30 14:56:38'),
(277, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.15188193', '2019-08-30', '08:01:08', '2019-08-30 15:01:08', '2019-08-30 15:01:08'),
(278, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/56/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/56/edit', '0.2062819', '2019-08-30', '08:01:21', '2019-08-30 15:01:21', '2019-08-30 15:01:21'),
(279, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.16703916', '2019-08-30', '08:07:36', '2019-08-30 15:07:36', '2019-08-30 15:07:36'),
(280, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', '0.24361706', '2019-08-30', '08:07:40', '2019-08-30 15:07:40', '2019-08-30 15:07:40'),
(281, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.14635396', '2019-08-30', '08:14:11', '2019-08-30 15:14:11', '2019-08-30 15:14:11'),
(282, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/18/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/18/edit', '0.25398993', '2019-08-30', '08:16:14', '2019-08-30 15:16:14', '2019-08-30 15:16:14'),
(283, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/21/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/21/edit', '0.21720505', '2019-08-30', '08:16:36', '2019-08-30 15:16:36', '2019-08-30 15:16:36'),
(284, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', '0.20333791', '2019-08-30', '08:19:55', '2019-08-30 15:19:55', '2019-08-30 15:19:55'),
(285, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.20840192', '2019-08-30', '08:20:16', '2019-08-30 15:20:16', '2019-08-30 15:20:16'),
(286, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/22/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/22/edit', '0.24269295', '2019-08-30', '08:20:30', '2019-08-30 15:20:30', '2019-08-30 15:20:30'),
(287, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/public/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/public/admin/7/topics', '0.14906788', '2019-08-30', '08:42:17', '2019-08-30 15:42:17', '2019-08-30 15:42:17'),
(288, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign//admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign//admin/7/topics', '0.72511005', '2019-08-30', '08:42:25', '2019-08-30 15:42:25', '2019-08-30 15:42:25'),
(289, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics', '0.18966699', '2019-08-30', '08:42:29', '2019-08-30 15:42:29', '2019-08-30 15:42:29'),
(290, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.19204998', '2019-08-30', '08:43:06', '2019-08-30 15:43:06', '2019-08-30 15:43:06'),
(291, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.18196702', '2019-08-30', '08:59:42', '2019-08-30 15:59:42', '2019-08-30 15:59:42'),
(292, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/1/topics/55/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/55/edit', '0.25492501', '2019-08-30', '08:59:50', '2019-08-30 15:59:50', '2019-08-30 15:59:50'),
(293, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/1/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics', '0.17059278', '2019-08-30', '09:00:40', '2019-08-30 16:00:40', '2019-08-30 16:00:40'),
(294, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.18645191', '2019-08-30', '09:59:26', '2019-08-30 16:59:26', '2019-08-30 16:59:26'),
(295, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', '0.21368504', '2019-08-30', '09:59:38', '2019-08-30 16:59:38', '2019-08-30 16:59:38'),
(296, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/10/edit', '0.21094704', '2019-08-30', '10:02:20', '2019-08-30 17:02:20', '2019-08-30 17:02:20'),
(297, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', '0.22346997', '2019-08-30', '10:02:22', '2019-08-30 17:02:22', '2019-08-30 17:02:22'),
(298, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/12/edit', '0.20896101', '2019-08-30', '10:02:25', '2019-08-30 17:02:25', '2019-08-30 17:02:25'),
(299, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics', '0.20115399', '2019-08-30', '10:04:01', '2019-08-30 17:04:01', '2019-08-30 17:04:01'),
(300, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/13/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/13/edit', '0.20751405', '2019-08-30', '10:04:42', '2019-08-30 17:04:42', '2019-08-30 17:04:42'),
(301, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/14/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/14/edit', '0.20895195', '2019-08-30', '10:04:45', '2019-08-30 17:04:45', '2019-08-30 17:04:45'),
(302, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.18772197', '2019-08-30', '10:06:31', '2019-08-30 17:06:31', '2019-08-30 17:06:31'),
(303, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics/14/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/14/edit', '0.21191597', '2019-08-30', '10:06:47', '2019-08-30 17:06:47', '2019-08-30 17:06:47'),
(304, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', '0.2358942', '2019-08-30', '10:07:31', '2019-08-30 17:07:31', '2019-08-30 17:07:31'),
(305, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/10/edit', '0.20178986', '2019-08-30', '10:07:55', '2019-08-30 17:07:55', '2019-08-30 17:07:55'),
(306, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', '0.21400094', '2019-08-30', '10:08:06', '2019-08-30 17:08:06', '2019-08-30 17:08:06'),
(307, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/3/topics/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/12/edit', '0.20855689', '2019-08-30', '10:08:17', '2019-08-30 17:08:17', '2019-08-30 17:08:17'),
(308, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', '0.21503115', '2019-08-30', '10:08:35', '2019-08-30 17:08:35', '2019-08-30 17:08:35'),
(309, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/menus/6/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/6/edit/1', '0.19392991', '2019-08-30', '10:08:38', '2019-08-30 17:08:38', '2019-08-30 17:08:38'),
(310, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.17755699', '2019-08-30', '10:08:49', '2019-08-30 17:08:49', '2019-08-30 17:08:49'),
(311, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/3/edit', '0.19102216', '2019-08-30', '10:08:51', '2019-08-30 17:08:51', '2019-08-30 17:08:51'),
(312, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', '0.21034718', '2019-08-30', '10:10:20', '2019-08-30 17:10:20', '2019-08-30 17:10:20'),
(313, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.16740203', '2019-08-30', '10:18:42', '2019-08-30 17:18:42', '2019-08-30 17:18:42'),
(314, 14, '103.41.26.102', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.20618296', '2019-08-30', '10:24:38', '2019-08-30 17:24:38', '2019-08-30 17:24:38'),
(315, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/create', '0.20822811', '2019-08-30', '10:30:02', '2019-08-30 17:30:02', '2019-08-30 17:30:02'),
(316, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', '0.21972394', '2019-08-30', '10:31:57', '2019-08-30 17:31:57', '2019-08-30 17:31:57'),
(317, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/58/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/58/edit', '0.21346307', '2019-08-30', '10:34:10', '2019-08-30 17:34:10', '2019-08-30 17:34:10'),
(318, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/59/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/59/edit', '0.20828295', '2019-08-30', '10:39:41', '2019-08-30 17:39:41', '2019-08-30 17:39:41'),
(319, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/60/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/60/edit', '0.20896196', '2019-08-30', '10:41:02', '2019-08-30 17:41:02', '2019-08-30 17:41:02'),
(320, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/61/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/61/edit', '0.21028399', '2019-08-30', '10:42:01', '2019-08-30 17:42:01', '2019-08-30 17:42:01'),
(321, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.17333412', '2019-08-30', '11:12:37', '2019-08-30 18:12:37', '2019-08-30 18:12:37'),
(322, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.20474005', '2019-08-30', '11:12:56', '2019-08-30 18:12:56', '2019-08-30 18:12:56'),
(323, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/create/2', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/create/2', '0.19125509', '2019-08-30', '11:13:06', '2019-08-30 18:13:06', '2019-08-30 18:13:06'),
(324, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners', '0.20473313', '2019-08-30', '11:14:31', '2019-08-30 18:14:31', '2019-08-30 18:14:31'),
(325, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', '0.19454217', '2019-08-30', '11:14:41', '2019-08-30 18:14:41', '2019-08-30 18:14:41'),
(326, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.1874311', '2019-08-30', '11:44:29', '2019-08-30 18:44:29', '2019-08-30 18:44:29'),
(327, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/create', '0.18783402', '2019-08-30', '11:44:38', '2019-08-30 18:44:38', '2019-08-30 18:44:38'),
(328, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections', '0.1752739', '2019-08-30', '11:45:43', '2019-08-30 18:45:43', '2019-08-30 18:45:43'),
(329, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/create', '0.21118784', '2019-08-30', '11:47:14', '2019-08-30 18:47:14', '2019-08-30 18:47:14'),
(330, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.18161798', '2019-08-30', '12:31:43', '2019-08-30 19:31:43', '2019-08-30 19:31:43'),
(331, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.2665441', '2019-08-30', '12:31:59', '2019-08-30 19:31:59', '2019-08-30 19:31:59'),
(332, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', '0.3506279', '2019-08-30', '13:04:33', '2019-08-30 20:04:33', '2019-08-30 20:04:33'),
(333, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/50/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/50/edit', '0.20458102', '2019-08-30', '13:04:47', '2019-08-30 20:04:47', '2019-08-30 20:04:47'),
(334, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.17268896', '2019-08-30', '13:06:07', '2019-08-30 20:06:07', '2019-08-30 20:06:07'),
(335, 13, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/admin/1/topics', 'unknown', 'http://royalscrown.com/cofeesign/en/admin/1/topics', '0.19706702', '2019-08-30', '13:06:23', '2019-08-30 20:06:23', '2019-08-30 20:06:23'),
(336, 15, '125.62.105.129', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.22418094', '2019-09-01', '11:05:16', '2019-09-01 18:05:16', '2019-09-01 18:05:16'),
(337, 15, '125.62.105.129', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.33218193', '2019-09-01', '11:05:36', '2019-09-01 18:05:36', '2019-09-01 18:05:36'),
(338, 15, '125.62.105.129', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.24368', '2019-09-01', '11:18:32', '2019-09-01 18:18:32', '2019-09-01 18:18:32'),
(339, 15, '125.62.105.129', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.26822019', '2019-09-01', '11:18:36', '2019-09-01 18:18:36', '2019-09-01 18:18:36'),
(340, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.25353599', '2019-09-02', '04:20:10', '2019-09-02 11:20:10', '2019-09-02 11:20:10'),
(341, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.29755306', '2019-09-02', '04:20:20', '2019-09-02 11:20:20', '2019-09-02 11:20:20'),
(342, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.22938395', '2019-09-02', '04:23:17', '2019-09-02 11:23:17', '2019-09-02 11:23:17'),
(343, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.174546', '2019-09-02', '04:24:22', '2019-09-02 11:24:22', '2019-09-02 11:24:22'),
(344, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.2313652', '2019-09-02', '04:24:47', '2019-09-02 11:24:47', '2019-09-02 11:24:47'),
(345, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', '0.22788501', '2019-09-02', '04:31:02', '2019-09-02 11:31:02', '2019-09-02 11:31:02'),
(346, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/3/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/3/edit/1', '0.28165007', '2019-09-02', '04:31:08', '2019-09-02 11:31:08', '2019-09-02 11:31:08'),
(347, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/create/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/create/1', '0.24763608', '2019-09-02', '04:31:55', '2019-09-02 11:31:55', '2019-09-02 11:31:55'),
(348, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/1', '0.22891402', '2019-09-02', '04:32:45', '2019-09-02 11:32:45', '2019-09-02 11:32:45'),
(349, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/19/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/19/edit/1', '0.17832708', '2019-09-02', '04:32:52', '2019-09-02 11:32:52', '2019-09-02 11:32:52'),
(350, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.18032098', '2019-09-02', '04:33:48', '2019-09-02 11:33:48', '2019-09-02 11:33:48'),
(351, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.173033', '2019-09-02', '04:33:54', '2019-09-02 11:33:54', '2019-09-02 11:33:54'),
(352, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.40714598', '2019-09-02', '04:33:59', '2019-09-02 11:33:59', '2019-09-02 11:33:59'),
(353, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.23214793', '2019-09-02', '04:34:01', '2019-09-02 11:34:01', '2019-09-02 11:34:01'),
(354, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners', '0.17067909', '2019-09-02', '04:37:44', '2019-09-02 11:37:44', '2019-09-02 11:37:44'),
(355, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', '0.18264699', '2019-09-02', '04:52:19', '2019-09-02 11:52:19', '2019-09-02 11:52:19'),
(356, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.32385206', '2019-09-02', '04:53:27', '2019-09-02 11:53:27', '2019-09-02 11:53:27'),
(357, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', '0.43738294', '2019-09-02', '04:53:27', '2019-09-02 11:53:27', '2019-09-02 11:53:27'),
(358, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.17874384', '2019-09-02', '04:53:30', '2019-09-02 11:53:30', '2019-09-02 11:53:30'),
(359, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', '0.57433796', '2019-09-02', '04:53:33', '2019-09-02 11:53:33', '2019-09-02 11:53:33'),
(360, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/3/edit', '0.21455693', '2019-09-02', '04:54:00', '2019-09-02 11:54:00', '2019-09-02 11:54:00'),
(361, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/create', '0.205935', '2019-09-02', '04:54:37', '2019-09-02 11:54:37', '2019-09-02 11:54:37'),
(362, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/create', '0.18070102', '2019-09-02', '04:54:59', '2019-09-02 11:54:59', '2019-09-02 11:54:59'),
(363, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections', '0.59574604', '2019-09-02', '04:55:46', '2019-09-02 11:55:46', '2019-09-02 11:55:46'),
(364, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/12/edit', '0.18052816', '2019-09-02', '04:56:01', '2019-09-02 11:56:01', '2019-09-02 11:56:01'),
(365, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', '0.2413671', '2019-09-02', '04:57:56', '2019-09-02 11:57:56', '2019-09-02 11:57:56'),
(366, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments', '0.23808384', '2019-09-02', '05:12:06', '2019-09-02 12:12:06', '2019-09-02 12:12:06'),
(367, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', '0.19085383', '2019-09-02', '06:27:57', '2019-09-02 13:27:57', '2019-09-02 13:27:57'),
(368, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.1885159', '2019-09-02', '06:28:57', '2019-09-02 13:28:57', '2019-09-02 13:28:57'),
(369, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/create', '0.20490813', '2019-09-02', '06:55:40', '2019-09-02 13:55:40', '2019-09-02 13:55:40'),
(370, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/create', '0.18539906', '2019-09-02', '07:03:09', '2019-09-02 14:03:09', '2019-09-02 14:03:09'),
(371, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/7/edit', '0.22160602', '2019-09-02', '07:03:17', '2019-09-02 14:03:17', '2019-09-02 14:03:17'),
(372, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/8/edit', '0.21074581', '2019-09-02', '07:04:38', '2019-09-02 14:04:38', '2019-09-02 14:04:38'),
(373, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/3/edit', '0.17123008', '2019-09-02', '07:35:59', '2019-09-02 14:35:59', '2019-09-02 14:35:59'),
(374, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', '0.20152092', '2019-09-02', '07:37:55', '2019-09-02 14:37:55', '2019-09-02 14:37:55'),
(375, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/10/edit', '0.22884798', '2019-09-02', '07:38:21', '2019-09-02 14:38:21', '2019-09-02 14:38:21'),
(376, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections/11/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/11/edit', '0.19341993', '2019-09-02', '07:38:34', '2019-09-02 14:38:34', '2019-09-02 14:38:34'),
(377, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/2/edit', '0.17126513', '2019-09-02', '07:45:27', '2019-09-02 14:45:27', '2019-09-02 14:45:27'),
(378, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/services/topics/9', 'unknown', 'http://royalscrown.com/cofeesign/en/services/topics/9', '0.18363786', '2019-09-02', '07:57:53', '2019-09-02 14:57:53', '2019-09-02 14:57:53'),
(379, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', '0.2728281', '2019-09-02', '09:31:19', '2019-09-02 16:31:19', '2019-09-02 16:31:19'),
(380, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/create/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/create/1', '0.22528887', '2019-09-02', '10:16:47', '2019-09-02 17:16:47', '2019-09-02 17:16:47'),
(381, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics', '0.23805213', '2019-09-02', '10:34:08', '2019-09-02 17:34:08', '2019-09-02 17:34:08'),
(382, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections', '0.20230007', '2019-09-02', '10:41:28', '2019-09-02 17:41:28', '2019-09-02 17:41:28'),
(383, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/7/edit', '0.18190789', '2019-09-02', '10:46:10', '2019-09-02 17:46:10', '2019-09-02 17:46:10'),
(384, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/8/edit', '0.22085619', '2019-09-02', '11:14:23', '2019-09-02 18:14:23', '2019-09-02 18:14:23'),
(385, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/1/edit', '0.20701814', '2019-09-02', '11:24:17', '2019-09-02 18:24:17', '2019-09-02 18:24:17'),
(386, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/create', '0.22001481', '2019-09-02', '12:43:44', '2019-09-02 19:43:44', '2019-09-02 19:43:44'),
(387, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/create', '0.20215297', '2019-09-02', '12:43:58', '2019-09-02 19:43:58', '2019-09-02 19:43:58'),
(388, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.25078106', '2019-09-02', '12:49:08', '2019-09-02 19:49:08', '2019-09-02 19:49:08'),
(389, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.18756294', '2019-09-02', '12:49:13', '2019-09-02 19:49:13', '2019-09-02 19:49:13'),
(390, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', '0.18391395', '2019-09-02', '12:49:15', '2019-09-02 19:49:15', '2019-09-02 19:49:15'),
(391, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.1669879', '2019-09-02', '12:49:49', '2019-09-02 19:49:49', '2019-09-02 19:49:49'),
(392, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', '0.16867304', '2019-09-02', '12:49:53', '2019-09-02 19:49:53', '2019-09-02 19:49:53'),
(393, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/8/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics/create', '0.18896413', '2019-09-02', '12:49:57', '2019-09-02 19:49:57', '2019-09-02 19:49:57'),
(394, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', '0.20097494', '2019-09-02', '12:55:39', '2019-09-02 19:55:39', '2019-09-02 19:55:39'),
(395, 16, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', '0.24477601', '2019-09-02', '12:55:52', '2019-09-02 19:55:52', '2019-09-02 19:55:52'),
(396, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.17694306', '2019-09-02', '12:59:32', '2019-09-02 19:59:32', '2019-09-02 19:59:32'),
(397, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.21158504', '2019-09-02', '12:59:35', '2019-09-02 19:59:35', '2019-09-02 19:59:35'),
(398, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.22300005', '2019-09-02', '12:59:45', '2019-09-02 19:59:45', '2019-09-02 19:59:45'),
(399, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/7/edit', '0.18569708', '2019-09-02', '13:01:15', '2019-09-02 20:01:15', '2019-09-02 20:01:15'),
(400, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', '0.17717195', '2019-09-02', '13:03:46', '2019-09-02 20:03:46', '2019-09-02 20:03:46'),
(401, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', '0.35294509', '2019-09-02', '13:04:32', '2019-09-02 20:04:32', '2019-09-02 20:04:32');
INSERT INTO `smartend_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(402, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/2/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections', '0.18850017', '2019-09-02', '13:05:09', '2019-09-02 20:05:09', '2019-09-02 20:05:09'),
(403, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics', '0.17663598', '2019-09-02', '13:10:38', '2019-09-02 20:10:38', '2019-09-02 20:10:38'),
(404, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', '0.16409612', '2019-09-02', '13:11:52', '2019-09-02 20:11:52', '2019-09-02 20:11:52'),
(405, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/admin/payments/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/1/edit', '0.18155003', '2019-09-02', '13:11:56', '2019-09-02 20:11:56', '2019-09-02 20:11:56'),
(406, 17, '103.41.26.12', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.15116286', '2019-09-02', '13:23:34', '2019-09-02 20:23:34', '2019-09-02 20:23:34'),
(407, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.25582099', '2019-09-03', '03:47:22', '2019-09-03 10:47:22', '2019-09-03 10:47:22'),
(408, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.21145701', '2019-09-03', '03:47:30', '2019-09-03 10:47:30', '2019-09-03 10:47:30'),
(409, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.26336408', '2019-09-03', '04:15:10', '2019-09-03 11:15:10', '2019-09-03 11:15:10'),
(410, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.36436987', '2019-09-03', '04:15:15', '2019-09-03 11:15:15', '2019-09-03 11:15:15'),
(411, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', '0.34812808', '2019-09-03', '04:15:15', '2019-09-03 11:15:15', '2019-09-03 11:15:15'),
(412, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', '0.34892297', '2019-09-03', '05:46:16', '2019-09-03 12:46:16', '2019-09-03 12:46:16'),
(413, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/banners/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners/1/edit', '0.23991108', '2019-09-03', '05:46:21', '2019-09-03 12:46:21', '2019-09-03 12:46:21'),
(414, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/banners/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners/create', '0.17510414', '2019-09-03', '05:46:33', '2019-09-03 12:46:33', '2019-09-03 12:46:33'),
(415, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.1815691', '2019-09-03', '05:46:40', '2019-09-03 12:46:40', '2019-09-03 12:46:40'),
(416, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/1/edit', '0.28396702', '2019-09-03', '05:46:43', '2019-09-03 12:46:43', '2019-09-03 12:46:43'),
(417, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', '0.19743395', '2019-09-03', '05:47:37', '2019-09-03 12:47:37', '2019-09-03 12:47:37'),
(418, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.3489151', '2019-09-03', '06:07:13', '2019-09-03 13:07:13', '2019-09-03 13:07:13'),
(419, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.23870301', '2019-09-03', '06:07:15', '2019-09-03 13:07:15', '2019-09-03 13:07:15'),
(420, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/create', '0.21338606', '2019-09-03', '06:07:17', '2019-09-03 13:07:17', '2019-09-03 13:07:17'),
(421, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.22174001', '2019-09-03', '06:07:27', '2019-09-03 13:07:27', '2019-09-03 13:07:27'),
(422, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/create', '0.2037549', '2019-09-03', '06:07:29', '2019-09-03 13:07:29', '2019-09-03 13:07:29'),
(423, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', '0.20874', '2019-09-03', '06:09:47', '2019-09-03 13:09:47', '2019-09-03 13:09:47'),
(424, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments', '0.16431189', '2019-09-03', '06:09:57', '2019-09-03 13:09:57', '2019-09-03 13:09:57'),
(425, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/create', '0.19984198', '2019-09-03', '06:10:12', '2019-09-03 13:10:12', '2019-09-03 13:10:12'),
(426, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.1646018', '2019-09-03', '06:19:29', '2019-09-03 13:19:29', '2019-09-03 13:19:29'),
(427, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/403', 'unknown', 'http://royalscrown.com/cofeesign/admin/403', '0.15423799', '2019-09-03', '06:19:39', '2019-09-03 13:19:39', '2019-09-03 13:19:39'),
(428, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.22556806', '2019-09-03', '06:19:49', '2019-09-03 13:19:49', '2019-09-03 13:19:49'),
(429, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.17988205', '2019-09-03', '06:20:17', '2019-09-03 13:20:17', '2019-09-03 13:20:17'),
(430, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', '0.22524595', '2019-09-03', '06:24:39', '2019-09-03 13:24:39', '2019-09-03 13:24:39'),
(431, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/5/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/5/edit', '0.22012305', '2019-09-03', '06:26:30', '2019-09-03 13:26:30', '2019-09-03 13:26:30'),
(432, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.1673162', '2019-09-03', '06:31:50', '2019-09-03 13:31:50', '2019-09-03 13:31:50'),
(433, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', '0.16589403', '2019-09-03', '06:31:52', '2019-09-03 13:31:52', '2019-09-03 13:31:52'),
(434, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.16992188', '2019-09-03', '06:32:45', '2019-09-03 13:32:45', '2019-09-03 13:32:45'),
(435, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', '0.176054', '2019-09-03', '06:32:51', '2019-09-03 13:32:51', '2019-09-03 13:32:51'),
(436, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/8/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics/create', '0.18765306', '2019-09-03', '06:32:54', '2019-09-03 13:32:54', '2019-09-03 13:32:54'),
(437, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/8/topics/62/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics/62/edit', '0.22577381', '2019-09-03', '06:33:49', '2019-09-03 13:33:49', '2019-09-03 13:33:49'),
(438, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.17380905', '2019-09-03', '06:36:32', '2019-09-03 13:36:32', '2019-09-03 13:36:32'),
(439, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/1/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/create', '0.19474292', '2019-09-03', '06:36:34', '2019-09-03 13:36:34', '2019-09-03 13:36:34'),
(440, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/1/topics/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/2/edit', '0.24302793', '2019-09-03', '06:36:44', '2019-09-03 13:36:44', '2019-09-03 13:36:44'),
(441, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/8/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/8/edit', '0.17887712', '2019-09-03', '06:37:09', '2019-09-03 13:37:09', '2019-09-03 13:37:09'),
(442, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/users/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/users/1/edit', '0.22307205', '2019-09-03', '07:12:57', '2019-09-03 14:12:57', '2019-09-03 14:12:57'),
(443, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/403', 'unknown', 'http://royalscrown.com/cofeesign/admin/403', '0.14228702', '2019-09-03', '07:15:37', '2019-09-03 14:15:37', '2019-09-03 14:15:37'),
(444, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.19735813', '2019-09-03', '07:16:59', '2019-09-03 14:16:59', '2019-09-03 14:16:59'),
(445, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/8/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics', '0.16759706', '2019-09-03', '07:27:08', '2019-09-03 14:27:08', '2019-09-03 14:27:08'),
(446, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.19274306', '2019-09-03', '07:29:53', '2019-09-03 14:29:53', '2019-09-03 14:29:53'),
(447, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.30875897', '2019-09-03', '07:30:02', '2019-09-03 14:30:02', '2019-09-03 14:30:02'),
(448, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', '0.27393484', '2019-09-03', '07:35:22', '2019-09-03 14:35:22', '2019-09-03 14:35:22'),
(449, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', '0.22011089', '2019-09-03', '07:43:04', '2019-09-03 14:43:04', '2019-09-03 14:43:04'),
(450, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', '0.17008901', '2019-09-03', '08:02:05', '2019-09-03 15:02:05', '2019-09-03 15:02:05'),
(451, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/8/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics/create', '0.20493412', '2019-09-03', '08:02:07', '2019-09-03 15:02:07', '2019-09-03 15:02:07'),
(452, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics', '0.186064', '2019-09-03', '09:03:27', '2019-09-03 16:03:27', '2019-09-03 16:03:27'),
(453, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/2/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/create', '0.22186899', '2019-09-03', '09:12:34', '2019-09-03 16:12:34', '2019-09-03 16:12:34'),
(454, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/63/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/63/edit', '0.21078491', '2019-09-03', '09:20:49', '2019-09-03 16:20:49', '2019-09-03 16:20:49'),
(455, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/64/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/64/edit', '0.23933387', '2019-09-03', '09:24:06', '2019-09-03 16:24:06', '2019-09-03 16:24:06'),
(456, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.20325899', '2019-09-03', '09:26:26', '2019-09-03 16:26:26', '2019-09-03 16:26:26'),
(457, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/65/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/65/edit', '0.30155802', '2019-09-03', '09:44:52', '2019-09-03 16:44:52', '2019-09-03 16:44:52'),
(458, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/create/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/create/1', '0.20693898', '2019-09-03', '09:45:51', '2019-09-03 16:45:51', '2019-09-03 16:45:51'),
(459, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.23672485', '2019-09-03', '09:46:00', '2019-09-03 16:46:00', '2019-09-03 16:46:00'),
(460, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/create', '0.25906801', '2019-09-03', '09:46:02', '2019-09-03 16:46:02', '2019-09-03 16:46:02'),
(461, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.2006979', '2019-09-03', '09:46:10', '2019-09-03 16:46:10', '2019-09-03 16:46:10'),
(462, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/create', '0.20991516', '2019-09-03', '09:46:12', '2019-09-03 16:46:12', '2019-09-03 16:46:12'),
(463, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.22971082', '2019-09-03', '09:48:51', '2019-09-03 16:48:51', '2019-09-03 16:48:51'),
(464, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', '0.32225299', '2019-09-03', '09:51:56', '2019-09-03 16:51:56', '2019-09-03 16:51:56'),
(465, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', '0.33924484', '2019-09-03', '09:51:56', '2019-09-03 16:51:56', '2019-09-03 16:51:56'),
(466, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/8/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics', '0.26886797', '2019-09-03', '10:31:41', '2019-09-03 17:31:41', '2019-09-03 17:31:41'),
(467, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections', '0.20942211', '2019-09-03', '11:02:31', '2019-09-03 18:02:31', '2019-09-03 18:02:31'),
(468, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', '0.18531394', '2019-09-03', '11:03:05', '2019-09-03 18:03:05', '2019-09-03 18:03:05'),
(469, 19, '103.41.26.132', 'http://royalscrown.com/cofeesign/admin/2/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics', '0.19448614', '2019-09-03', '11:08:06', '2019-09-03 18:08:06', '2019-09-03 18:08:06'),
(470, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/15/edit', '0.25325894', '2019-09-03', '12:03:49', '2019-09-03 19:03:49', '2019-09-03 19:03:49'),
(471, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/en/blog/topic/56', 'unknown', 'http://royalscrown.com/cofeesign/en/en/blog/topic/56', '0.29148602', '2019-09-03', '13:29:10', '2019-09-03 20:29:10', '2019-09-03 20:29:10'),
(472, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/en/blog/topic/60', 'unknown', 'http://royalscrown.com/cofeesign/en/en/blog/topic/60', '0.28909707', '2019-09-03', '13:29:24', '2019-09-03 20:29:24', '2019-09-03 20:29:24'),
(473, 18, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/en/blog/topic/58', 'unknown', 'http://royalscrown.com/cofeesign/en/en/blog/topic/58', '0.28676295', '2019-09-03', '13:32:01', '2019-09-03 20:32:01', '2019-09-03 20:32:01'),
(474, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.25566006', '2019-09-04', '04:15:16', '2019-09-04 11:15:16', '2019-09-04 11:15:16'),
(475, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.25710297', '2019-09-04', '04:15:20', '2019-09-04 11:15:20', '2019-09-04 11:15:20'),
(476, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.20808911', '2019-09-04', '04:15:47', '2019-09-04 11:15:47', '2019-09-04 11:15:47'),
(477, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.26848292', '2019-09-04', '04:15:50', '2019-09-04 11:15:50', '2019-09-04 11:15:50'),
(478, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', '0.20601296', '2019-09-04', '04:15:54', '2019-09-04 11:15:54', '2019-09-04 11:15:54'),
(479, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', '0.16943312', '2019-09-04', '04:15:54', '2019-09-04 11:15:54', '2019-09-04 11:15:54'),
(480, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', '0.52959204', '2019-09-04', '04:18:04', '2019-09-04 11:18:04', '2019-09-04 11:18:04'),
(481, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections', '0.24109793', '2019-09-04', '04:20:24', '2019-09-04 11:20:24', '2019-09-04 11:20:24'),
(482, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.13963985', '2019-09-04', '04:21:45', '2019-09-04 11:21:45', '2019-09-04 11:21:45'),
(483, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.20200181', '2019-09-04', '04:21:56', '2019-09-04 11:21:56', '2019-09-04 11:21:56'),
(484, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.17514515', '2019-09-04', '04:22:16', '2019-09-04 11:22:16', '2019-09-04 11:22:16'),
(485, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/2/edit', '0.18861699', '2019-09-04', '04:22:18', '2019-09-04 11:22:18', '2019-09-04 11:22:18'),
(486, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.17952991', '2019-09-04', '04:22:39', '2019-09-04 11:22:39', '2019-09-04 11:22:39'),
(487, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.17772102', '2019-09-04', '04:22:41', '2019-09-04 11:22:41', '2019-09-04 11:22:41'),
(488, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.17490292', '2019-09-04', '04:22:47', '2019-09-04 11:22:47', '2019-09-04 11:22:47'),
(489, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', '0.17828798', '2019-09-04', '04:22:58', '2019-09-04 11:22:58', '2019-09-04 11:22:58'),
(490, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.20609283', '2019-09-04', '04:41:41', '2019-09-04 11:41:41', '2019-09-04 11:41:41'),
(491, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/edit', '0.264709', '2019-09-04', '04:41:52', '2019-09-04 11:41:52', '2019-09-04 11:41:52'),
(492, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/close-icon-small.svg', '0.35267997', '2019-09-04', '04:42:00', '2019-09-04 11:42:00', '2019-09-04 11:42:00'),
(493, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/6/img/svg/hint.svg', '0.34521508', '2019-09-04', '04:42:00', '2019-09-04 11:42:00', '2019-09-04 11:42:00'),
(494, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.5123148', '2019-09-04', '04:49:11', '2019-09-04 11:49:11', '2019-09-04 11:49:11'),
(495, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/7/edit', '0.20266795', '2019-09-04', '04:49:14', '2019-09-04 11:49:14', '2019-09-04 11:49:14'),
(496, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/img/svg/hint.svg', 'unknown', 'http://royalscrown.com/cofeesign/en/img/svg/hint.svg', '0.29493284', '2019-09-04', '05:07:12', '2019-09-04 12:07:12', '2019-09-04 12:07:12'),
(497, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/img/svg/close-icon-small.svg', 'unknown', 'http://royalscrown.com/cofeesign/en/img/svg/close-icon-small.svg', '0.28858304', '2019-09-04', '05:07:12', '2019-09-04 12:07:12', '2019-09-04 12:07:12'),
(498, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.15063596', '2019-09-04', '05:08:33', '2019-09-04 12:08:33', '2019-09-04 12:08:33'),
(499, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/topics/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics/7/edit', '0.30331397', '2019-09-04', '05:09:36', '2019-09-04 12:09:36', '2019-09-04 12:09:36'),
(500, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.18881607', '2019-09-04', '05:19:22', '2019-09-04 12:19:22', '2019-09-04 12:19:22'),
(501, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://royalscrown.com/cofeesign/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.15458298', '2019-09-04', '05:20:00', '2019-09-04 12:20:00', '2019-09-04 12:20:00'),
(502, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/users/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/users/1/edit', '0.1871779', '2019-09-04', '05:23:58', '2019-09-04 12:23:58', '2019-09-04 12:23:58'),
(503, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.57686496', '2019-09-04', '05:34:29', '2019-09-04 12:34:29', '2019-09-04 12:34:29'),
(504, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics?_pjax=%23view', '0.20153189', '2019-09-04', '05:34:32', '2019-09-04 12:34:32', '2019-09-04 12:34:32'),
(505, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.22920418', '2019-09-04', '05:36:02', '2019-09-04 12:36:02', '2019-09-04 12:36:02'),
(506, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/8/topics?_pjax=%23view', '0.17257595', '2019-09-04', '05:36:11', '2019-09-04 12:36:11', '2019-09-04 12:36:11'),
(507, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners?_pjax=%23view', '0.17745113', '2019-09-04', '05:36:15', '2019-09-04 12:36:15', '2019-09-04 12:36:15'),
(508, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/create/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/create/1', '0.24157119', '2019-09-04', '05:36:19', '2019-09-04 12:36:19', '2019-09-04 12:36:19'),
(509, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/7/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/7/edit', '0.20506406', '2019-09-04', '05:36:27', '2019-09-04 12:36:27', '2019-09-04 12:36:27'),
(510, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', '0.19313097', '2019-09-04', '05:40:41', '2019-09-04 12:40:41', '2019-09-04 12:40:41'),
(511, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners', '0.19597483', '2019-09-04', '05:41:58', '2019-09-04 12:41:58', '2019-09-04 12:41:58'),
(512, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/banners/13/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/13/edit', '0.21628213', '2019-09-04', '05:42:02', '2019-09-04 12:42:02', '2019-09-04 12:42:02'),
(513, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/banners', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners', '0.17371202', '2019-09-04', '05:44:38', '2019-09-04 12:44:38', '2019-09-04 12:44:38'),
(514, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/banners/12/edit', '0.18032598', '2019-09-04', '05:44:41', '2019-09-04 12:44:41', '2019-09-04 12:44:41'),
(515, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections?_pjax=%23view', '0.18680406', '2019-09-04', '05:51:16', '2019-09-04 12:51:16', '2019-09-04 12:51:16'),
(516, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', '0.3140862', '2019-09-04', '05:51:46', '2019-09-04 12:51:46', '2019-09-04 12:51:46'),
(517, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/2/sections/10/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/10/edit', '0.182688', '2019-09-04', '05:53:27', '2019-09-04 12:53:27', '2019-09-04 12:53:27'),
(518, 22, '52.114.6.38', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.15698195', '2019-09-04', '05:54:06', '2019-09-04 12:54:06', '2019-09-04 12:54:06'),
(519, 23, '108.65.78.171', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.1265409', '2019-09-04', '05:54:58', '2019-09-04 12:54:58', '2019-09-04 12:54:58'),
(520, 23, '108.65.78.171', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.21152806', '2019-09-04', '05:55:15', '2019-09-04 12:55:15', '2019-09-04 12:55:15'),
(521, 23, '108.65.78.171', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.17270398', '2019-09-04', '05:55:38', '2019-09-04 12:55:38', '2019-09-04 12:55:38'),
(522, 23, '108.65.78.171', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.17911601', '2019-09-04', '05:55:47', '2019-09-04 12:55:47', '2019-09-04 12:55:47'),
(523, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', '0.24685907', '2019-09-04', '06:00:42', '2019-09-04 13:00:42', '2019-09-04 13:00:42'),
(524, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus', '0.18146205', '2019-09-04', '06:00:58', '2019-09-04 13:00:58', '2019-09-04 13:00:58'),
(525, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics?_pjax=%23view', '0.17605805', '2019-09-04', '06:02:19', '2019-09-04 13:02:19', '2019-09-04 13:02:19'),
(526, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.20252705', '2019-09-04', '06:08:51', '2019-09-04 13:08:51', '2019-09-04 13:08:51'),
(527, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.35308313', '2019-09-04', '06:10:22', '2019-09-04 13:10:22', '2019-09-04 13:10:22'),
(528, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', '0.34781098', '2019-09-04', '06:10:22', '2019-09-04 13:10:22', '2019-09-04 13:10:22'),
(529, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', '0.17731714', '2019-09-04', '06:10:43', '2019-09-04 13:10:43', '2019-09-04 13:10:43'),
(530, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/banners', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners', '0.22586083', '2019-09-04', '06:10:55', '2019-09-04 13:10:55', '2019-09-04 13:10:55'),
(531, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/admin/settings', '0.51482797', '2019-09-04', '06:11:06', '2019-09-04 13:11:06', '2019-09-04 13:11:06'),
(532, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/settings?_pjax=%23view', '0.52793503', '2019-09-04', '06:11:06', '2019-09-04 13:11:06', '2019-09-04 13:11:06'),
(533, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/1/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics', '0.17827988', '2019-09-04', '06:11:59', '2019-09-04 13:11:59', '2019-09-04 13:11:59'),
(534, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/1/topics/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/1/topics/1/edit', '0.24116492', '2019-09-04', '06:13:07', '2019-09-04 13:13:07', '2019-09-04 13:13:07'),
(535, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', '0.19906688', '2019-09-04', '06:13:52', '2019-09-04 13:13:52', '2019-09-04 13:13:52'),
(536, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus/12/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/12/edit/1', '0.21971607', '2019-09-04', '06:15:32', '2019-09-04 13:15:32', '2019-09-04 13:15:32'),
(537, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/1', '0.2110219', '2019-09-04', '06:15:38', '2019-09-04 13:15:38', '2019-09-04 13:15:38'),
(538, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/404', 'unknown', 'http://royalscrown.com/cofeesign/admin/404', '0.178859', '2019-09-04', '06:34:15', '2019-09-04 13:34:15', '2019-09-04 13:34:15'),
(539, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics', '0.17816281', '2019-09-04', '06:34:21', '2019-09-04 13:34:21', '2019-09-04 13:34:21'),
(540, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments?_pjax=%23view', '0.21009302', '2019-09-04', '06:34:23', '2019-09-04 13:34:23', '2019-09-04 13:34:23'),
(541, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/create', '0.20317101', '2019-09-04', '06:34:42', '2019-09-04 13:34:42', '2019-09-04 13:34:42'),
(542, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/create', '0.18751192', '2019-09-04', '06:35:09', '2019-09-04 13:35:09', '2019-09-04 13:35:09'),
(543, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/3/edit', '0.18321705', '2019-09-04', '06:35:53', '2019-09-04 13:35:53', '2019-09-04 13:35:53'),
(544, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/sections/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/sections/12/edit', '0.18088818', '2019-09-04', '06:36:16', '2019-09-04 13:36:16', '2019-09-04 13:36:16'),
(545, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/4/edit', '0.22001004', '2019-09-04', '06:36:53', '2019-09-04 13:36:53', '2019-09-04 13:36:53'),
(546, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/5/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/5/edit', '0.17957401', '2019-09-04', '06:37:06', '2019-09-04 13:37:06', '2019-09-04 13:37:06'),
(547, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/create', '0.26489019', '2019-09-04', '06:38:01', '2019-09-04 13:38:01', '2019-09-04 13:38:01'),
(548, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/1/edit', '0.18375015', '2019-09-04', '06:53:05', '2019-09-04 13:53:05', '2019-09-04 13:53:05'),
(549, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/users', 'unknown', 'http://royalscrown.com/cofeesign/admin/users', '0.1733861', '2019-09-04', '06:57:34', '2019-09-04 13:57:34', '2019-09-04 13:57:34'),
(550, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/users?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/users?_pjax=%23view', '0.17960596', '2019-09-04', '06:59:10', '2019-09-04 13:59:10', '2019-09-04 13:59:10'),
(551, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags', '0.16305304', '2019-09-04', '07:08:42', '2019-09-04 14:08:42', '2019-09-04 14:08:42'),
(552, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments/create', '0.20994401', '2019-09-04', '07:08:50', '2019-09-04 14:08:50', '2019-09-04 14:08:50'),
(553, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags?_pjax=%23view', '0.15855598', '2019-09-04', '07:08:55', '2019-09-04 14:08:55', '2019-09-04 14:08:55'),
(554, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/users/12/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/users/12/edit', '0.16871595', '2019-09-04', '07:10:43', '2019-09-04 14:10:43', '2019-09-04 14:10:43'),
(555, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/tags?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags?_pjax=%23view', '0.16283202', '2019-09-04', '07:12:57', '2019-09-04 14:12:57', '2019-09-04 14:12:57'),
(556, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', '0.41582394', '2019-09-04', '07:13:10', '2019-09-04 14:13:10', '2019-09-04 14:13:10'),
(557, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags/create', '0.20441604', '2019-09-04', '07:16:41', '2019-09-04 14:16:41', '2019-09-04 14:16:41'),
(558, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/payments', 'unknown', 'http://royalscrown.com/cofeesign/admin/payments', '0.21007681', '2019-09-04', '07:18:31', '2019-09-04 14:18:31', '2019-09-04 14:18:31'),
(559, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus/4/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/4/edit/1', '0.18069196', '2019-09-04', '07:18:49', '2019-09-04 14:18:49', '2019-09-04 14:18:49'),
(560, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus/11/edit/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/11/edit/1', '0.18190694', '2019-09-04', '07:18:55', '2019-09-04 14:18:55', '2019-09-04 14:18:55'),
(561, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/menus/1?id=11', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/1?id=11', '0.18103814', '2019-09-04', '07:19:08', '2019-09-04 14:19:08', '2019-09-04 14:19:08'),
(562, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags/1/edit', '0.17937994', '2019-09-04', '07:28:01', '2019-09-04 14:28:01', '2019-09-04 14:28:01'),
(563, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/tags/1/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags/1/edit', '0.17384315', '2019-09-04', '07:29:34', '2019-09-04 14:29:34', '2019-09-04 14:29:34'),
(564, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/16/edit', '0.24611592', '2019-09-04', '07:29:45', '2019-09-04 14:29:45', '2019-09-04 14:29:45'),
(565, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags/3/edit', '0.18341804', '2019-09-04', '07:30:19', '2019-09-04 14:30:19', '2019-09-04 14:30:19'),
(566, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/tags/4/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/tags/4/edit', '0.17051721', '2019-09-04', '07:46:20', '2019-09-04 14:46:20', '2019-09-04 14:46:20'),
(567, 21, '103.41.26.180', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/3/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/3/edit', '0.22185516', '2019-09-04', '07:53:25', '2019-09-04 14:53:25', '2019-09-04 14:53:25'),
(568, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics', '0.21396708', '2019-09-04', '08:58:38', '2019-09-04 15:58:38', '2019-09-04 15:58:38'),
(569, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/66/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/66/edit', '0.29449081', '2019-09-04', '09:11:02', '2019-09-04 16:11:02', '2019-09-04 16:11:02'),
(570, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/67/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/67/edit', '0.22681808', '2019-09-04', '09:13:06', '2019-09-04 16:13:06', '2019-09-04 16:13:06'),
(571, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/68/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/68/edit', '0.25271893', '2019-09-04', '09:16:14', '2019-09-04 16:16:14', '2019-09-04 16:16:14'),
(572, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/69/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/69/edit', '0.23699498', '2019-09-04', '09:17:06', '2019-09-04 16:17:06', '2019-09-04 16:17:06'),
(573, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', '0.39910507', '2019-09-04', '09:24:39', '2019-09-04 16:24:39', '2019-09-04 16:24:39'),
(574, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus?_pjax=%23view', '0.21384907', '2019-09-04', '09:27:11', '2019-09-04 16:27:11', '2019-09-04 16:27:11'),
(575, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus', '0.19821501', '2019-09-04', '09:27:52', '2019-09-04 16:27:52', '2019-09-04 16:27:52'),
(576, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/2', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/2', '0.19723296', '2019-09-04', '09:28:03', '2019-09-04 16:28:03', '2019-09-04 16:28:03'),
(577, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/menus/1', 'unknown', 'http://royalscrown.com/cofeesign/admin/menus/1', '0.20544481', '2019-09-04', '09:28:11', '2019-09-04 16:28:11', '2019-09-04 16:28:11'),
(578, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/users?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/users?_pjax=%23view', '0.20802999', '2019-09-04', '09:28:27', '2019-09-04 16:28:27', '2019-09-04 16:28:27'),
(579, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections?_pjax=%23view', '0.19194698', '2019-09-04', '09:28:31', '2019-09-04 16:28:31', '2019-09-04 16:28:31'),
(580, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/banners?_pjax=%23view', '0.20211601', '2019-09-04', '09:28:41', '2019-09-04 16:28:41', '2019-09-04 16:28:41'),
(581, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster', '0.359586', '2019-09-04', '09:28:46', '2019-09-04 16:28:46', '2019-09-04 16:28:46'),
(582, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster?_pjax=%23view', '0.35961986', '2019-09-04', '09:28:46', '2019-09-04 16:28:46', '2019-09-04 16:28:46'),
(583, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/settings', 'unknown', 'http://royalscrown.com/cofeesign/admin/settings', '0.41414404', '2019-09-04', '09:29:50', '2019-09-04 16:29:50', '2019-09-04 16:29:50'),
(584, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/settings?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/settings?_pjax=%23view', '0.43387222', '2019-09-04', '09:29:50', '2019-09-04 16:29:50', '2019-09-04 16:29:50'),
(585, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections/create', '0.17672086', '2019-09-04', '09:32:53', '2019-09-04 16:32:53', '2019-09-04 16:32:53'),
(586, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/webmaster/sections', '0.17000318', '2019-09-04', '09:33:34', '2019-09-04 16:33:34', '2019-09-04 16:33:34'),
(587, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/9/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/9/topics?_pjax=%23view', '0.17246819', '2019-09-04', '09:33:39', '2019-09-04 16:33:39', '2019-09-04 16:33:39'),
(588, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', '0.22097182', '2019-09-04', '10:20:35', '2019-09-04 17:20:35', '2019-09-04 17:20:35'),
(589, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/50/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/50/edit', '0.32188082', '2019-09-04', '10:21:04', '2019-09-04 17:21:04', '2019-09-04 17:21:04'),
(590, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/56/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/56/edit', '0.25536013', '2019-09-04', '10:21:18', '2019-09-04 17:21:18', '2019-09-04 17:21:18'),
(591, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/70/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/70/edit', '0.26549101', '2019-09-04', '10:57:43', '2019-09-04 17:57:43', '2019-09-04 17:57:43'),
(592, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/71/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/71/edit', '0.23368216', '2019-09-04', '11:00:15', '2019-09-04 18:00:15', '2019-09-04 18:00:15'),
(593, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/57/edit', '0.28140712', '2019-09-04', '11:01:25', '2019-09-04 18:01:25', '2019-09-04 18:01:25'),
(594, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/58/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/58/edit', '0.23041511', '2019-09-04', '11:01:40', '2019-09-04 18:01:40', '2019-09-04 18:01:40'),
(595, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/59/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/59/edit', '0.26424098', '2019-09-04', '11:02:03', '2019-09-04 18:02:03', '2019-09-04 18:02:03'),
(596, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/60/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/60/edit', '0.23863196', '2019-09-04', '11:02:19', '2019-09-04 18:02:19', '2019-09-04 18:02:19'),
(597, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/7/topics/61/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/7/topics/61/edit', '0.41825008', '2019-09-04', '11:02:33', '2019-09-04 18:02:33', '2019-09-04 18:02:33'),
(598, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/en/12/casestudybycat/14', 'unknown', 'http://royalscrown.com/cofeesign/en/12/casestudybycat/14', '0.21224594', '2019-09-04', '12:10:13', '2019-09-04 19:10:13', '2019-09-04 19:10:13'),
(599, 20, '122.173.132.125', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/11/edit', '0.26417685', '2019-09-04', '13:02:12', '2019-09-04 20:02:12', '2019-09-04 20:02:12'),
(600, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.20327783', '2019-09-04', '23:51:15', '2019-09-05 06:51:15', '2019-09-05 06:51:15'),
(601, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin?_pjax=%23view', '0.29340792', '2019-09-04', '23:51:18', '2019-09-05 06:51:18', '2019-09-05 06:51:18'),
(602, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections?_pjax=%23view', '0.26725101', '2019-09-04', '23:51:32', '2019-09-05 06:51:32', '2019-09-05 06:51:32'),
(603, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections/9/edit', '0.1895051', '2019-09-04', '23:51:37', '2019-09-05 06:51:37', '2019-09-05 06:51:37'),
(604, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/2/sections', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/sections', '0.18562698', '2019-09-04', '23:51:45', '2019-09-05 06:51:45', '2019-09-05 06:51:45'),
(605, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/2/topics?_pjax=%23view', '0.19919705', '2019-09-04', '23:52:09', '2019-09-05 06:52:09', '2019-09-05 06:52:09');
INSERT INTO `smartend_analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(606, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin', 'unknown', 'http://royalscrown.com/cofeesign/admin', '0.40437698', '2019-09-04', '23:52:15', '2019-09-05 06:52:15', '2019-09-05 06:52:15'),
(607, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics?_pjax=%23view', '0.178756', '2019-09-04', '23:52:17', '2019-09-05 06:52:17', '2019-09-05 06:52:17'),
(608, 24, '96.72.168.211', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', 'unknown', 'http://royalscrown.com/cofeesign/admin/3/topics/9/edit', '0.27435017', '2019-09-04', '23:52:21', '2019-09-05 06:52:21', '2019-09-05 06:52:21'),
(609, 25, '122.173.132.125', 'http://royalscrown.com/cofeesign/login', 'unknown', 'http://royalscrown.com/cofeesign/login', '0.22545004', '2019-09-05', '04:36:08', '2019-09-05 11:36:08', '2019-09-05 11:36:08'),
(610, 26, '122.173.132.125', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.02423906', '2019-09-06', '04:22:36', '2019-09-06 04:22:36', '2019-09-06 04:22:36'),
(611, 26, '122.173.132.125', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.06388521', '2019-09-06', '04:23:00', '2019-09-06 04:23:00', '2019-09-06 04:23:00'),
(612, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/libs/swiper/swiper.min.css', 'unknown', 'http://40.122.39.187:3000/assets/static/libs/swiper/swiper.min.css', '0.02306914', '2019-09-06', '05:51:59', '2019-09-06 05:51:59', '2019-09-06 05:51:59'),
(613, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/css/main.min.css', 'unknown', 'http://40.122.39.187:3000/assets/static/css/main.min.css', '0.01580191', '2019-09-06', '05:51:59', '2019-09-06 05:51:59', '2019-09-06 05:51:59'),
(614, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/bootstrap-3/css/bootstrap.min.css', 'unknown', 'http://40.122.39.187:3000/assets/static/bootstrap-3/css/bootstrap.min.css', '0.02602196', '2019-09-06', '05:51:59', '2019-09-06 05:51:59', '2019-09-06 05:51:59'),
(615, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/js/jquery-3.4.1.min.js', 'unknown', 'http://40.122.39.187:3000/assets/static/js/jquery-3.4.1.min.js', '0.03508806', '2019-09-06', '05:51:59', '2019-09-06 05:51:59', '2019-09-06 05:51:59'),
(616, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/bootstrap-3/js/bootstrap.min.js', 'unknown', 'http://40.122.39.187:3000/assets/static/bootstrap-3/js/bootstrap.min.js', '0.02735305', '2019-09-06', '05:51:59', '2019-09-06 05:51:59', '2019-09-06 05:51:59'),
(617, 26, '122.173.132.125', 'http://40.122.39.187:3000/assets/static/img/favicon/favicon.ico', 'unknown', 'http://40.122.39.187:3000/assets/static/img/favicon/favicon.ico', '0.01393914', '2019-09-06', '05:52:00', '2019-09-06 05:52:00', '2019-09-06 05:52:00'),
(618, 26, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.04122806', '2019-09-06', '08:17:01', '2019-09-06 08:17:01', '2019-09-06 08:17:01'),
(619, 26, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', '0.04105401', '2019-09-06', '08:17:04', '2019-09-06 08:17:04', '2019-09-06 08:17:04'),
(620, 26, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/sections/10/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/sections/10/edit', '0.05083799', '2019-09-06', '08:17:07', '2019-09-06 08:17:07', '2019-09-06 08:17:07'),
(621, 26, '122.173.132.125', 'http://40.122.39.187:3000/admin?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin?_pjax=%23view', '0.05650401', '2019-09-06', '08:17:15', '2019-09-06 08:17:15', '2019-09-06 08:17:15'),
(622, 26, '122.173.132.125', 'http://40.122.39.187:3000/home', 'unknown', 'http://40.122.39.187:3000/home', '0.15539193', '2019-09-06', '10:33:12', '2019-09-06 10:33:12', '2019-09-06 10:33:12'),
(623, 26, '122.173.132.125', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.01955795', '2019-09-06', '10:33:14', '2019-09-06 10:33:14', '2019-09-06 10:33:14'),
(624, 27, '122.173.132.125', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.08857203', '2019-09-07', '12:14:24', '2019-09-07 12:14:24', '2019-09-07 12:14:24'),
(625, 27, '122.173.132.125', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.13394189', '2019-09-07', '12:14:42', '2019-09-07 12:14:42', '2019-09-07 12:14:42'),
(626, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', '0.15507793', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(627, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/js/jquery-3.4.1.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/js/jquery-3.4.1.min.js', '0.20886302', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(628, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/css/main.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/css/main.min.css', '0.2297101', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(629, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/css/bootstrap.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/css/bootstrap.min.css', '0.12625408', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(630, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/js/bootstrap.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/js/bootstrap.min.js', '0.16363716', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(631, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/js/scripts.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/js/scripts.min.js', '0.07456017', '2019-09-07', '13:04:50', '2019-09-07 13:04:50', '2019-09-07 13:04:50'),
(632, 27, '122.173.132.125', 'http://40.122.39.187:3000/casestudypost/static/img/favicon/favicon.ico', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/img/favicon/favicon.ico', '0.07762504', '2019-09-07', '13:04:51', '2019-09-07 13:04:51', '2019-09-07 13:04:51'),
(633, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/css/bootstrap.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/css/bootstrap.min.css', '0.02068996', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(634, 29, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', '0.02700901', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(635, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/css/main.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/css/main.min.css', '0.01358199', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(636, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/js/bootstrap.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/bootstrap-3/js/bootstrap.min.js', '0.01776409', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(637, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/js/jquery-3.4.1.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/js/jquery-3.4.1.min.js', '0.02000809', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(638, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/js/scripts.min.js', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/js/scripts.min.js', '0.01448917', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(639, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/img/favicon/favicon.ico', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/img/favicon/favicon.ico', '0.01915503', '2019-09-08', '07:18:57', '2019-09-08 07:18:57', '2019-09-08 07:18:57'),
(640, 28, '42.109.232.60', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', 'unknown', 'http://40.122.39.187:3000/casestudypost/static/libs/swiper/swiper.min.css', '0.02467108', '2019-09-08', '07:19:19', '2019-09-08 07:19:19', '2019-09-08 07:19:19'),
(641, 30, '103.41.26.84', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01970911', '2019-09-09', '04:52:14', '2019-09-09 04:52:14', '2019-09-09 04:52:14'),
(642, 31, '108.65.78.171', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01270604', '2019-09-09', '04:52:43', '2019-09-09 04:52:43', '2019-09-09 04:52:43'),
(643, 32, '122.173.132.125', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01508498', '2019-09-09', '05:14:15', '2019-09-09 05:14:15', '2019-09-09 05:14:15'),
(644, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.06787586', '2019-09-09', '05:40:11', '2019-09-09 05:40:11', '2019-09-09 05:40:11'),
(645, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', '0.0456872', '2019-09-09', '05:40:18', '2019-09-09 05:40:18', '2019-09-09 05:40:18'),
(646, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/edit', '0.0960381', '2019-09-09', '05:40:23', '2019-09-09 05:40:23', '2019-09-09 05:40:23'),
(647, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', '0.02639389', '2019-09-09', '05:40:29', '2019-09-09 05:40:29', '2019-09-09 05:40:29'),
(648, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', '0.02619004', '2019-09-09', '05:40:29', '2019-09-09 05:40:29', '2019-09-09 05:40:29'),
(649, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/7/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/7/edit', '0.08814001', '2019-09-09', '05:40:38', '2019-09-09 05:40:38', '2019-09-09 05:40:38'),
(650, 32, '122.173.132.125', 'http://40.122.39.187:3000/%22img//svg//hint.svg/%22', 'unknown', 'http://40.122.39.187:3000/%22img//svg//hint.svg/%22', '0.02330685', '2019-09-09', '05:43:19', '2019-09-09 05:43:19', '2019-09-09 05:43:19'),
(651, 32, '122.173.132.125', 'http://40.122.39.187:3000/%22img//svg//close-icon-small.svg/%22', 'unknown', 'http://40.122.39.187:3000/%22img//svg//close-icon-small.svg/%22', '0.02577996', '2019-09-09', '05:43:19', '2019-09-09 05:43:19', '2019-09-09 05:43:19'),
(652, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', '0.06650186', '2019-09-09', '05:44:38', '2019-09-09 05:44:38', '2019-09-09 05:44:38'),
(653, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', '0.04502892', '2019-09-09', '05:52:59', '2019-09-09 05:52:59', '2019-09-09 05:52:59'),
(654, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.03285003', '2019-09-09', '05:53:02', '2019-09-09 05:53:02', '2019-09-09 05:53:02'),
(655, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', '0.02968311', '2019-09-09', '05:53:05', '2019-09-09 05:53:05', '2019-09-09 05:53:05'),
(656, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', '0.033077', '2019-09-09', '05:53:09', '2019-09-09 05:53:09', '2019-09-09 05:53:09'),
(657, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners/create/1', 'unknown', 'http://40.122.39.187:3000/admin/banners/create/1', '0.05147815', '2019-09-09', '05:53:11', '2019-09-09 05:53:11', '2019-09-09 05:53:11'),
(658, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.02959013', '2019-09-09', '05:53:14', '2019-09-09 05:53:14', '2019-09-09 05:53:14'),
(659, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/payments/create', 'unknown', 'http://40.122.39.187:3000/admin/payments/create', '0.05323911', '2019-09-09', '05:53:15', '2019-09-09 05:53:15', '2019-09-09 05:53:15'),
(660, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/payments/5/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/5/edit', '0.04847598', '2019-09-09', '05:54:06', '2019-09-09 05:54:06', '2019-09-09 05:54:06'),
(661, 32, '122.173.132.125', 'http://40.122.39.187:3000/en/img/svg/close-icon-small.svg', 'unknown', 'http://40.122.39.187:3000/en/img/svg/close-icon-small.svg', '0.02373195', '2019-09-09', '05:56:18', '2019-09-09 05:56:18', '2019-09-09 05:56:18'),
(662, 32, '122.173.132.125', 'http://40.122.39.187:3000/en/img/svg/hint.svg', 'unknown', 'http://40.122.39.187:3000/en/img/svg/hint.svg', '0.02677202', '2019-09-09', '05:56:18', '2019-09-09 05:56:18', '2019-09-09 05:56:18'),
(663, 32, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/8/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/8/edit', '0.08580399', '2019-09-09', '06:20:55', '2019-09-09 06:20:55', '2019-09-09 06:20:55'),
(664, 33, '50.208.44.147', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.02741909', '2019-09-09', '17:52:31', '2019-09-09 17:52:31', '2019-09-09 17:52:31'),
(665, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.08136415', '2019-09-09', '17:52:46', '2019-09-09 17:52:46', '2019-09-09 17:52:46'),
(666, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin?_pjax=%23view', '0.08687401', '2019-09-09', '17:52:54', '2019-09-09 17:52:54', '2019-09-09 17:52:54'),
(667, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', '0.04514694', '2019-09-09', '17:52:57', '2019-09-09 17:52:57', '2019-09-09 17:52:57'),
(668, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', '0.04333401', '2019-09-09', '17:53:04', '2019-09-09 17:53:04', '2019-09-09 17:53:04'),
(669, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/sections/create', 'unknown', 'http://40.122.39.187:3000/admin/2/sections/create', '0.03774285', '2019-09-09', '17:53:10', '2019-09-09 17:53:10', '2019-09-09 17:53:10'),
(670, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/sections', 'unknown', 'http://40.122.39.187:3000/admin/2/sections', '0.03574109', '2019-09-09', '17:53:39', '2019-09-09 17:53:39', '2019-09-09 17:53:39'),
(671, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/sections/9/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/sections/9/edit', '0.05286503', '2019-09-09', '17:54:14', '2019-09-09 17:54:14', '2019-09-09 17:54:14'),
(672, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics/create', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/create', '0.07123899', '2019-09-09', '17:58:59', '2019-09-09 17:58:59', '2019-09-09 17:58:59'),
(673, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics', 'unknown', 'http://40.122.39.187:3000/admin/2/topics', '0.03483105', '2019-09-09', '17:59:03', '2019-09-09 17:59:03', '2019-09-09 17:59:03'),
(674, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics/6/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/edit', '0.07913494', '2019-09-09', '17:59:25', '2019-09-09 17:59:25', '2019-09-09 17:59:25'),
(675, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', '0.03157806', '2019-09-09', '17:59:26', '2019-09-09 17:59:26', '2019-09-09 17:59:26'),
(676, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', '0.02993107', '2019-09-09', '17:59:26', '2019-09-09 17:59:26', '2019-09-09 17:59:26'),
(677, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.03870702', '2019-09-09', '17:59:40', '2019-09-09 17:59:40', '2019-09-09 17:59:40'),
(678, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/3/topics/9/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/9/edit', '0.08163214', '2019-09-09', '17:59:56', '2019-09-09 17:59:56', '2019-09-09 17:59:56'),
(679, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', '0.03823185', '2019-09-09', '18:01:12', '2019-09-09 18:01:12', '2019-09-09 18:01:12'),
(680, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', '0.03096008', '2019-09-09', '18:01:18', '2019-09-09 18:01:18', '2019-09-09 18:01:18'),
(681, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', '0.06153393', '2019-09-09', '18:01:23', '2019-09-09 18:01:23', '2019-09-09 18:01:23'),
(682, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', '0.03352809', '2019-09-09', '18:01:27', '2019-09-09 18:01:27', '2019-09-09 18:01:27'),
(683, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.02912402', '2019-09-09', '18:01:29', '2019-09-09 18:01:29', '2019-09-09 18:01:29'),
(684, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/payments/4/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/4/edit', '0.04113293', '2019-09-09', '18:02:30', '2019-09-09 18:02:30', '2019-09-09 18:02:30'),
(685, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/payments', 'unknown', 'http://40.122.39.187:3000/admin/payments', '0.0258379', '2019-09-09', '18:02:41', '2019-09-09 18:02:41', '2019-09-09 18:02:41'),
(686, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/payments/5/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/5/edit', '0.03933907', '2019-09-09', '18:02:43', '2019-09-09 18:02:43', '2019-09-09 18:02:43'),
(687, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', '0.11496305', '2019-09-09', '18:03:42', '2019-09-09 18:03:42', '2019-09-09 18:03:42'),
(688, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', '0.04524803', '2019-09-09', '18:04:18', '2019-09-09 18:04:18', '2019-09-09 18:04:18'),
(689, 33, '50.208.44.147', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', '0.03246903', '2019-09-09', '18:04:31', '2019-09-09 18:04:31', '2019-09-09 18:04:31'),
(690, 34, '122.173.132.125', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.0117209', '2019-09-10', '04:09:28', '2019-09-10 04:09:28', '2019-09-10 04:09:28'),
(691, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.06149507', '2019-09-10', '04:53:01', '2019-09-10 04:53:01', '2019-09-10 04:53:01'),
(692, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', '0.03844094', '2019-09-10', '04:53:06', '2019-09-10 04:53:06', '2019-09-10 04:53:06'),
(693, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/edit', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/edit', '0.07157397', '2019-09-10', '04:53:09', '2019-09-10 04:53:09', '2019-09-10 04:53:09'),
(694, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/hint.svg', '0.02736497', '2019-09-10', '04:53:15', '2019-09-10 04:53:15', '2019-09-10 04:53:15'),
(695, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', 'unknown', 'http://40.122.39.187:3000/admin/2/topics/6/img/svg/close-icon-small.svg', '0.02589393', '2019-09-10', '04:53:15', '2019-09-10 04:53:15', '2019-09-10 04:53:15'),
(696, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics/9/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/9/edit', '0.08014107', '2019-09-10', '04:55:39', '2019-09-10 04:55:39', '2019-09-10 04:55:39'),
(697, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.02993703', '2019-09-10', '04:56:04', '2019-09-10 04:56:04', '2019-09-10 04:56:04'),
(698, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics/10/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/10/edit', '0.06805897', '2019-09-10', '04:56:08', '2019-09-10 04:56:08', '2019-09-10 04:56:08'),
(699, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', '0.02684689', '2019-09-10', '04:58:25', '2019-09-10 04:58:25', '2019-09-10 04:58:25'),
(700, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', '0.0405519', '2019-09-10', '04:58:48', '2019-09-10 04:58:48', '2019-09-10 04:58:48'),
(701, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics/create', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/create', '0.0463419', '2019-09-10', '04:58:52', '2019-09-10 04:58:52', '2019-09-10 04:58:52'),
(702, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/settings', 'unknown', 'http://40.122.39.187:3000/admin/settings', '0.08213496', '2019-09-10', '04:59:18', '2019-09-10 04:59:18', '2019-09-10 04:59:18'),
(703, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', '0.07210803', '2019-09-10', '04:59:18', '2019-09-10 04:59:18', '2019-09-10 04:59:18'),
(704, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus/6/edit/1', 'unknown', 'http://40.122.39.187:3000/admin/menus/6/edit/1', '0.0411911', '2019-09-10', '04:59:33', '2019-09-10 04:59:34', '2019-09-10 04:59:34'),
(705, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus/1?id=6', 'unknown', 'http://40.122.39.187:3000/admin/menus/1?id=6', '0.03482819', '2019-09-10', '04:59:54', '2019-09-10 04:59:54', '2019-09-10 04:59:54'),
(706, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus/5/edit/1', 'unknown', 'http://40.122.39.187:3000/admin/menus/5/edit/1', '0.03756618', '2019-09-10', '05:00:10', '2019-09-10 05:00:10', '2019-09-10 05:00:10'),
(707, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus/1', 'unknown', 'http://40.122.39.187:3000/admin/menus/1', '0.03569293', '2019-09-10', '05:00:17', '2019-09-10 05:00:17', '2019-09-10 05:00:17'),
(708, 34, '122.173.132.125', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.01835799', '2019-09-10', '05:00:30', '2019-09-10 05:00:30', '2019-09-10 05:00:30'),
(709, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/menus/3/edit/1', 'unknown', 'http://40.122.39.187:3000/admin/menus/3/edit/1', '0.04333115', '2019-09-10', '05:00:44', '2019-09-10 05:00:44', '2019-09-10 05:00:44'),
(710, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/sections', 'unknown', 'http://40.122.39.187:3000/admin/2/sections', '0.04306889', '2019-09-10', '05:15:49', '2019-09-10 05:15:49', '2019-09-10 05:15:49'),
(711, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', '0.03476095', '2019-09-10', '05:16:26', '2019-09-10 05:16:26', '2019-09-10 05:16:26'),
(712, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners/7/edit', 'unknown', 'http://40.122.39.187:3000/admin/banners/7/edit', '0.05254483', '2019-09-10', '05:16:29', '2019-09-10 05:16:29', '2019-09-10 05:16:29'),
(713, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners/12/edit', 'unknown', 'http://40.122.39.187:3000/admin/banners/12/edit', '0.04571104', '2019-09-10', '05:16:32', '2019-09-10 05:16:32', '2019-09-10 05:16:32'),
(714, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics/62/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/62/edit', '0.07670403', '2019-09-10', '05:22:53', '2019-09-10 05:22:53', '2019-09-10 05:22:53'),
(715, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics', 'unknown', 'http://40.122.39.187:3000/admin/3/topics', '0.02806187', '2019-09-10', '05:23:06', '2019-09-10 05:23:06', '2019-09-10 05:23:06'),
(716, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics/63/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/63/edit', '0.08914113', '2019-09-10', '05:32:44', '2019-09-10 05:32:44', '2019-09-10 05:32:44'),
(717, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', '0.05165982', '2019-09-10', '05:33:44', '2019-09-10 05:33:44', '2019-09-10 05:33:44'),
(718, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.0303731', '2019-09-10', '05:33:57', '2019-09-10 05:33:57', '2019-09-10 05:33:57'),
(719, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', '0.02790999', '2019-09-10', '05:34:08', '2019-09-10 05:34:08', '2019-09-10 05:34:08'),
(720, 34, '122.173.132.125', 'http://40.122.39.187:3000/en/6/img/svg/close-icon-small.svg', 'unknown', 'http://40.122.39.187:3000/en/6/img/svg/close-icon-small.svg', '0.02503896', '2019-09-10', '05:41:53', '2019-09-10 05:41:53', '2019-09-10 05:41:53'),
(721, 34, '122.173.132.125', 'http://40.122.39.187:3000/en/6/img/svg/hint.svg', 'unknown', 'http://40.122.39.187:3000/en/6/img/svg/hint.svg', '0.02424908', '2019-09-10', '05:41:53', '2019-09-10 05:41:53', '2019-09-10 05:41:53'),
(722, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics/16/edit', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/16/edit', '0.07170296', '2019-09-10', '05:48:02', '2019-09-10 05:48:02', '2019-09-10 05:48:02'),
(723, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster/banners?_pjax=%23view', '0.0313859', '2019-09-10', '06:06:07', '2019-09-10 06:06:07', '2019-09-10 06:06:07'),
(724, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster/sections?_pjax=%23view', '0.03099394', '2019-09-10', '06:06:09', '2019-09-10 06:06:09', '2019-09-10 06:06:09'),
(725, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster', 'unknown', 'http://40.122.39.187:3000/admin/webmaster', '0.04000592', '2019-09-10', '06:06:47', '2019-09-10 06:06:47', '2019-09-10 06:06:47'),
(726, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', '0.03697681', '2019-09-10', '06:06:48', '2019-09-10 06:06:48', '2019-09-10 06:06:48'),
(727, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics/61/edit', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/61/edit', '0.07416081', '2019-09-10', '06:21:53', '2019-09-10 06:21:53', '2019-09-10 06:21:53'),
(728, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/tags/3/edit', 'unknown', 'http://40.122.39.187:3000/admin/tags/3/edit', '0.04172993', '2019-09-10', '06:56:14', '2019-09-10 06:56:14', '2019-09-10 06:56:14'),
(729, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/tags', 'unknown', 'http://40.122.39.187:3000/admin/tags', '0.02721691', '2019-09-10', '06:56:28', '2019-09-10 06:56:28', '2019-09-10 06:56:28'),
(730, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics/61/edit?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/61/edit?_pjax=%23view', '0.07159901', '2019-09-10', '06:56:30', '2019-09-10 06:56:30', '2019-09-10 06:56:30'),
(731, 34, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics', 'unknown', 'http://40.122.39.187:3000/admin/7/topics', '0.04660702', '2019-09-10', '07:37:24', '2019-09-10 07:37:24', '2019-09-10 07:37:24'),
(732, 35, '103.41.26.12', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01216292', '2019-09-10', '11:04:19', '2019-09-10 11:04:19', '2019-09-10 11:04:19'),
(733, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/403', 'unknown', 'http://40.122.39.187:3000/admin/403', '0.01441216', '2019-09-10', '11:04:23', '2019-09-10 11:04:23', '2019-09-10 11:04:23'),
(734, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/', 'unknown', 'http://40.122.39.187:3000/admin/', '0.06088996', '2019-09-10', '11:04:28', '2019-09-10 11:04:28', '2019-09-10 11:04:28'),
(735, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.02883101', '2019-09-10', '11:04:31', '2019-09-10 11:04:31', '2019-09-10 11:04:31'),
(736, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/payments/6/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/6/edit', '0.04274106', '2019-09-10', '11:04:38', '2019-09-10 11:04:38', '2019-09-10 11:04:38'),
(737, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/payments/7/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/7/edit', '0.03986907', '2019-09-10', '11:06:29', '2019-09-10 11:06:29', '2019-09-10 11:06:29'),
(738, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/payments/4/edit', 'unknown', 'http://40.122.39.187:3000/admin/payments/4/edit', '0.04315996', '2019-09-10', '11:08:39', '2019-09-10 11:08:39', '2019-09-10 11:08:39'),
(739, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/settings', 'unknown', 'http://40.122.39.187:3000/admin/settings', '0.13062501', '2019-09-10', '11:46:19', '2019-09-10 11:46:19', '2019-09-10 11:46:19'),
(740, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', '0.13911605', '2019-09-10', '11:46:19', '2019-09-10 11:46:19', '2019-09-10 11:46:19'),
(741, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/webmaster', 'unknown', 'http://40.122.39.187:3000/admin/webmaster', '0.056499', '2019-09-10', '11:47:02', '2019-09-10 11:47:02', '2019-09-10 11:47:02'),
(742, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', '0.04401207', '2019-09-10', '11:47:02', '2019-09-10 11:47:02', '2019-09-10 11:47:02'),
(743, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.10566401', '2019-09-10', '13:07:21', '2019-09-10 13:07:21', '2019-09-10 13:07:21'),
(744, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin?_pjax=%23view', '0.09405398', '2019-09-10', '13:07:21', '2019-09-10 13:07:21', '2019-09-10 13:07:21'),
(745, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', '0.03616285', '2019-09-10', '13:07:29', '2019-09-10 13:07:29', '2019-09-10 13:07:29'),
(746, 35, '103.41.26.12', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', '0.02996206', '2019-09-10', '13:07:44', '2019-09-10 13:07:44', '2019-09-10 13:07:44'),
(747, 36, '146.74.94.74', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01499391', '2019-09-10', '20:21:58', '2019-09-10 20:21:58', '2019-09-10 20:21:58'),
(748, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/2/sections', 'unknown', 'http://40.122.39.187:3000/admin/2/sections', '0.04257083', '2019-09-10', '21:39:53', '2019-09-10 21:39:53', '2019-09-10 21:39:53'),
(749, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/2/sections/create', 'unknown', 'http://40.122.39.187:3000/admin/2/sections/create', '0.04388809', '2019-09-10', '21:41:42', '2019-09-10 21:41:42', '2019-09-10 21:41:42'),
(750, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/3/topics/9/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/9/edit', '0.07456303', '2019-09-10', '21:48:05', '2019-09-10 21:48:05', '2019-09-10 21:48:05'),
(751, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/settings', 'unknown', 'http://40.122.39.187:3000/admin/settings', '0.08027005', '2019-09-10', '21:53:03', '2019-09-10 21:53:03', '2019-09-10 21:53:03'),
(752, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/settings?_pjax=%23view', '0.12001085', '2019-09-10', '21:53:10', '2019-09-10 21:53:10', '2019-09-10 21:53:10'),
(753, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/menus?_pjax=%23view', '0.0460031', '2019-09-10', '21:53:12', '2019-09-10 21:53:12', '2019-09-10 21:53:12'),
(754, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/webmaster', 'unknown', 'http://40.122.39.187:3000/admin/webmaster', '0.04508805', '2019-09-10', '21:53:15', '2019-09-10 21:53:15', '2019-09-10 21:53:15'),
(755, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', '0.04785395', '2019-09-10', '21:53:15', '2019-09-10 21:53:15', '2019-09-10 21:53:15'),
(756, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/users?_pjax=%23view', '0.02962899', '2019-09-10', '21:53:33', '2019-09-10 21:53:33', '2019-09-10 21:53:33'),
(757, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.032686', '2019-09-10', '21:55:18', '2019-09-10 21:55:18', '2019-09-10 21:55:18'),
(758, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', '0.03189993', '2019-09-10', '21:55:24', '2019-09-10 21:55:24', '2019-09-10 21:55:24'),
(759, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', '0.04120803', '2019-09-10', '21:55:29', '2019-09-10 21:55:29', '2019-09-10 21:55:29'),
(760, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', '0.02748704', '2019-09-10', '21:55:42', '2019-09-10 21:55:42', '2019-09-10 21:55:42'),
(761, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', '0.0457418', '2019-09-10', '21:55:43', '2019-09-10 21:55:43', '2019-09-10 21:55:43'),
(762, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.02516603', '2019-09-10', '21:55:50', '2019-09-10 21:55:50', '2019-09-10 21:55:50'),
(763, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin?_pjax=%23view', '0.07651591', '2019-09-10', '21:55:53', '2019-09-10 21:55:53', '2019-09-10 21:55:53'),
(764, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.06848001', '2019-09-10', '21:55:53', '2019-09-10 21:55:53', '2019-09-10 21:55:53'),
(765, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/tags/create', 'unknown', 'http://40.122.39.187:3000/admin/tags/create', '0.03190684', '2019-09-10', '21:59:14', '2019-09-10 21:59:14', '2019-09-10 21:59:14'),
(766, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/tags', 'unknown', 'http://40.122.39.187:3000/admin/tags', '0.03013587', '2019-09-10', '21:59:17', '2019-09-10 21:59:17', '2019-09-10 21:59:17'),
(767, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/7/topics/56/edit', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/56/edit', '0.07592893', '2019-09-10', '22:00:43', '2019-09-10 22:00:43', '2019-09-10 22:00:43'),
(768, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/3/topics/create', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/create', '0.0568471', '2019-09-10', '22:26:46', '2019-09-10 22:26:46', '2019-09-10 22:26:46'),
(769, 36, '146.74.94.74', 'http://40.122.39.187:3000/admin/3/topics/64/edit', 'unknown', 'http://40.122.39.187:3000/admin/3/topics/64/edit', '0.07797885', '2019-09-10', '22:28:58', '2019-09-10 22:28:58', '2019-09-10 22:28:58'),
(770, 37, '116.98.145.45', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.0140202', '2019-09-11', '02:38:22', '2019-09-11 02:38:22', '2019-09-11 02:38:22'),
(771, 38, '122.173.132.125', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.02313495', '2019-09-11', '04:02:47', '2019-09-11 04:02:47', '2019-09-11 04:02:47'),
(772, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin', 'unknown', 'http://40.122.39.187:3000/admin', '0.07726812', '2019-09-11', '04:02:58', '2019-09-11 04:02:58', '2019-09-11 04:02:58'),
(773, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/analytics/date', 'unknown', 'http://40.122.39.187:3000/admin/analytics/date', '0.06446314', '2019-09-11', '04:03:09', '2019-09-11 04:03:09', '2019-09-11 04:03:09'),
(774, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/webmaster?_pjax=%23view', '0.05752206', '2019-09-11', '04:03:19', '2019-09-11 04:03:19', '2019-09-11 04:03:19'),
(775, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/webmaster', 'unknown', 'http://40.122.39.187:3000/admin/webmaster', '0.06926084', '2019-09-11', '04:03:19', '2019-09-11 04:03:19', '2019-09-11 04:03:19'),
(776, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin?_pjax=%23view', '0.07542491', '2019-09-11', '04:03:23', '2019-09-11 04:03:23', '2019-09-11 04:03:23'),
(777, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/sections?_pjax=%23view', '0.043818', '2019-09-11', '04:03:27', '2019-09-11 04:03:27', '2019-09-11 04:03:27'),
(778, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/topics?_pjax=%23view', '0.05832601', '2019-09-11', '04:05:27', '2019-09-11 04:05:27', '2019-09-11 04:05:27'),
(779, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/banners?_pjax=%23view', '0.03875589', '2019-09-11', '04:05:44', '2019-09-11 04:05:44', '2019-09-11 04:05:44'),
(780, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners/12/edit', 'unknown', 'http://40.122.39.187:3000/admin/banners/12/edit', '0.05353189', '2019-09-11', '04:05:55', '2019-09-11 04:05:55', '2019-09-11 04:05:55'),
(781, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/banners/7/edit', 'unknown', 'http://40.122.39.187:3000/admin/banners/7/edit', '0.05470085', '2019-09-11', '04:12:18', '2019-09-11 04:12:18', '2019-09-11 04:12:18'),
(782, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/2/topics?_pjax=%23view', '0.04422593', '2019-09-11', '04:22:12', '2019-09-11 04:22:12', '2019-09-11 04:22:12'),
(783, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/payments?_pjax=%23view', '0.030128', '2019-09-11', '04:28:42', '2019-09-11 04:28:42', '2019-09-11 04:28:42'),
(784, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/3/topics?_pjax=%23view', '0.03642821', '2019-09-11', '04:43:43', '2019-09-11 04:43:43', '2019-09-11 04:43:43'),
(785, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics/57/edit', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/57/edit', '0.08140492', '2019-09-11', '04:46:20', '2019-09-11 04:46:20', '2019-09-11 04:46:20'),
(786, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/7/sections?_pjax=%23view', '0.04394317', '2019-09-11', '04:47:49', '2019-09-11 04:47:49', '2019-09-11 04:47:49'),
(787, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', 'unknown', 'http://40.122.39.187:3000/admin/tags?_pjax=%23view', '0.02831793', '2019-09-11', '04:52:50', '2019-09-11 04:52:50', '2019-09-11 04:52:50'),
(788, 38, '122.173.132.125', 'http://40.122.39.187:3000/admin/7/topics/create', 'unknown', 'http://40.122.39.187:3000/admin/7/topics/create', '0.05020714', '2019-09-11', '04:54:01', '2019-09-11 04:54:01', '2019-09-11 04:54:01'),
(789, 39, '211.24.101.114', 'http://40.122.39.187:3000/login', 'unknown', 'http://40.122.39.187:3000/login', '0.01936316', '2019-09-11', '05:39:28', '2019-09-11 05:39:28', '2019-09-11 05:39:28'),
(790, 39, '211.24.101.114', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://40.122.39.187:3000/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.01771903', '2019-09-11', '05:43:35', '2019-09-11 05:43:35', '2019-09-11 05:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_analytics_visitors`
--

CREATE TABLE `smartend_analytics_visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_analytics_visitors`
--

INSERT INTO `smartend_analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-08-27', '07:42:11', '2019-08-27 14:42:11', '2019-08-27 14:42:11'),
(2, '52.114.14.102', 'Singapore', 'SG', 'Singapore', 'unknown', 'unknown, Singapore, Singapore', '1.2897', '103.8501', 'Windows 7', NULL, 'unknown', 'unknown', 'No Hostname', 'AS8075 Microsoft Corporation', '2019-08-27', '07:42:36', '2019-08-27 14:42:36', '2019-08-27 14:42:36'),
(3, '103.41.26.222', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '222.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-27', '07:43:11', '2019-08-27 14:43:11', '2019-08-27 14:43:11'),
(4, '52.114.6.38', 'Tsuen Wan', 'HK', 'Hong Kong', 'Tsuen Wan', 'Tsuen Wan, Tsuen Wan, Hong Kong', '22.3714', '114.1133', 'Windows 7', NULL, 'unknown', 'unknown', 'No Hostname', 'AS8075 Microsoft Corporation', '2019-08-27', '07:45:00', '2019-08-27 14:45:00', '2019-08-27 14:45:00'),
(5, '103.41.26.6', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '6.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-28', '05:01:55', '2019-08-28 12:01:55', '2019-08-28 12:01:55'),
(6, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Chrome', 'unknown', 'https://royalscrown.com/cofeesign/public/blog', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-08-28', '07:00:39', '2019-08-28 14:00:39', '2019-08-28 14:00:39'),
(7, '103.41.26.222', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'https://royalscrown.com/cofeesign/public/admin/7/sections', '222.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-28', '09:01:22', '2019-08-28 16:01:22', '2019-08-28 16:01:22'),
(8, '103.41.26.246', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '246.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-28', '12:42:52', '2019-08-28 19:42:52', '2019-08-28 19:42:52'),
(9, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-08-29', '04:38:12', '2019-08-29 11:38:12', '2019-08-29 11:38:12'),
(10, '103.41.26.6', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '6.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-29', '04:45:23', '2019-08-29 11:45:23', '2019-08-29 11:45:23'),
(11, '52.114.6.38', 'Tsuen Wan', 'HK', 'Hong Kong', 'Tsuen Wan', 'Tsuen Wan, Tsuen Wan, Hong Kong', '22.3714', '114.1133', 'Windows 7', NULL, 'unknown', 'unknown', 'No Hostname', 'AS8075 Microsoft Corporation', '2019-08-29', '05:51:01', '2019-08-29 12:51:01', '2019-08-29 12:51:01'),
(12, '125.62.111.163', 'Jalandhar', 'IN', 'India', 'Punjab', 'Punjab, Jalandhar, India', '31.3247', '75.5811', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'AS17917 Quadrant Televentures Limited', '2019-08-29', '16:04:13', '2019-08-29 23:04:13', '2019-08-29 23:04:13'),
(13, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'unknown', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-08-30', '04:31:15', '2019-08-30 11:31:15', '2019-08-30 11:31:15'),
(14, '103.41.26.102', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '102.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-08-30', '06:39:28', '2019-08-30 13:39:28', '2019-08-30 13:39:28'),
(15, '125.62.105.129', 'Ludhiana', 'IN', 'India', 'Punjab', 'Punjab, Ludhiana, India', '30.9120', '75.8538', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'AS17917 Quadrant Televentures Limited', '2019-09-01', '11:05:16', '2019-09-01 18:05:16', '2019-09-01 18:05:16'),
(16, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'unknown', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-02', '04:20:10', '2019-09-02 11:20:10', '2019-09-02 11:20:10'),
(17, '103.41.26.12', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '12.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-09-02', '12:49:08', '2019-09-02 19:49:08', '2019-09-02 19:49:08'),
(18, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'unknown', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-03', '03:47:22', '2019-09-03 10:47:22', '2019-09-03 10:47:22'),
(19, '103.41.26.132', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'https://royalscrown.com/cofeesign/admin/payments', '132.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-09-03', '06:19:29', '2019-09-03 13:19:29', '2019-09-03 13:19:29'),
(20, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'unknown', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-04', '04:15:16', '2019-09-04 11:15:16', '2019-09-04 11:15:16'),
(21, '103.41.26.180', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '180.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-09-04', '04:21:45', '2019-09-04 11:21:45', '2019-09-04 11:21:45'),
(22, '52.114.6.38', 'Tsuen Wan', 'HK', 'Hong Kong', 'Tsuen Wan', 'Tsuen Wan, Tsuen Wan, Hong Kong', '22.3714', '114.1133', 'Windows 7', NULL, 'unknown', 'unknown', 'No Hostname', 'AS8075 Microsoft Corporation', '2019-09-04', '05:54:06', '2019-09-04 12:54:06', '2019-09-04 12:54:06'),
(23, '108.65.78.171', 'Mountain View', 'US', 'United States of America', 'California', 'California, Mountain View, United States of America', '37.4043', '-122.0750', 'Mac OS X', 'Chrome', 'unknown', 'unknown', '108-65-78-171.lightspeed.sntcca.sbcglobal.net', 'AS7018 AT&T Services, Inc.', '2019-09-04', '05:54:58', '2019-09-04 12:54:58', '2019-09-04 12:54:58'),
(24, '96.72.168.211', 'San Francisco', 'US', 'United States of America', 'California', 'California, San Francisco, United States of America', '37.7833', '-122.4950', 'Mac OS X', 'Chrome', 'unknown', 'https://royalscrown.com/cofeesign/admin/2/sections', '96-72-168-211-static.hfc.comcastbusiness.net', 'AS33651 Comcast Cable Communications, LLC', '2019-09-04', '23:51:15', '2019-09-05 06:51:15', '2019-09-05 06:51:15'),
(25, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-05', '04:36:08', '2019-09-05 11:36:08', '2019-09-05 11:36:08'),
(26, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-06', '04:22:35', '2019-09-06 04:22:35', '2019-09-06 04:22:35'),
(27, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-07', '12:14:23', '2019-09-07 12:14:23', '2019-09-07 12:14:23'),
(28, '42.109.232.60', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'http://40.122.39.187:3000/casestudypost/56', '42-109-232-60.live.vodafone.in', 'AS38266 Vodafone India Ltd.', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(29, '42.109.232.60', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'http://40.122.39.187:3000/casestudypost/56', '42-109-232-60.live.vodafone.in', 'AS38266 Vodafone India Ltd.', '2019-09-08', '07:18:56', '2019-09-08 07:18:56', '2019-09-08 07:18:56'),
(30, '103.41.26.84', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'unknown', '84.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-09-09', '04:52:13', '2019-09-09 04:52:13', '2019-09-09 04:52:13'),
(31, '108.65.78.171', 'Mountain View', 'US', 'United States of America', 'California', 'California, Mountain View, United States of America', '37.4043', '-122.0750', 'Mac OS X', 'Chrome', 'unknown', 'unknown', '108-65-78-171.lightspeed.sntcca.sbcglobal.net', 'AS7018 AT&T Services, Inc.', '2019-09-09', '04:52:43', '2019-09-09 04:52:43', '2019-09-09 04:52:43'),
(32, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'http://40.122.39.187:3000/', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-09', '05:14:15', '2019-09-09 05:14:15', '2019-09-09 05:14:15'),
(33, '50.208.44.147', 'Santa Clara', 'US', 'United States of America', 'California', 'California, Santa Clara, United States of America', '37.3417', '-121.9750', 'Mac OS X', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'AS33651 Comcast Cable Communications, LLC', '2019-09-09', '17:52:30', '2019-09-09 17:52:30', '2019-09-09 17:52:30'),
(34, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-10', '04:09:28', '2019-09-10 04:09:28', '2019-09-10 04:09:28'),
(35, '103.41.26.12', 'Mohali', 'IN', 'India', 'Punjab', 'Punjab, Mohali, India', '30.7800', '76.6900', 'Windows 7', 'Chrome', 'unknown', 'http://40.122.39.187:3000/admin/banners/7/edit', '12.26.41.103.netplus.co.in', 'AS133661 Netplus Broadband Services Private Limited', '2019-09-10', '11:04:19', '2019-09-10 11:04:19', '2019-09-10 11:04:19'),
(36, '146.74.94.74', 'Milpitas', 'US', 'United States of America', 'California', 'California, Milpitas, United States of America', '37.4371', '-121.8950', 'Mac OS X', 'Chrome', 'unknown', 'http://40.122.39.187:3000/', 'No Hostname', 'AS2152 California State University, Office of the Chancellor', '2019-09-10', '20:21:58', '2019-09-10 20:21:58', '2019-09-10 20:21:58'),
(37, '116.98.145.45', 'Hoai Duc', 'VN', 'Viet Nam', 'Thuin Hai', 'Thuin Hai, Hoai Duc, Viet Nam', '11.2000', '107.7170', 'Linux', 'Chrome', 'unknown', 'unknown', 'dynamic-ip-adsl.viettel.vn', 'AS24086 Viettel Corporation', '2019-09-11', '02:38:22', '2019-09-11 02:38:22', '2019-09-11 02:38:22'),
(38, '122.173.132.125', 'Chandigarh', 'IN', 'India', 'Chandigarh', 'Chandigarh, Chandigarh, India', '30.7363', '76.7884', 'unknown', 'Chrome', 'unknown', 'unknown', 'abts-north-dynamic-125.132.173.122.airtelbroadband.in', 'AS24560 Bharti Airtel Ltd., Telemedia Services', '2019-09-11', '04:02:47', '2019-09-11 04:02:47', '2019-09-11 04:02:47'),
(39, '211.24.101.114', 'Malacca', 'MY', 'Malaysia', 'Melaka', 'Melaka, Malacca, Malaysia', '2.2074', '102.2510', 'unknown', 'Chrome', 'unknown', 'http://40.122.39.187:3000/', 'cgw-211-24-101-114.bbrtl.time.net.my', 'AS9930 TIME dotCom Berhad', '2019-09-11', '05:39:28', '2019-09-11 05:39:28', '2019-09-11 05:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_attach_files`
--

CREATE TABLE `smartend_attach_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_banners`
--

CREATE TABLE `smartend_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `details_jp` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `file_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_banners`
--

INSERT INTO `smartend_banners` (`id`, `section_id`, `title_ar`, `title_en`, `title_jp`, `details_ar`, `details_en`, `details_jp`, `code`, `file_ar`, `file_en`, `file_jp`, `video_type`, `youtube_link`, `link_url`, `icon`, `status`, `visits`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, 1, '계약서 서명은 우리에게, 커피는 당신에게 있습니다.', 'Contract signing is on us, Coffee is on you.', '契約調印は私たちにあり, コーヒーはあなたにあります。', NULL, 'CoffeeSign is a 3-step e-signature service that helps your business grow faster. Save your time and money with us.', NULL, NULL, '14888126419392.jpg', '15668980935062.jpg', '15675761552696.png', NULL, NULL, 'login', NULL, 1, 0, 1, 1, 1, '2017-03-06 13:04:01', '2019-09-10 07:52:28'),
(12, 2, '새로운', 'New', '新しい', 'Android 앱을 사용할 수 있습니다!', 'Android app is available now!', 'Androidアプリが利用可能になりました！', NULL, '', '', '', NULL, NULL, NULL, NULL, 1, 0, 2, 1, 1, '2019-08-30 18:14:30', '2019-09-10 05:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_comments`
--

CREATE TABLE `smartend_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_comments`
--

INSERT INTO `smartend_comments` (`id`, `topic_id`, `name`, `email`, `date`, `comment`, `status`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 9, 'Roza Hesham', 'email@site.com', '2017-03-06 15:55:21', 'Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti.', 1, 1, NULL, NULL, '2017-03-06 13:55:21', '2017-03-06 13:55:21'),
(2, 9, 'Adam Ali', 'emm@site.com', '2017-03-06 15:55:59', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.', 1, 2, NULL, NULL, '2017-03-06 13:55:59', '2017-03-06 13:55:59'),
(6, 90, 'mmmmm', 'eee@ss.ccd', '2017-11-12 05:15:03', 'The topic id field is required.', 1, 1, NULL, NULL, '2017-11-12 03:15:03', '2017-11-12 03:15:03'),
(7, 90, 'mmmmm', 'eee@ss.ccd', '2017-11-12 05:18:26', 'The topic id field is required.', 1, 2, NULL, NULL, '2017-11-12 03:18:26', '2017-11-12 03:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_contacts`
--

CREATE TABLE `smartend_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_contacts`
--

INSERT INTO `smartend_contacts` (`id`, `group_id`, `first_name`, `last_name`, `company`, `email`, `password`, `phone`, `country_id`, `city`, `address`, `photo`, `notes`, `last_login`, `last_login_ip`, `facebook_id`, `twitter_id`, `google_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Sara', 'Smith', 'HiMan Company', 'email@site.com', NULL, '234-245-5674', 68, NULL, NULL, '14889022279857.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 13:57:07', '2017-03-07 13:57:07'),
(2, 2, 'Maro', 'Faheed', 'Havway  Company', 'email@site.com', NULL, '386-755-7788', 30, NULL, NULL, '14889022842486.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2017-03-07 13:58:04', '2017-03-07 13:58:35'),
(3, 2, 'Adam', 'Ali', 'Trident company', 'email@site.com', NULL, '589-234-2342', 35, NULL, NULL, '14889023586327.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2017-03-07 13:59:08', '2017-03-07 13:59:18'),
(4, 2, 'Donal', 'Tashee', 'Hamer school', 'email@site.com', NULL, '674-274-8944', 41, NULL, NULL, '14889024177699.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 14:00:17', '2017-03-07 14:00:17'),
(5, NULL, 'Mona', 'Lamen', 'Troly Company', 'email@site.com', NULL, '324-674-4578', 10, 'Moco', NULL, '14889024974798.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 14:01:37', '2017-03-07 14:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_contacts_groups`
--

CREATE TABLE `smartend_contacts_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_contacts_groups`
--

INSERT INTO `smartend_contacts_groups` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Newsletter Emails', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(2, 'VIP', 1, NULL, '2017-03-07 13:56:11', '2017-03-07 13:56:11'),
(3, 'Customers', 1, NULL, '2017-03-07 13:56:24', '2017-03-07 13:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_countries`
--

CREATE TABLE `smartend_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_countries`
--

INSERT INTO `smartend_countries` (`id`, `code`, `title_ar`, `title_en`, `title_jp`, `tel`, `created_at`, `updated_at`) VALUES
(1, 'AL', 'ألبانيا', 'Albania', NULL, '355', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(2, 'DZ', 'الجزائر', 'Algeria', NULL, '213', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(3, 'AS', 'ساموا الأمريكية', 'American Samoa', NULL, '1-684', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(4, 'AD', 'أندورا', 'Andorra', NULL, '376', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(5, 'AO', 'أنغولا', 'Angola', NULL, '244', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(6, 'AI', 'أنغيلا', 'Anguilla', NULL, '1-264', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(7, 'AR', 'الأرجنتين', 'Argentina', NULL, '54', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(8, 'AM', 'أرمينيا', 'Armenia', NULL, '374', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(9, 'AW', 'أروبا', 'Aruba', NULL, '297', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(10, 'AU', 'أستراليا', 'Australia', NULL, '61', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(11, 'AT', 'النمسا', 'Austria', NULL, '43', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(12, 'AZ', 'أذربيجان', 'Azerbaijan', NULL, '994', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(13, 'BS', 'جزر البهاما', 'Bahamas', NULL, '1-242', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(14, 'BH', 'البحرين', 'Bahrain', NULL, '973', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(15, 'BD', 'بنغلاديش', 'Bangladesh', NULL, '880', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(16, 'BB', 'بربادوس', 'Barbados', NULL, '1-246', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(17, 'BY', 'روسيا البيضاء', 'Belarus', NULL, '375', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(18, 'BE', 'بلجيكا', 'Belgium', NULL, '32', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(19, 'BZ', 'بليز', 'Belize', NULL, '501', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(20, 'BJ', 'بنين', 'Benin', NULL, '229', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(21, 'BM', 'برمودا', 'Bermuda', NULL, '1-441', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(22, 'BT', 'بوتان', 'Bhutan', NULL, '975', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(23, 'BO', 'بوليفيا', 'Bolivia', NULL, '591', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(24, 'BA', 'البوسنة والهرسك', 'Bosnia and Herzegovina', NULL, '387', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(25, 'BW', 'بوتسوانا', 'Botswana', NULL, '267', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(26, 'BR', 'البرازيل', 'Brazil', NULL, '55', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(27, 'VG', 'جزر فيرجن البريطانية', 'British Virgin Islands', NULL, '1-284', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(28, 'IO', 'إقليم المحيط الهندي البريطاني', 'British Indian Ocean Territory', NULL, '246', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(29, 'BN', 'بروناي دار السلام', 'Brunei Darussalam', NULL, '673', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(30, 'BG', 'بلغاريا', 'Bulgaria', NULL, '359', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(31, 'BF', 'بوركينا فاسو', 'Burkina Faso', NULL, '226', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(32, 'BI', 'بوروندي', 'Burundi', NULL, '257', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(33, 'KH', 'كمبوديا', 'Cambodia', NULL, '855', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(34, 'CM', 'الكاميرون', 'Cameroon', NULL, '237', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(35, 'CA', 'كندا', 'Canada', NULL, '1', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(36, 'CV', 'الرأس الأخضر', 'Cape Verde', NULL, '238', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(37, 'KY', 'جزر كايمان', 'Cayman Islands', NULL, '1-345', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(38, 'CF', 'افريقيا الوسطى', 'Central African Republic', NULL, '236', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(39, 'TD', 'تشاد', 'Chad', NULL, '235', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(40, 'CL', 'تشيلي', 'Chile', NULL, '56', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(41, 'CN', 'الصين', 'China', NULL, '86', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(42, 'HK', 'هونغ كونغ', 'Hong Kong', NULL, '852', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(43, 'MO', 'ماكاو', 'Macao', NULL, '853', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(44, 'CX', 'جزيرة الكريسماس', 'Christmas Island', NULL, '61', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(45, 'CC', 'جزر كوكوس (كيلينغ)', 'Cocos (Keeling) Islands', NULL, '61', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(46, 'CO', 'كولومبيا', 'Colombia', NULL, '57', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(47, 'KM', 'جزر القمر', 'Comoros', NULL, '269', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(48, 'CK', 'جزر كوك', 'Cook Islands', NULL, '682', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(49, 'CR', 'كوستا ريكا', 'Costa Rica', NULL, '506', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(50, 'HR', 'كرواتيا', 'Croatia', NULL, '385', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(51, 'CU', 'كوبا', 'Cuba', NULL, '53', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(52, 'CY', 'قبرص', 'Cyprus', NULL, '357', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(53, 'CZ', 'الجمهورية التشيكية', 'Czech Republic', NULL, '420', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(54, 'DK', 'الدنمارك', 'Denmark', NULL, '45', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(55, 'DJ', 'جيبوتي', 'Djibouti', NULL, '253', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(56, 'DM', 'دومينيكا', 'Dominica', NULL, '1-767', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(57, 'DO', 'جمهورية الدومينيكان', 'Dominican Republic', NULL, '1-809', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(58, 'EC', 'الاكوادور', 'Ecuador', NULL, '593', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(59, 'EG', 'مصر', 'Egypt', NULL, '20', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(60, 'SV', 'السلفادور', 'El Salvador', NULL, '503', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(61, 'GQ', 'غينيا الاستوائية', 'Equatorial Guinea', NULL, '240', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(62, 'ER', 'إريتريا', 'Eritrea', NULL, '291', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(63, 'EE', 'استونيا', 'Estonia', NULL, '372', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(64, 'ET', 'أثيوبيا', 'Ethiopia', NULL, '251', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(65, 'FO', 'جزر فارو', 'Faroe Islands', NULL, '298', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(66, 'FJ', 'فيجي', 'Fiji', NULL, '679', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(67, 'FI', 'فنلندا', 'Finland', NULL, '358', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(68, 'FR', 'فرنسا', 'France', NULL, '33', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(69, 'GF', 'جيانا الفرنسية', 'French Guiana', NULL, '689', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(70, 'GA', 'الغابون', 'Gabon', NULL, '241', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(71, 'GM', 'غامبيا', 'Gambia', NULL, '220', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(72, 'GE', 'جورجيا', 'Georgia', NULL, '995', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(73, 'DE', 'ألمانيا', 'Germany', NULL, '49', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(74, 'GH', 'غانا', 'Ghana', NULL, '233', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(75, 'GI', 'جبل طارق', 'Gibraltar', NULL, '350', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(76, 'GR', 'يونان', 'Greece', NULL, '30', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(77, 'GL', 'غرينلاند', 'Greenland', NULL, '299', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(78, 'GD', 'غرينادا', 'Grenada', NULL, '1-473', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(79, 'GU', 'غوام', 'Guam', NULL, '1-671', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(80, 'GT', 'غواتيمالا', 'Guatemala', NULL, '502', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(81, 'GN', 'غينيا', 'Guinea', NULL, '224', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(82, 'GW', 'غينيا-بيساو', 'Guinea-Bissau', NULL, '245', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(83, 'GY', 'غيانا', 'Guyana', NULL, '592', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(84, 'HT', 'هايتي', 'Haiti', NULL, '509', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(85, 'HN', 'هندوراس', 'Honduras', NULL, '504', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(86, 'HU', 'هنغاريا', 'Hungary', NULL, '36', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(87, 'IS', 'أيسلندا', 'Iceland', NULL, '354', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(88, 'IN', 'الهند', 'India', NULL, '91', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(89, 'ID', 'أندونيسيا', 'Indonesia', NULL, '62', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(90, 'IR', 'جمهورية إيران الإسلامية', 'Iran, Islamic Republic of', NULL, '98', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(91, 'IQ', 'العراق', 'Iraq', NULL, '964', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(92, 'IE', 'أيرلندا', 'Ireland', NULL, '353', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(93, 'IM', 'جزيرة مان', 'Isle of Man', NULL, '44-1624', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(94, 'IL', 'إسرائيل', 'Israel', NULL, '972', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(95, 'IT', 'إيطاليا', 'Italy', NULL, '39', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(96, 'JM', 'جامايكا', 'Jamaica', NULL, '1-876', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(97, 'JP', 'اليابان', 'Japan', NULL, '81', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(98, 'JE', 'جيرسي', 'Jersey', NULL, '44-1534', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(99, 'JO', 'الأردن', 'Jordan', NULL, '962', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(100, 'KZ', 'كازاخستان', 'Kazakhstan', NULL, '7', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(101, 'KE', 'كينيا', 'Kenya', NULL, '254', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(102, 'KI', 'كيريباس', 'Kiribati', NULL, '686', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(103, 'KW', 'الكويت', 'Kuwait', NULL, '965', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(104, 'KG', 'قيرغيزستان', 'Kyrgyzstan', NULL, '996', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(105, 'LV', 'لاتفيا', 'Latvia', NULL, '371', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(106, 'LB', 'لبنان', 'Lebanon', NULL, '961', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(107, 'LS', 'ليسوتو', 'Lesotho', NULL, '266', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(108, 'LR', 'ليبيريا', 'Liberia', NULL, '231', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(109, 'LY', 'ليبيا', 'Libya', NULL, '218', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(110, 'LI', 'ليشتنشتاين', 'Liechtenstein', NULL, '423', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(111, 'LT', 'ليتوانيا', 'Lithuania', NULL, '370', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(112, 'LU', 'لوكسمبورغ', 'Luxembourg', NULL, '352', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(113, 'MK', 'مقدونيا، جمهورية', 'Macedonia', NULL, '389', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(114, 'MG', 'مدغشقر', 'Madagascar', NULL, '261', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(115, 'MW', 'ملاوي', 'Malawi', NULL, '265', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(116, 'MY', 'ماليزيا', 'Malaysia', NULL, '60', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(117, 'MV', 'جزر المالديف', 'Maldives', NULL, '960', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(118, 'ML', 'مالي', 'Mali', NULL, '223', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(119, 'MT', 'مالطا', 'Malta', NULL, '356', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(120, 'MH', 'جزر مارشال', 'Marshall Islands', NULL, '692', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(121, 'MR', 'موريتانيا', 'Mauritania', NULL, '222', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(122, 'MU', 'موريشيوس', 'Mauritius', NULL, '230', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(123, 'YT', 'مايوت', 'Mayotte', NULL, '262', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(124, 'MX', 'المكسيك', 'Mexico', NULL, '52', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(125, 'FM', 'ولايات ميكرونيزيا الموحدة', 'Micronesia', NULL, '691', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(126, 'MD', 'مولدوفا', 'Moldova', NULL, '373', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(127, 'MC', 'موناكو', 'Monaco', NULL, '377', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(128, 'MN', 'منغوليا', 'Mongolia', NULL, '976', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(129, 'ME', 'الجبل الأسود', 'Montenegro', NULL, '382', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(130, 'MS', 'مونتسيرات', 'Montserrat', NULL, '1-664', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(131, 'MA', 'المغرب', 'Morocco', NULL, '212', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(132, 'MZ', 'موزمبيق', 'Mozambique', NULL, '258', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(133, 'MM', 'ميانمار', 'Myanmar', NULL, '95', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(134, 'NA', 'ناميبيا', 'Namibia', NULL, '264', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(135, 'NR', 'ناورو', 'Nauru', NULL, '674', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(136, 'NP', 'نيبال', 'Nepal', NULL, '977', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(137, 'NL', 'هولندا', 'Netherlands', NULL, '31', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(138, 'AN', 'جزر الأنتيل الهولندية', 'Netherlands Antilles', NULL, '599', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(139, 'NC', 'كاليدونيا الجديدة', 'New Caledonia', NULL, '687', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(140, 'NZ', 'نيوزيلندا', 'New Zealand', NULL, '64', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(141, 'NI', 'نيكاراغوا', 'Nicaragua', NULL, '505', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(142, 'NE', 'النيجر', 'Niger', NULL, '227', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(143, 'NG', 'نيجيريا', 'Nigeria', NULL, '234', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(144, 'NU', 'نيوي', 'Niue', NULL, '683', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(145, 'NO', 'النرويج', 'Norway', NULL, '47', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(146, 'OM', 'عمان', 'Oman', NULL, '968', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(147, 'PK', 'باكستان', 'Pakistan', NULL, '92', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(148, 'PW', 'بالاو', 'Palau', NULL, '680', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(149, 'PS', 'فلسطين', 'Palestinian', NULL, '972', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(150, 'PA', 'بناما', 'Panama', NULL, '507', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(151, 'PY', 'باراغواي', 'Paraguay', NULL, '595', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(152, 'PE', 'بيرو', 'Peru', NULL, '51', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(153, 'PH', 'الفلبين', 'Philippines', NULL, '63', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(154, 'PN', 'بيتكيرن', 'Pitcairn', NULL, '870', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(155, 'PL', 'بولندا', 'Poland', NULL, '48', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(156, 'PT', 'البرتغال', 'Portugal', NULL, '351', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(157, 'PR', 'بويرتو ريكو', 'Puerto Rico', NULL, '1-787', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(158, 'QA', 'قطر', 'Qatar', NULL, '974', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(159, 'RO', 'رومانيا', 'Romania', NULL, '40', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(160, 'RU', 'الفيدرالية الروسية', 'Russian Federation', NULL, '7', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(161, 'RW', 'رواندا', 'Rwanda', NULL, '250', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(162, 'SH', 'سانت هيلينا', 'Saint Helena', NULL, '290', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(163, 'KN', 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', NULL, '1-869', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(164, 'LC', 'سانت لوسيا', 'Saint Lucia', NULL, '1-758', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(165, 'PM', 'سان بيار وميكلون', 'Saint Pierre and Miquelon', NULL, '508', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(166, 'VC', 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and Grenadines', NULL, '1-784', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(167, 'WS', 'ساموا', 'Samoa', NULL, '685', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(168, 'SM', 'سان مارينو', 'San Marino', NULL, '378', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(169, 'ST', 'ساو تومي وبرينسيبي', 'Sao Tome and Principe', NULL, '239', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(170, 'SA', 'المملكة العربية السعودية', 'Saudi Arabia', NULL, '966', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(171, 'SN', 'السنغال', 'Senegal', NULL, '221', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(172, 'RS', 'صربيا', 'Serbia', NULL, '381', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(173, 'SC', 'سيشيل', 'Seychelles', NULL, '248', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(174, 'SL', 'سيرا ليون', 'Sierra Leone', NULL, '232', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(175, 'SG', 'سنغافورة', 'Singapore', NULL, '65', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(176, 'SK', 'سلوفاكيا', 'Slovakia', NULL, '421', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(177, 'SI', 'سلوفينيا', 'Slovenia', NULL, '386', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(178, 'SB', 'جزر سليمان', 'Solomon Islands', NULL, '677', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(179, 'SO', 'الصومال', 'Somalia', NULL, '252', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(180, 'ZA', 'جنوب أفريقيا', 'South Africa', NULL, '27', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(181, 'ES', 'إسبانيا', 'Spain', NULL, '34', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(182, 'LK', 'سيريلانكا', 'Sri Lanka', NULL, '94', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(183, 'SD', 'السودان', 'Sudan', NULL, '249', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(184, 'SR', 'سورينام', 'Suriname', NULL, '597', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(185, 'SJ', 'جزر سفالبارد وجان ماين', 'Svalbard and Jan Mayen Islands', NULL, '47', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(186, 'SZ', 'سوازيلاند', 'Swaziland', NULL, '268', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(187, 'SE', 'السويد', 'Sweden', NULL, '46', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(188, 'CH', 'سويسرا', 'Switzerland', NULL, '41', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(189, 'SY', 'سوريا', 'Syrian Arab Republic', NULL, '963', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(190, 'TW', 'تايوان، جمهورية الصين', 'Taiwan, Republic of China', NULL, '886', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(191, 'TJ', 'طاجيكستان', 'Tajikistan', NULL, '992', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(192, 'TZ', 'تنزانيا', 'Tanzania', NULL, '255', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(193, 'TH', 'تايلاند', 'Thailand', NULL, '66', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(194, 'TG', 'توغو', 'Togo', NULL, '228', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(195, 'TK', 'توكيلاو', 'Tokelau', NULL, '690', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(196, 'TO', 'تونغا', 'Tonga', NULL, '676', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(197, 'TT', 'ترينداد وتوباغو', 'Trinidad and Tobago', NULL, '1-868', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(198, 'TN', 'تونس', 'Tunisia', NULL, '216', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(199, 'TR', 'تركيا', 'Turkey', NULL, '90', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(200, 'TM', 'تركمانستان', 'Turkmenistan', NULL, '993', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(201, 'TC', 'جزر تركس وكايكوس', 'Turks and Caicos Islands', NULL, '1-649', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(202, 'TV', 'توفالو', 'Tuvalu', NULL, '688', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(203, 'UG', 'أوغندا', 'Uganda', NULL, '256', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(204, 'UA', 'أوكرانيا', 'Ukraine', NULL, '380', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(205, 'AE', 'الإمارات العربية المتحدة', 'United Arab Emirates', NULL, '971', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(206, 'GB', 'المملكة المتحدة', 'United Kingdom', NULL, '44', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(207, 'US', 'الولايات المتحدة الأمريكية', 'United States of America', NULL, '1', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(208, 'UY', 'أوروغواي', 'Uruguay', NULL, '598', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(209, 'UZ', 'أوزبكستان', 'Uzbekistan', NULL, '998', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(210, 'VU', 'فانواتو', 'Vanuatu', NULL, '678', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(211, 'VE', 'فنزويلا', 'Venezuela', NULL, '58', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(212, 'VN', 'فيتنام', 'Viet Nam', NULL, '84', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(213, 'WF', 'واليس وفوتونا', 'Wallis and Futuna Islands', NULL, '681', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(214, 'YE', 'اليمن', 'Yemen', NULL, '967', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(215, 'ZM', 'زامبيا', 'Zambia', NULL, '260', '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(216, 'ZW', 'زيمبابوي', 'Zimbabwe', NULL, '263', '2017-11-08 13:20:40', '2017-11-08 13:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_events`
--

CREATE TABLE `smartend_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_events`
--

INSERT INTO `smartend_events` (`id`, `user_id`, `type`, `title`, `details`, `start_date`, `end_date`, `color`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'test note to my calendar', 'this is a test note to my calendar', '2018-12-07 00:00:00', '2018-12-07 00:00:00', NULL, 1, NULL, '2017-03-07 14:06:32', '2017-03-07 14:06:32'),
(2, 1, 1, 'meeting test for multi purposes', 'meeting test for multi purposes meeting test for multi purposes', '2018-11-07 16:03:00', '2018-11-07 16:03:00', NULL, 1, NULL, '2017-03-07 14:07:06', '2017-03-07 14:07:06'),
(3, 1, 2, 'Test the events on calendar', 'sample to test', '2018-12-07 16:00:00', '2018-12-07 18:00:00', NULL, 1, NULL, '2017-03-07 14:07:53', '2017-03-07 14:07:53'),
(4, 1, 3, 'Site task test will take half month', 'test task', '2018-11-07 00:00:00', '2018-11-22 00:00:00', NULL, 1, NULL, '2017-03-07 14:08:53', '2017-03-07 14:08:53'),
(5, 1, 0, 'my test note i just test', 'my test note i just test', '2018-09-22 00:00:00', '2018-09-22 00:00:00', NULL, 1, NULL, '2017-03-07 14:09:26', '2017-03-07 14:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_ltm_translations`
--

CREATE TABLE `smartend_ltm_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_maps`
--

CREATE TABLE `smartend_maps` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `details_jp` text COLLATE utf8mb4_unicode_ci,
  `icon` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_menus`
--

CREATE TABLE `smartend_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_menus`
--

INSERT INTO `smartend_menus` (`id`, `row_no`, `father_id`, `title_ar`, `title_en`, `title_jp`, `status`, `type`, `cat_id`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'القائمة الرئيسية', 'Main Menu', NULL, 1, 0, 0, '', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(2, 2, 0, 'روابط سريعة', 'Quick Links', NULL, 1, 0, 0, '', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(3, 1, 1, 'الرئيسية', 'Home', NULL, 1, 1, 0, 'home', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(4, 2, 1, 'من نحن', 'About', NULL, 1, 1, 0, 'topic/about', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(5, 3, 1, 'خدماتنا', 'Services', NULL, 1, 3, 2, '', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(6, 4, 1, 'أخبارنا', 'News', 'أخبارنا', 0, 2, 3, NULL, 1, 1, '2017-03-06 11:06:24', '2019-09-10 04:59:53'),
(11, 9, 1, 'المدونة', 'Blog', 'blog', 0, 2, 7, NULL, 1, 1, '2017-03-06 11:06:24', '2019-09-04 14:19:08'),
(13, 1, 2, 'الرئيسية', 'Home', NULL, 1, 1, 0, 'home', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(14, 2, 2, 'من نحن', 'About Us', NULL, 1, 1, 0, 'topic/about', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(15, 3, 2, 'المدونة', 'Blog', NULL, 1, 2, 7, '', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(16, 4, 2, 'الخصوصية', 'Privacy', NULL, 1, 1, 0, 'topic/privacy', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(17, 5, 2, 'الشروط والأحكام', 'Terms & Conditions', NULL, 1, 1, 0, 'topic/terms', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(18, 6, 2, 'اتصل بنا', 'Contact Us', NULL, 1, 1, 0, 'contact', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_migrations`
--

CREATE TABLE `smartend_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_migrations`
--

INSERT INTO `smartend_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_04_02_193005_create_translations_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2017_02_16_230800_create_webmaster_settings_table', 1),
(5, '2017_02_16_230846_create_webmaster_sections_table', 1),
(6, '2017_02_16_230900_create_webmaster_banners_table', 1),
(7, '2017_02_16_231036_create_webmails_groups_table', 1),
(8, '2017_02_16_231044_create_webmails_files_table', 1),
(9, '2017_02_16_231053_create_webmails_table', 1),
(10, '2017_02_16_231114_create_topics_table', 1),
(11, '2017_02_16_231142_create_settings_table', 1),
(12, '2017_02_16_231230_create_sections_table', 1),
(13, '2017_02_16_231240_create_photos_table', 1),
(14, '2017_02_16_231248_create_menus_table', 1),
(15, '2017_02_16_231259_create_maps_table', 1),
(16, '2017_02_16_231306_create_events_table', 1),
(17, '2017_02_16_231317_create_countries_table', 1),
(18, '2017_02_16_231327_create_contacts_groups_table', 1),
(19, '2017_02_16_231339_create_contacts_table', 1),
(20, '2017_02_16_231346_create_comments_table', 1),
(21, '2017_02_16_231352_create_banners_table', 1),
(22, '2017_02_16_231359_create_analytics_visitors_table', 1),
(23, '2017_02_16_231409_create_analytics_pages_table', 1),
(24, '2017_02_28_095712_create_permissions_table', 1),
(25, '2017_08_13_144908_create_attach_files_table', 1),
(26, '2017_10_06_115859_create_related_topics_table', 1),
(27, '2017_10_07_144738_create_topic_categories_table', 1),
(28, '2017_10_08_125452_create_webmaster_section_fields_table', 1),
(29, '2017_10_23_162021_create_topic_fields_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `smartend_password_resets`
--

CREATE TABLE `smartend_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_payments`
--

CREATE TABLE `smartend_payments` (
  `id` int(11) NOT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_file` varchar(255) DEFAULT NULL,
  `limit_signature_ar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `limit_signature_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `limit_signature_jp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `anually_title_ar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anually_title_en` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anually_title_jp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anually_price` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anually_photo_file` varchar(255) DEFAULT NULL,
  `anually_limit_signature_ar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `anually_limit_signature_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `anually_limit_signature_jp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `monthly` varchar(10) DEFAULT NULL,
  `anually` varchar(10) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` varchar(100) DEFAULT NULL,
  `updated_at` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smartend_payments`
--

INSERT INTO `smartend_payments` (`id`, `title_ar`, `title_jp`, `title_en`, `price`, `photo_file`, `limit_signature_ar`, `limit_signature_en`, `limit_signature_jp`, `anually_title_ar`, `anually_title_en`, `anually_title_jp`, `anually_price`, `anually_photo_file`, `anually_limit_signature_ar`, `anually_limit_signature_en`, `anually_limit_signature_jp`, `monthly`, `anually`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`) VALUES
(4, '비어 있는', '無料', 'Free', '0', '15674916226954.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 5 회</div></div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">5 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計5回</div></div>', '비어 있는', 'Free', '無料', '0', '15674916223656.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 5 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">5 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計5回</div></div>', 'yes', 'yes', 1, 1, '2019-09-03 06:20:22', '2019-09-03 12:24:59', 1),
(5, '인원', '人事', 'Personnel', '5', '15674919831247.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 10 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"margin: 0px 0px 5px; padding: 0px; box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"margin: 0px; padding: 0px; box-sizing: border-box; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">10 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計10回</div></div>', '인원', 'Personnel', '人事', '10', '15674919834120.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 10 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">10 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計10回</div></div>', 'yes', 'yes', 1, 1, '2019-09-03 06:26:23', '2019-09-09 18:03:18', 1),
(6, '표준', '標準', 'Standard', '10', '15674927729516.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 5 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">5 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計5回</div></div>', '표준', 'Standard', '標準', '25', '15674927725786.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 30 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">30 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計30回</div></div>', 'yes', 'yes', 1, 1, '2019-09-03 06:39:32', '2019-09-10 11:06:17', 1),
(7, '찬성', 'プロ', 'Pro', '20', '15674929224843.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">No limit</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">No limit</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">No limit</div></div>', '찬성', 'Pro', 'プロ', '40', '15674929221335.png', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px 0px 5px; padding: 0px;\">서명을위한 전송 한도</div><div class=\"times\" style=\"box-sizing: border-box; font-family: Roboto, sans-serif; font-size: 12px; text-align: center; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative;\">총 5 회</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">Limit for Sending for Signature</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">5 Times total</div></div>', '<div dir=\"ltr\"><div class=\"name\" style=\"box-sizing: border-box; margin: 0px 0px 5px; padding: 0px; color: rgb(46, 34, 30); font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">署名の送信の制限</div><div class=\"times\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; color: rgb(239, 99, 76); position: relative; font-family: Roboto, sans-serif; font-size: 12px; text-align: center;\">合計5回</div></div>', 'yes', 'yes', 1, 1, '2019-09-03 06:42:02', '2019-09-10 11:07:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `smartend_permissions`
--

CREATE TABLE `smartend_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_status` tinyint(4) NOT NULL DEFAULT '0',
  `add_status` tinyint(4) NOT NULL DEFAULT '0',
  `edit_status` tinyint(4) NOT NULL DEFAULT '0',
  `delete_status` tinyint(4) NOT NULL DEFAULT '0',
  `analytics_status` tinyint(4) NOT NULL DEFAULT '0',
  `inbox_status` tinyint(4) NOT NULL DEFAULT '0',
  `newsletter_status` tinyint(4) NOT NULL DEFAULT '0',
  `calendar_status` tinyint(4) NOT NULL DEFAULT '0',
  `banners_status` tinyint(4) NOT NULL DEFAULT '0',
  `settings_status` tinyint(4) NOT NULL DEFAULT '0',
  `webmaster_status` tinyint(4) NOT NULL DEFAULT '0',
  `data_sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_permissions`
--

INSERT INTO `smartend_permissions` (`id`, `name`, `view_status`, `add_status`, `edit_status`, `delete_status`, `analytics_status`, `inbox_status`, `newsletter_status`, `calendar_status`, `banners_status`, `settings_status`, `webmaster_status`, `data_sections`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Webmaster', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4,5,6,7,8,9,8,9,8,9', 1, 1, NULL, '2017-11-08 13:20:40', '2019-09-04 16:33:34'),
(2, 'Website Manager', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '1,2,3,4,5,6,7,8,9', 1, 1, NULL, '2017-11-08 13:20:40', '2017-11-08 13:20:40'),
(3, 'Limited User', 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, '1,2,3,4,5,6,7,8,9', 1, 1, NULL, '2017-11-08 13:20:40', '2017-11-08 13:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_photos`
--

CREATE TABLE `smartend_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_photos`
--

INSERT INTO `smartend_photos` (`id`, `topic_id`, `file`, `title`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 9, '14888159356958.jpg', '14888146712437', 1, 1, NULL, '2017-03-06 13:58:55', '2017-03-06 13:58:55'),
(3, 9, '14888159357505.jpg', '14888155324481', 2, 1, NULL, '2017-03-06 13:58:55', '2017-03-06 13:58:55'),
(4, 12, '14888160421353.jpg', '14888159357505', 1, 1, NULL, '2017-03-06 14:00:42', '2017-03-06 14:00:42'),
(6, 12, '14888162827801.jpg', '14888159356958', 2, 1, NULL, '2017-03-06 14:04:42', '2017-03-06 14:04:42');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_related_topics`
--

CREATE TABLE `smartend_related_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `topic2_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_sections`
--

CREATE TABLE `smartend_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_sections`
--

INSERT INTO `smartend_sections` (`id`, `title_ar`, `title_en`, `title_jp`, `photo`, `icon`, `status`, `visits`, `webmaster_id`, `father_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_title_jp`, `seo_description_ar`, `seo_description_en`, `seo_description_jp`, `seo_keywords_ar`, `seo_keywords_en`, `seo_keywords_jp`, `seo_url_slug_ar`, `seo_url_slug_en`, `seo_url_slug_jp`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'legal guide', NULL, NULL, 'fa-desktop', 1, 633, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:11:25', '2019-09-04 12:31:33'),
(3, NULL, 'case study', NULL, NULL, 'fa-motorcycle', 1, 423, 7, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:12:24', '2019-09-11 06:22:03'),
(5, '간증', 'TESTIMONIALS', '証言', NULL, 'fa-connectdevelop', 1, 1023, 7, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:13:41', '2019-09-11 06:20:38'),
(9, '미국', 'United States', 'アメリカ', '15680517222713.png', NULL, 1, 92, 2, 0, 1, '미국', 'United States', 'アメリカ', NULL, NULL, NULL, NULL, NULL, NULL, '', 'united-states', NULL, 1, 1, '2019-08-30 18:45:43', '2019-09-11 04:38:13'),
(10, '일본', 'Japan', '日本', '15674099095182.png', NULL, 1, 39, 2, 0, 2, '일본', 'Japan', '日本', NULL, NULL, NULL, NULL, NULL, NULL, '', 'japan', NULL, 1, 1, '2019-08-30 18:46:37', '2019-09-09 06:34:18'),
(11, '대한민국', 'Korea', '韓国', '15674099237560.png', NULL, 1, 22, 2, 0, 3, '대한민국', 'Korea', '韓国', NULL, NULL, NULL, NULL, NULL, NULL, '', 'korea', NULL, 1, 1, '2019-08-30 18:48:00', '2019-09-09 06:34:22'),
(12, 'service contract', 'service contract', 'service contract', NULL, 'fa-android', 1, 5, 7, 3, 1, 'service contract', 'service contract', 'service contract', NULL, NULL, NULL, NULL, NULL, NULL, 'service-contract', 'service-contract', NULL, 1, NULL, '2019-09-02 11:55:45', '2019-09-04 11:21:48'),
(14, '인프라 산업', 'infrastructure industry', 'インフラ産業', NULL, NULL, 1, 0, 7, 3, 2, '인프라 산업', 'infrastructure industry', 'インフラ産業', NULL, NULL, NULL, NULL, NULL, NULL, '', 'infrastructure-industry', NULL, 1, NULL, '2019-09-02 11:57:36', '2019-09-02 11:57:36'),
(16, '한글 test', 'english test', 'ニオhンゴテス', NULL, NULL, 1, 0, 2, 0, 4, '한글 test', 'english test', 'ニオhンゴテス', NULL, NULL, NULL, NULL, NULL, NULL, 'test', 'english-test', NULL, 1, NULL, '2019-09-10 21:42:08', '2019-09-10 21:42:08');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_settings`
--

CREATE TABLE `smartend_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_desc_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_keywords_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_jp` text COLLATE utf8mb4_unicode_ci,
  `site_webmails` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_messages_status` tinyint(4) DEFAULT NULL,
  `notify_comments_status` tinyint(4) DEFAULT NULL,
  `notify_orders_status` tinyint(4) DEFAULT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_status` tinyint(4) NOT NULL,
  `close_msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link7` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link8` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link9` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link10` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_t3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_fav` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_apple` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_type` tinyint(4) DEFAULT NULL,
  `style_bg_type` tinyint(4) DEFAULT NULL,
  `style_bg_pattern` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_subscribe` tinyint(4) DEFAULT NULL,
  `style_footer` tinyint(4) DEFAULT NULL,
  `style_header` tinyint(4) DEFAULT NULL,
  `style_footer_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_preload` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_settings`
--

INSERT INTO `smartend_settings` (`id`, `site_title_ar`, `site_title_en`, `site_title_jp`, `site_desc_ar`, `site_desc_en`, `site_desc_jp`, `site_keywords_ar`, `site_keywords_en`, `site_keywords_jp`, `site_webmails`, `notify_messages_status`, `notify_comments_status`, `notify_orders_status`, `site_url`, `site_status`, `close_msg`, `social_link1`, `social_link2`, `social_link3`, `social_link4`, `social_link5`, `social_link6`, `social_link7`, `social_link8`, `social_link9`, `social_link10`, `contact_t1_ar`, `contact_t1_en`, `contact_t1_jp`, `contact_t3`, `contact_t4`, `contact_t5`, `contact_t6`, `contact_t7_ar`, `contact_t7_en`, `contact_t7_jp`, `style_logo_ar`, `style_logo_en`, `style_logo_jp`, `style_fav`, `style_apple`, `style_color1`, `style_color2`, `style_type`, `style_bg_type`, `style_bg_pattern`, `style_bg_color`, `style_bg_image`, `style_subscribe`, `style_footer`, `style_header`, `style_footer_bg`, `style_preload`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '', 'Coffee landing page', NULL, '', '', NULL, '', 'key, words, website, web', NULL, '', 0, 0, 0, '', 1, 'Website under maintenance \r\n<h1>Comming SOON</h1>', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '', '', NULL, '', '', '', '', '', '', NULL, '15671436274248.png', '15671436276330.png', '15671436276674.png', '15668992483341.png', '14888091198179.png', '#0cbaa4', '#2e3e4e', 0, 0, NULL, '#2e3e4e', NULL, 0, 1, 0, '', 0, 1, 1, '2017-03-06 11:06:23', '2019-09-10 05:02:01');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_tags`
--

CREATE TABLE `smartend_tags` (
  `id` int(11) NOT NULL,
  `title_ar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_en` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `title_jp` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `created_at` varchar(40) DEFAULT NULL,
  `updated_at` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smartend_tags`
--

INSERT INTO `smartend_tags` (`id`, `title_ar`, `title_en`, `title_jp`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, '한 달에 6-10', '6-10 a month', '月に6〜10', 1, '1', '1', '2019-09-04 07:30:16', '2019-09-04 07:41:59'),
(4, '1 ~ 5 개월', '1 to 5 months', '1〜5か月', 1, '1', '1', '2019-09-04 07:46:16', '2019-09-04 07:46:35'),
(5, '한달에 10-50', '10-50 a month', '10-50の契約件数', 1, '1', NULL, '2019-09-10 21:59:52', '2019-09-10 21:59:52');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_topics`
--

CREATE TABLE `smartend_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci,
  `details_en` longtext COLLATE utf8mb4_unicode_ci,
  `details_jp` longtext COLLATE utf8mb4_unicode_ci,
  `summary_ar` longtext COLLATE utf8mb4_unicode_ci,
  `summary_en` longtext COLLATE utf8mb4_unicode_ci,
  `summary_jp` longtext COLLATE utf8mb4_unicode_ci,
  `bannertext_ar` longtext COLLATE utf8mb4_unicode_ci,
  `bannertext_en` longtext COLLATE utf8mb4_unicode_ci,
  `bannertext_jp` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `photo_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_file` text COLLATE utf8mb4_unicode_ci,
  `audio_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_jp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci,
  `section_ids` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_topics`
--

INSERT INTO `smartend_topics` (`id`, `title_ar`, `title_en`, `title_jp`, `details_ar`, `details_en`, `details_jp`, `summary_ar`, `summary_en`, `summary_jp`, `bannertext_ar`, `bannertext_en`, `bannertext_jp`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_title_jp`, `seo_description_ar`, `seo_description_en`, `seo_description_jp`, `seo_keywords_ar`, `seo_keywords_en`, `seo_keywords_jp`, `seo_url_slug_ar`, `seo_url_slug_en`, `seo_url_slug_jp`, `tags`, `section_ids`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'Terms & Conditions', 'Terms & Conditions', NULL, 'Terms & Conditions', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(6, '미국의 전자 서명 합법성', 'eSignature Legality in United State', '米国の電子署名の合法性', '<div class=\"advantages__content container-width-medium\">\r\n  \r\n          <div class=\"advantages__content-wrap advantages__content-wrap--leaglityCountry\">\r\n            <div class=\"advantages__title sectionTitle\">\r\n              <div class=\"subtitle\"><p>우크라이나의 법률 시스템은 로마 민법과 영미 공통 법률 시스템이 혼합되어 있습니다. 민법은 가족 관계, 재산, 승계, 계약 및 형법과 같은 분야에서 운영되는 반면, 일반법의 기원과 법령은 헌법, 절차, 법인 법, 과세, 보험, 노사 관계, 은행과 같은 분야에서 분명합니다. 그리고 통화.               </p><br></div></div><div class=\"legality__description\"> <p>\r\n                민법 시스템은 구 로마 법에서 파생 된 개념을 기반으로하며, 시민과 법률 전문가 모두가 쉽게 정리하고 접근 할 수있는 포괄적 인 규칙과 원칙 세트에 의존한다는 점에서 구별됩니다. 성문화 법은 현재 환경을 반영하기 위해 정기적으로 개정되며 민법 국가에서는 이전의 법원 판례보다 우선 순위가 더 강합니다. 민법 국가는 유럽 대륙, 중남미, 중동, 아시아 및 아프리카의 대다수를 포함하여 세계 법률 시스템의 65 % 이상을 차지합니다.</p></div></div></div>', '<div class=\"advantages__content container-width-medium\">\r\n          <div class=\"advantages__content-wrap advantages__content-wrap--leaglityCountry\">\r\n            <div class=\"legality__description\">\r\n              <p>United State\'s legal system is a mixture of Roman civil law and Anglo-American common law systems.  Civil law operates in areas such as family relations, property, succession, contract, and criminal law, while statutes and principles of common law origin are evident in such areas as constitutional law, procedure, corporations law, taxation, insurance, labour relations, banking and currency.</p></div>\r\n			  <div class=\"legality__description\">\r\n              <p>\r\n                Civil law systems are based on concepts derived from old Roman law, distinguishable by their reliance on having a comprehensive set of rules and principles codified and easily accessible to both citizens and legal professionals. Codified laws are regularly revised to reflect the current environment, and have stronger emphasis in civil law countries than any precedent set by earlier court cases. Civil law countries cover more than 65% of world’s legal system, including the majority of continental Europe, Central and South America, the Middle East, Asia and Africa.\r\n              </p>\r\n            </div>\r\n            <!-- /.legality__description -->\r\n            <!-- END LEGALITY DESCRIPTION -->\r\n          </div>\r\n          <!-- /.advantages__content-wrap -->\r\n        </div>', '<div class=\"advantages__content container-width-medium\">\r\n  \r\n          <div class=\"advantages__content-wrap advantages__content-wrap--leaglityCountry\">\r\n            <div class=\"advantages__title sectionTitle\">\r\n              <div class=\"subtitle\"><p>ウクライナの法制度は、ローマの民法と英米の慣習法制度が混在しています。民法は家族関係、財産、承継、契約、刑法などの分野で機能しますが、憲法、手続き、会社法、課税、保険、労使関係、銀行などの分野では慣習法の制定法と原則が明白ですと通貨。</p><br></div></div><div class=\"legality__description\">\r\n  \r\n              <p>民法システムは、ローマ法に由来する概念に基づいており、包括的なルールと原則のセットを成文化し、市民と法律専門家の両方が簡単にアクセスできることに依存していることで区別できます。体系化された法律は、現在の環境を反映するために定期的に改訂されており、民法国では、以前の裁判で設定された先例よりも強調されています。民法国は、ヨーロッパ大陸、中南米、中東、アジア、アフリカの大部分を含む世界の法制度の65％以上をカバーしています。\r\n              </p>\r\n  \r\n            </div>\r\n            <!-- /.legality__description -->\r\n            <!-- END LEGALITY DESCRIPTION -->\r\n          </div>\r\n          <!-- /.advantages__content-wrap -->\r\n  \r\n        </div>', NULL, '<section class=\"legalityCountryText\">\r\n        <div class=\"legalityCountryText__content container-width-medium\">\r\n  \r\n          <div class=\"legalityCountryText__items\">\r\n  \r\n            <div class=\"legalityCountryText__item legalityCountryText__item--left\">\r\n              <div class=\"legalityCountryText__text\">\r\n                <h3 class=\"title itemTitle\">eSignature Legality Summary</h3>\r\n  \r\n                <p class=\"text\">\r\n                  Under Ukrainian law, a written signature is not necessarily required for a valid contract - contracts are generally valid if legally competent parties reach an agreement, whether they agree verbally, electronically or in a physical paper document (Article 207 of the Civil Code of Ukraine). Article 8 of the Law of Ukraine No. 851-IV, “On Electronic Documents and Electronic Documents Circulation,” specifically confirms that documents cannot be denied enforceability merely because they are concluded electronically. To prove a valid contract, parties sometimes have to present evidence in court. Leading digital transaction management solutions can provide electronic records that are admissible in evidence where (a) directly prescribed by law, or (b) the parties agreed to the use of electronic signatures to support the existence, authenticity and valid acceptance of a signed document. Furthermore, a handwritten signature is first required before permitting the use of an electronic or digital signature, if otherwise not prescribed by law.\r\n                </p>\r\n              </div>\r\n              <!-- /.legalityCountryText__text -->\r\n  \r\n              <div class=\"legalityCountryText__text\">\r\n                <h3 class=\"title itemTitle\">Use Cases for Standard Electronic Signature (SES)</h3>\r\n  \r\n                <div class=\"subtitle\">\r\n                  Use cases where an SES is typically appropriate include:\r\n                </div>\r\n  \r\n                <ul class=\"text-list\">\r\n                  <li>\r\n                    commercial agreements between corporate entities, including NDAs, procurement documents\r\n                  </li>\r\n                  <li>\r\n                    consumer agreements, including new retail account opening documents\r\n                  </li>\r\n                </ul>\r\n                <!-- /.text-list -->\r\n  \r\n                <p class=\"text\">\r\n                  NOTE: electronic signatures are not appropriate if the parties have not agreed in advance, with handwritten signatures, to the use of electronic signatures.\r\n                </p>\r\n              </div>\r\n              <!-- /.legalityCountryText__text -->\r\n  \r\n              <div class=\"legalityCountryText__text\">\r\n                <h3 class=\"title itemTitle\">Use Cases for Other Types of Electronic Signature (e.g. Digital Signature, AES[1], QES[2])</h3>\r\n  \r\n                <div class=\"subtitle\">\r\n                  Use cases where an electronic signature other than SES may be required include:\r\n                </div>\r\n  \r\n                <ul class=\"text-list\">\r\n                  <li>\r\n                    QES - acts by public authorities, municipal bodies, state enterprises, institutions and organizations (Article 5 of the E-Signature Law)\r\n                  </li>\r\n                  <li>\r\n                    QES - privately owned legal entities and individuals in cases of filing electronic documents or reporting in electronic form to state authorities (Order of the State Tax Administration of Ukraine “On Submission of Electronic Tax Reporting” dated 10 April 2008 No. 233; Order of the State Statistics Service of Ukraine “On Approval of Rules on the Submission of Electronic Reporting to State Statistics Authorities” dated 12 January 2011 etc.)\r\n                  </li>\r\n                  <li>\r\n                    AES or QES - entities which introduce or maintain internal electronic document management (Order of the Ministry of Justice of Ukraine “On the Approval of the Rules on Management of the Electronic Documents in the Recordkeeping and Preparation for Their Transfer to Archives” dated 11 November 2014 No. 1886/5)\r\n                  </li>\r\n                  <li>\r\n                    AES or QES – promissory notes (Art. 5, Law of Ukraine “On Circulation of Promissory Notes)\r\n                  </li>\r\n                  <li>\r\n                    QES – transaction documentation, including invoices and acts of acceptance of services\r\n                  </li>\r\n                </ul>\r\n                <!-- /.text-list -->\r\n              </div>\r\n              <!-- /.legalityCountryText__text -->\r\n  \r\n              <div class=\"legalityCountryText__text\">\r\n                <h3 class=\"title itemTitle\">Use Cases That Are Not Typically Appropriate for Electronic Signatures or Digital Transaction Management</h3>\r\n  \r\n                <div class=\"subtitle\">\r\n                  Use cases that are specifically barred from digital or electronic processes or that include explicit requirements, such as handwritten (e.g. wet ink) signatures or formal notarial process that are not usually compatible with electronic signatures or digital transaction management.\r\n                </div>\r\n  \r\n                <ul class=\"text-list\">\r\n                  <li>\r\n                    Notarization - agreements on sale and purchase of real estate objects\r\n                  </li>\r\n                  <li>\r\n                    Notarization - agreements on lease of buildings for three and more year term\r\n                  </li>\r\n                  <li>\r\n                    Notarization - certain contracts in family law, such as marriage contracts and alimony agreements\r\n                  </li>\r\n                  <li>\r\n                    Handwritten - certificate of inheritance\r\n                  </li>\r\n                  <li>\r\n                    Handwritten - a document that according to the applicable law can be executed only in one single original (except for the existence of centralized storage of electronic original documents)\r\n                  </li>\r\n                </ul>\r\n                <!-- /.text-list -->\r\n  \r\n                <p class=\"text\">\r\n                  [1] An AES is an “advanced electronic signature”, a type of electronic signature that meets the following requirements: (a) it is uniquely linked to the signatory; (b) it is capable of identifying the signatory; (c) it is created using means that are under the signatory’s sole control; and (d) it is linked to other electronic data in such a way that any alteration to the said data can be detected.\r\n                </p>\r\n  \r\n                <p class=\"text\">\r\n                  [2] A QES is a specific digital signature implementation that has met the particular specifications of a government, including using a secure signature creation device, and been certified as ‘qualified’ by either that government or a party contracted by that government.\r\n                </p>\r\n              </div>\r\n              <!-- /.legalityCountryText__text -->\r\n            </div>\r\n            <!-- /.legalityCountryText__item -->\r\n  \r\n            <div class=\"legalityCountryText__item legalityCountryText__item--right\">\r\n              <div class=\"legalityCountryText__links\">\r\n  \r\n                <h3 class=\"legalityCountryText__h3 itemTitle\">External Resources</h3>\r\n  \r\n                <ul class=\"legalityCountryText__list text-list\">\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      Law of Ukraine No. 852-IV on Electronic Digital Signature (the “E-signature Law”) (2003)\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      The Civil Code of Ukraine\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      The Code of Criminal Procedure\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      The Code of Administrative Procedure\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      Law of Ukraine 851-IV on Electronic Documents and Electronic Documents Circulation (2003)\r\n                    </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#\">\r\n                      Resolution of the Cabinet of Ministers of Ukraine No. 1452 on Adoption of the Procedure of the Electronic Digital Signature Utilization by State Authorities, Local Self-Administration Bodies, State-Owned Enterprises, Institutions and Organizations (2004)\r\n                    </a>\r\n                  </li>\r\n                </ul>\r\n                <!-- /.legalityCountryText__list -->\r\n  \r\n                <div class=\"legalityCountryText__card\">\r\n                  <h3 class=\"title itemTitle\">Request More Info</h3>\r\n                  <p class=\"text\">\r\n                    Talk to our Sales Team about all of you business needs.\r\n                  </p>\r\n                  <a href=\"#\" class=\"link button-large\">Contact Sales</a>\r\n                </div>\r\n                <!-- /.legalityCountryText__card -->\r\n  \r\n              </div>\r\n              <!-- /.legalityCountryText__links -->\r\n            </div>\r\n            <!-- /.legalityCountryText__item -->\r\n            \r\n          </div>\r\n          <!-- /.legalityCountryText__items -->\r\n  \r\n          <div class=\"legalityCountryText__banner\">\r\n            <p class=\"text\">\r\n              DISCLAIMER: The information on this site is for general information purposes only. You use this information at your own risk. For legal advice or representation, contact a licensed attorney in your area. Laws may change quickly, so DocuSign, Inc. cannot guarantee that all the information on this form is current or correct. DOCUSIGN DOES NOT GIVE ANY EXPRESS OR IMPLIED WARRANTIES OF MERCHANTABILITY, SUITABILITY, OR COMPLETENESS OF THIS INFORMATION. TO THE EXTENT PERMITTED UNDER APPLICABLE LAW, NEITHER DOCUSIGN, NOR ITS AGENTS, OFFICERS, EMPLOYEES, OR AFFILIATES, ARE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, LOSS OF USE OR PROFITS, OR BUSINESS INTERRUPTION), EVEN IF DOCUSIGN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT, ARISING IN ANY WAY OUT OF THE USE OF OR INABILITY TO USE THIS INFORMATION\r\n            </p>\r\n            <span class=\"date\">\r\n              Last updated: November 01, 2018\r\n            </span>\r\n          </div>\r\n          <!-- /.legalityCountryText__banner -->\r\n  \r\n          <a href=\"#\" class=\"legalityCountryText__link\">\r\n            Submit feedback or corrections for this information <span>+</span>\r\n          </a>\r\n  \r\n        </div>\r\n        <!-- /.legalityCountryText__content container-width-medium -->\r\n      </section>', NULL, NULL, '<div class=\"header__subtitle header__subtitle--legalityCountry\">\r\n              *The information on this site is \"AS IS\" and for general information purposes only. More\r\n            </div>\r\n<p class=\"header__subtitle-text--legalityCountry\">\r\n              Electronic Signature has been recognized by law in Ukraine since 2003, with the passage of The Law of Ukraine No. 852-IV \"On Electronic Digital Signature.\"\r\n            </p>\r\n<div class=\"header__items--legalityCountry\">\r\n  \r\n              <div class=\"header__item--legalityCountry\">\r\n                <div class=\"name\">\r\n                  Court-Admissible\r\n                  <div class=\"hintWrap\">\r\n  \r\n                    <button class=\"hintButton\">\r\n                      <img src=\"img/svg/hint.svg\" alt=\"Hint\">\r\n                    </button>\r\n  \r\n                    <div class=\"hintText\">\r\n                      <div class=\"title\">Info Message</div>\r\n                      <p class=\"text\">\r\n                        This is an example top alert. You can edit what u wish.\r\n                      </p>\r\n                      <button class=\"close\">\r\n                        <img src=\"img/svg/close-icon-small.svg\" alt=\"Close\">\r\n                      </button>\r\n                      <span class=\"progressLine\"></span>\r\n                    </div>\r\n  \r\n                  </div>\r\n                </div>\r\n                <div class=\"option\">Yes</div>\r\n              </div>\r\n              <!-- /.header__item -->\r\n  \r\n              <div class=\"header__item--legalityCountry\">\r\n                <div class=\"name\">\r\n                  General Business Use\r\n                  <div class=\"hintWrap\">\r\n  \r\n                    <button class=\"hintButton\">\r\n                      <img src=\"img/svg/hint.svg\" alt=\"Hint\">\r\n                    </button>\r\n  \r\n                    <div class=\"hintText\">\r\n                      <div class=\"title\">Info Message</div>\r\n                      <p class=\"text\">\r\n                        This is an example top alert. You can edit what u wish.\r\n                      </p>\r\n                      <button class=\"close\">\r\n                        <img src=\"img/svg/close-icon-small.svg\" alt=\"Close\">\r\n                      </button>\r\n                      <span class=\"progressLine\"></span>\r\n                    </div>\r\n  \r\n                  </div>\r\n                </div>\r\n                <div class=\"option\">Yes</div>\r\n              </div>\r\n              <!-- /.header__item -->\r\n  \r\n              <div class=\"header__item--legalityCountry\">\r\n                <div class=\"name\">\r\n                  eSignature Legal Model\r\n                  <div class=\"hintWrap\">\r\n  \r\n                    <button class=\"hintButton\">\r\n                      <img src=\"img/svg/hint.svg\" alt=\"Hint\">\r\n                    </button>\r\n  \r\n                    <div class=\"hintText\" style=\"top: -91px;\">\r\n                      <div class=\"title\">Info Message</div>\r\n                      <p class=\"text\">\r\n                        This is an example top alert. You can edit what u wish.\r\n                      </p>\r\n                      <button class=\"close\">\r\n                        <img src=\"img/svg/close-icon-small.svg\" alt=\"Close\">\r\n                      </button>\r\n                      <span class=\"progressLine\"></span>\r\n                    </div>\r\n  \r\n                  </div>\r\n                </div>\r\n                <div class=\"option\">Tiered</div>\r\n              </div>\r\n              <!-- /.header__item -->\r\n  \r\n            </div>', NULL, '2017-03-06', NULL, NULL, '14888139889647.jpg', NULL, '', NULL, NULL, 1, 12, 2, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9', 1, 1, '2017-03-06 13:26:28', '2019-09-10 07:38:35'),
(7, '지구는 임신 수신', 'Gravida tellus suscipit', '地球は妊娠して受け取り', '<p><br></p>', '<div class=\"advantages__content container-width-medium\">\r\n          <div class=\"advantages__content-wrap advantages__content-wrap--leaglityCountry\">\r\n            <div class=\"legality__description\">\r\n              <p>Japan\'s legal system is a mixture of Roman civil law and Anglo-American common law systems.  Civil law operates in areas such as family relations, property, succession, contract, and criminal law, while statutes and principles of common law origin are evident in such areas as constitutional law, procedure, corporations law, taxation, insurance, labour relations, banking and currency.</p></div>\r\n			  <div class=\"legality__description\">\r\n              <p>\r\n                Civil law systems are based on concepts derived from old Roman law, distinguishable by their reliance on having a comprehensive set of rules and principles codified and easily accessible to both citizens and legal professionals. Codified laws are regularly revised to reflect the current environment, and have stronger emphasis in civil law countries than any precedent set by earlier court cases. Civil law countries cover more than 65% of world’s legal system, including the majority of continental Europe, Central and South America, the Middle East, Asia and Africa.\r\n              </p>\r\n            </div>\r\n            <!-- /.legality__description -->\r\n            <!-- END LEGALITY DESCRIPTION -->\r\n          </div>\r\n          <!-- /.advantages__content-wrap -->\r\n        </div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-06', NULL, NULL, '14888140236712.jpg', NULL, '', NULL, NULL, 1, 4, 2, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', 1, 1, '2017-03-06 13:27:03', '2019-09-09 06:33:34'),
(8, '나는 매우 행복 시대 해요', 'Curabitur sit amet era', '私はとても幸せな時代ですよ', '<p><br></p>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"text-align: justify; \">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-06', NULL, NULL, '14888140657735.jpg', NULL, '', NULL, NULL, 1, 1, 2, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', 1, 1, '2017-03-06 13:27:45', '2019-09-09 06:24:17'),
(9, '매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다', 'Title English', '非常に興味深い話についての長い見出しですが、もっと長くする必要があります', '<p><span style=\"color: rgb(158, 160, 165); font-family: Roboto, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque.</span><font color=\"#9ea0a5\" face=\"Roboto, sans-serif\"><span style=\"font-size: 14px;\">매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다</span></font><br></p>', '<div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Detail English&nbsp;</div>', '<p>非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります<br></p>', '<p>매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다<br></p>', '<p><span style=\"font-size: 11.832px;\">Summery English&nbsp;</span></p>', '<p>非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります<br></p>', '<p>매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다매우 흥미로운 이야기에 대한 긴 헤드 라인이지만 더 길어야합니다<br></p>', '<p><span style=\"font-size: 11.832px;\">Banner Text&nbsp;</span></p>', '<p>非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります非常に興味深い話についての長い見出しですが、もっと長くする必要があります<br></p>', '2019-08-30', NULL, NULL, '15671592708167.jpg', NULL, '', NULL, NULL, 1, 123, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, 1, 1, '2017-03-06 13:37:21', '2019-09-10 21:51:21'),
(10, 'Aliquam suscipit, lacus a iaculis adipiscing, Sample Lorem Ipsum Text', 'Aliquam suscipit, lacus a iaculis adipiscing, Sample Lorem Ipsum Text', 'Aliquam suscipit, lacus a iaculis adipiscing, Sample Lorem Ipsum Text', '<p><br></p>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671593686138.jpg', NULL, '', NULL, NULL, 1, 14, 3, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 13:37:51', '2019-09-11 05:35:06'),
(11, 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671593828091.jpg', NULL, '', NULL, NULL, 1, 16, 3, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 13:38:00', '2019-09-08 08:51:13'),
(12, 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', 'Suspendisse potenti. Vestibulum lacus Sample Lorem Ipsum Text', 'Suspendisse potenti. Vestibulum lacus Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671594009830.jpg', NULL, '', NULL, NULL, 1, 11, 3, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 13:38:09', '2019-09-02 20:01:33'),
(13, 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', 'Sample Lorem Ipsum Text', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-06', NULL, NULL, '15671595032043.jpg', NULL, NULL, NULL, NULL, 1, 13, 3, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 13:51:53', '2019-09-09 04:22:33');
INSERT INTO `smartend_topics` (`id`, `title_ar`, `title_en`, `title_jp`, `details_ar`, `details_en`, `details_jp`, `summary_ar`, `summary_en`, `summary_jp`, `bannertext_ar`, `bannertext_en`, `bannertext_jp`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_title_jp`, `seo_description_ar`, `seo_description_en`, `seo_description_jp`, `seo_keywords_ar`, `seo_keywords_en`, `seo_keywords_jp`, `seo_url_slug_ar`, `seo_url_slug_en`, `seo_url_slug_jp`, `tags`, `section_ids`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(14, 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', 'Sample Lorem Ipsum Text', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671595198137.jpg', NULL, NULL, NULL, NULL, 1, 11, 3, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 13:52:12', '2019-09-08 08:50:43'),
(15, '서명 합법 안내서 기술적으로, 법적으로 안전', 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', 'ESIGNATURE LEGALITY GUIDE技術的、法的安全', NULL, '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">The Korea Information Security Agency (KISA) is responsible for creating a secure environment for digital signatures and managing the certification authorities Korea Certification Authority Central (KCAC): The Korean Certification Authority Central (KCAC) of KISA issues certificates to intermediate CAs (licensed CAs = LCAs), which then issue end-entity certificates to Korean citizens, businesses, and other organizations. The LCAs are private organizations (not government organizations).</div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '14888170311535.jpg', NULL, '', NULL, NULL, 1, 13, 7, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:17:11', '2019-08-30 19:51:19'),
(16, '루시 뉴먼', 'Lucy Newman', 'ルーシー・ニューマン', NULL, 'Lorem ipsum dolor sit amet, no pro solum, duis platonem, quo eu omittam delicatissimi. Delicata voluptatum contentiones.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-27', NULL, NULL, '15669114615175.jpg', '15671524962165.png', '', NULL, NULL, 1, 2, 7, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:17:34', '2019-08-30 15:21:51'),
(18, '루시 뉴먼', 'Lucy Newman2', 'ルーシー・ニューマン', NULL, '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula.</div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-27', NULL, NULL, '15669116542709.jpg', '15671529874903.png', '', NULL, NULL, 1, 0, 7, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:17:54', '2019-08-30 17:19:38'),
(21, '루시 뉴먼', 'Lucy Newman3', 'ルーシー・ニューマン', NULL, 'Lorem ipsum dolor sit amet, no pro solum, duis platonem, quo eu omittam delicatissimi. Delicata voluptatum contentiones.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-27', NULL, NULL, '15669117638319.jpg', '15671531446787.png', NULL, NULL, NULL, 1, 4, 7, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:18:30', '2019-08-30 15:19:52'),
(22, '루시 뉴먼', 'Lucy Newman4', 'ルーシー・ニューマン', NULL, 'Lorem ipsum dolor sit amet, no pro solum, duis platonem, quo eu omittam delicatissimi. Delicata voluptatum contentiones.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-27', NULL, NULL, '15669118295314.jpg', '15671532503999.png', NULL, NULL, NULL, 1, 3, 7, 1, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 14:18:36', '2019-08-30 15:21:27'),
(50, 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', NULL, '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">The Korea Information Security Agency (KISA) is responsible for creating a secure environment for digital signatures and managing the certification authorities Korea Certification Authority Central (KCAC): The Korean Certification Authority Central (KCAC) of KISA issues certificates to intermediate CAs (licensed CAs = LCAs), which then issue end-entity certificates to Korean citizens, businesses, and other organizations. The LCAs are private organizations (not government organizations).</div></div>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 7, 0, 9, NULL, 'ESIGNATURE LEGALITY GUIDE Technically, legally Safe', NULL, '', '', NULL, NULL, NULL, NULL, '', 'esignature-legality-guide-technically-legally-safe', NULL, NULL, NULL, 1, 1, '2019-08-27 19:58:37', '2019-08-30 20:05:11'),
(56, 'Improve contract work efficiency and work style reform with our cooperation', 'English Title', 'Improve contract work efficiency and work style reform with our cooperation', '<p>  Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque</p>', '<p>English Detail    </p>', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, '<p>Summary English </p>', NULL, NULL, '<p>Banner Text English</p>', NULL, '2019-08-30', NULL, NULL, '15681531079765.png', '15681529433122.png', NULL, NULL, NULL, 1, 57, 7, 0, 10, 'new case study', 'new case study', 'new case study', 'this is a test case study', 'this is a test case study', 'this is a test case study', NULL, NULL, NULL, 'new-case-study', 'new-case-study', NULL, '3,4', '3,12,14', 1, 1, '2019-08-30 13:46:06', '2019-09-11 06:10:18'),
(57, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671611166074.jpg', NULL, NULL, NULL, NULL, 1, 20, 7, 0, 11, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', NULL, NULL, NULL, 'improve-contract-work-efficiency-and-work-style-reform-with-our-cooperation', 'improve-contract-work-efficiency-and-work-style-reform-with-our-cooperation', NULL, '3', '3,12,14', 1, 1, '2019-08-30 17:31:56', '2019-09-11 06:22:03'),
(58, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671612492056.jpg', NULL, NULL, NULL, NULL, 1, 18, 7, 0, 12, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', NULL, NULL, NULL, '', '', NULL, '3,4', '3', 1, 1, '2019-08-30 17:34:09', '2019-09-08 09:22:20'),
(59, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671615802334.jpg', NULL, NULL, NULL, NULL, 1, 10, 7, 0, 13, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', NULL, NULL, NULL, '', '', NULL, '4', '3', 1, 1, '2019-08-30 17:39:40', '2019-09-09 04:31:12'),
(60, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671616629908.jpg', NULL, NULL, NULL, NULL, 1, 10, 7, 0, 14, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', NULL, NULL, NULL, '', '', NULL, '4', '3', 1, 1, '2019-08-30 17:41:02', '2019-09-11 06:04:34'),
(61, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesque', NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-30', NULL, NULL, '15671617211825.jpg', NULL, NULL, NULL, NULL, 1, 13, 7, 0, 15, 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Improve contract work efficiency and work style reform with our cooperation', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', 'Lorem ipsum dolor sit amet, in appetere eleifend corrumpit nec, in vim bonorum ceteros maiestatis, case postulant an mea. Ea commune contentiones quo, possit omnesqu', NULL, NULL, NULL, '', '', NULL, '3,4', '3,14', 1, 1, '2019-08-30 17:42:01', '2019-09-10 06:23:38'),
(64, '한국어 타이틀', 'Title', '日本語タイトル', '<div dir=\"ltr\">한국어디테일</div><div dir=\"ltr\"><br></div>', '<div dir=\"ltr\">Detail&nbsp;</div>', '日本語出ている', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-10', NULL, NULL, '15681545383448.png', NULL, NULL, NULL, NULL, 1, 5, 3, 0, 7, '한국어 타이틀', 'Title', '日本語タイトル', '한국어디테일', 'Detail&nbsp;', '日本語出ている', NULL, NULL, NULL, '', 'title', NULL, NULL, NULL, 1, NULL, '2019-09-10 22:28:58', '2019-09-11 05:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_topic_categories`
--

CREATE TABLE `smartend_topic_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_topic_categories`
--

INSERT INTO `smartend_topic_categories` (`id`, `topic_id`, `section_id`, `created_at`, `updated_at`) VALUES
(99, 21, 5, '2019-08-30 15:19:52', '2019-08-30 15:19:52'),
(103, 22, 5, '2019-08-30 15:21:27', '2019-08-30 15:21:27'),
(104, 16, 5, '2019-08-30 15:21:51', '2019-08-30 15:21:51'),
(106, 15, 1, '2019-08-30 17:11:10', '2019-08-30 17:11:10'),
(107, 18, 5, '2019-08-30 17:19:38', '2019-08-30 17:19:38'),
(116, 50, 1, '2019-08-30 20:05:11', '2019-08-30 20:05:11'),
(135, 66, 3, '2019-09-04 16:11:01', '2019-09-04 16:11:01'),
(136, 66, 12, '2019-09-04 16:11:01', '2019-09-04 16:11:01'),
(137, 67, 3, '2019-09-04 16:13:06', '2019-09-04 16:13:06'),
(138, 67, 12, '2019-09-04 16:13:06', '2019-09-04 16:13:06'),
(153, 57, 3, '2019-09-04 18:01:32', '2019-09-04 18:01:32'),
(154, 57, 12, '2019-09-04 18:01:32', '2019-09-04 18:01:32'),
(155, 57, 14, '2019-09-04 18:01:32', '2019-09-04 18:01:32'),
(156, 58, 3, '2019-09-04 18:01:51', '2019-09-04 18:01:51'),
(157, 59, 3, '2019-09-04 18:02:10', '2019-09-04 18:02:10'),
(158, 60, 3, '2019-09-04 18:02:27', '2019-09-04 18:02:27'),
(170, 8, 11, '2019-09-09 06:24:17', '2019-09-09 06:24:17'),
(174, 7, 10, '2019-09-09 06:33:34', '2019-09-09 06:33:34'),
(181, 61, 3, '2019-09-10 06:23:38', '2019-09-10 06:23:38'),
(182, 61, 14, '2019-09-10 06:23:38', '2019-09-10 06:23:38'),
(183, 6, 9, '2019-09-10 07:38:35', '2019-09-10 07:38:35'),
(187, 56, 3, '2019-09-10 22:05:07', '2019-09-10 22:05:07'),
(188, 56, 12, '2019-09-10 22:05:07', '2019-09-10 22:05:07'),
(189, 56, 14, '2019-09-10 22:05:07', '2019-09-10 22:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_topic_fields`
--

CREATE TABLE `smartend_topic_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_users`
--

CREATE TABLE `smartend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `connect_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connect_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_users`
--

INSERT INTO `smartend_users` (`id`, `name`, `email`, `password`, `photo`, `permissions_id`, `status`, `connect_email`, `connect_password`, `provider`, `provider_id`, `access_token`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@site.com', '$2y$10$ufyoSSbqKIEnMr5OXpD3kONpii8iV6Zypsged3OqcvlY3fNNmt.4K', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, '4u0OgCClqtqerU1LRDK6gYLe6h3PGIkaVAJ5ECZkd5ayggN5bJxlWa6TjEEQ', 1, 1, '2018-04-21 22:02:49', '2019-09-04 14:03:10'),
(12, 'raman', 'raman.977@gmail.com', '$2y$10$ufyoSSbqKIEnMr5OXpD3kONpii8iV6Zypsged3OqcvlY3fNNmt.4K', NULL, 3, 1, NULL, NULL, NULL, NULL, NULL, 'veEtVgwiInsj5z2Lign1P2e7zsZl1mdn704HIJj7NSvLewWdg1rocaNgbEAx', NULL, 1, '2019-09-11 07:00:00', '2019-09-04 14:17:12'),
(13, 'Vijay Kumar', 'vijay@gmail.com', '$2y$10$uoHCpXzS60hs2DJcfpM0n.aUwgbG9GWPCMMIbEQmWIK672AktDs/y', NULL, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2019-09-09 04:58:40', '2019-09-09 04:58:40'),
(14, 'tester', 'tester@gmail.com', '$2y$10$.CQ0pkI9Iz229YkP8YQOAO7T2y7QM5nKlr5CqKhPKMWJ/wcxpeOtu', NULL, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '2019-09-09 06:56:16', '2019-09-09 06:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmails`
--

CREATE TABLE `smartend_webmails` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci,
  `date` datetime NOT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_webmails`
--

INSERT INTO `smartend_webmails` (`id`, `cat_id`, `group_id`, `contact_id`, `father_id`, `title`, `details`, `date`, `from_email`, `from_name`, `from_phone`, `to_email`, `to_name`, `to_cc`, `to_bcc`, `status`, `flag`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, 2, NULL, NULL, 'ORDER , Qty=12, Nullam mollis dolor', 'dfdfd', '2017-03-07 15:21:20', 'eng_m_mondy@hotmail.com', 'mohamed mondi', '01221485486', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 13:21:20', '2017-11-09 19:21:07'),
(2, 0, NULL, NULL, NULL, 'Need your help', 'Dear sir,\r\nI need your help to subscribe to your team. Please contact me as soon as possible.\r\n\r\nBest Regards', '2017-03-07 16:04:16', 'ayamen@site.com', 'Amar Yamen', '8378-475-466', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:04:16', '2017-11-09 19:21:07'),
(3, 0, 3, NULL, NULL, 'My test message to this site', 'I just test sending messages\r\nThanks', '2017-03-07 16:05:32', 'email@site.com', 'Donyo Hawa', '343423-543', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:05:32', '2017-11-09 19:21:07'),
(4, 0, 1, NULL, NULL, 'Contact me for support any time', 'This is a test message', '2017-03-07 16:10:29', 'email@site.com', 'MMondi', '7363758', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:10:29', '2017-11-09 19:21:07'),
(5, 0, NULL, NULL, NULL, 'Test mail delivery message', 'Dear team,\r\nThis is a Test mail delivery message\r\nThank you', '2017-03-07 21:06:41', 'email@site.com', 'Ramy Adams', '87557home', 'support@smartfordesign.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-08 02:06:41', '2017-11-09 19:21:07'),
(6, 0, NULL, NULL, NULL, 'Test mail delivery message', 'Dear team,\r\nThis is a Test mail delivery message\r\nThank you', '2017-03-07 21:08:54', 'email@site.com', 'Adam Ali', '3432423', 'support@smartfordesign.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-08 02:08:54', '2017-11-09 19:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmails_files`
--

CREATE TABLE `smartend_webmails_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `webmail_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmails_groups`
--

CREATE TABLE `smartend_webmails_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_webmails_groups`
--

INSERT INTO `smartend_webmails_groups` (`id`, `name`, `color`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Support', '# 00bcd4', 1, NULL, '2017-03-07 14:10:58', '2017-03-07 14:10:58'),
(2, 'Orders', '#f44336', 1, NULL, '2017-03-07 14:11:04', '2017-03-07 14:11:04'),
(3, 'Queries', '#8bc34a', 1, NULL, '2017-03-07 14:11:37', '2017-03-07 14:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmaster_banners`
--

CREATE TABLE `smartend_webmaster_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `desc_status` tinyint(4) NOT NULL,
  `link_status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_webmaster_banners`
--

INSERT INTO `smartend_webmaster_banners` (`id`, `row_no`, `name`, `width`, `height`, `desc_status`, `link_status`, `type`, `icon_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'homeBanners', 1600, 500, 1, 1, 1, 0, 1, 1, 1, '2017-11-08 13:20:40', '2017-11-12 04:55:02'),
(2, 2, 'textBanners', 330, 330, 1, 1, 0, 1, 1, 1, NULL, '2017-11-08 13:20:40', '2017-11-08 13:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmaster_sections`
--

CREATE TABLE `smartend_webmaster_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `sections_status` tinyint(4) NOT NULL,
  `comments_status` tinyint(4) NOT NULL,
  `date_status` tinyint(4) NOT NULL,
  `expire_date_status` tinyint(4) NOT NULL,
  `longtext_status` tinyint(4) NOT NULL,
  `editor_status` tinyint(4) NOT NULL,
  `attach_file_status` tinyint(4) NOT NULL,
  `extra_attach_file_status` tinyint(4) NOT NULL,
  `multi_images_status` tinyint(4) NOT NULL,
  `section_icon_status` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `maps_status` tinyint(4) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `related_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_webmaster_sections`
--

INSERT INTO `smartend_webmaster_sections` (`id`, `row_no`, `name`, `type`, `sections_status`, `comments_status`, `date_status`, `expire_date_status`, `longtext_status`, `editor_status`, `attach_file_status`, `extra_attach_file_status`, `multi_images_status`, `section_icon_status`, `icon_status`, `maps_status`, `order_status`, `related_status`, `status`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'sitePages', 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-11-08 13:20:40', '2019-09-04 13:53:14'),
(2, 2, 'Country', 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-11-08 13:20:40', '2019-09-04 11:22:30'),
(3, 3, 'news', 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-11-08 13:20:40', '2019-09-04 14:53:50'),
(7, 7, 'blog', 0, 2, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-11-08 13:20:40', '2019-09-02 11:53:51');

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmaster_section_fields`
--

CREATE TABLE `smartend_webmaster_section_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `row_no` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `required` tinyint(4) NOT NULL,
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `smartend_webmaster_settings`
--

CREATE TABLE `smartend_webmaster_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_box_status` tinyint(4) NOT NULL,
  `en_box_status` tinyint(4) NOT NULL,
  `jp_box_status` tinyint(4) DEFAULT NULL,
  `seo_status` tinyint(4) NOT NULL,
  `analytics_status` tinyint(4) NOT NULL,
  `banners_status` tinyint(4) NOT NULL,
  `inbox_status` tinyint(4) NOT NULL,
  `calendar_status` tinyint(4) NOT NULL,
  `settings_status` tinyint(4) NOT NULL,
  `newsletter_status` tinyint(4) NOT NULL,
  `members_status` tinyint(4) NOT NULL,
  `orders_status` tinyint(4) NOT NULL,
  `shop_status` tinyint(4) NOT NULL,
  `shop_settings_status` tinyint(4) NOT NULL,
  `default_currency_id` int(11) NOT NULL,
  `languages_ar_status` tinyint(4) NOT NULL,
  `languages_en_status` tinyint(4) NOT NULL,
  `languages_jp_status` tinyint(4) DEFAULT NULL,
  `languages_by_default` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latest_news_section_id` int(11) NOT NULL,
  `header_menu_id` int(11) NOT NULL,
  `footer_menu_id` int(11) NOT NULL,
  `home_banners_section_id` int(11) NOT NULL,
  `home_text_banners_section_id` int(11) NOT NULL,
  `side_banners_section_id` int(11) NOT NULL,
  `contact_page_id` int(11) NOT NULL,
  `newsletter_contacts_group` int(11) NOT NULL,
  `new_comments_status` tinyint(4) NOT NULL,
  `links_status` tinyint(4) NOT NULL,
  `register_status` tinyint(4) NOT NULL,
  `permission_group` int(11) NOT NULL,
  `api_status` tinyint(4) NOT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_content1_section_id` int(11) NOT NULL,
  `home_content2_section_id` int(11) NOT NULL,
  `home_content3_section_id` int(11) NOT NULL,
  `home_contents_per_page` int(11) NOT NULL,
  `mail_driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_host` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_port` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_encryption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_no_replay` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nocaptcha_status` tinyint(4) NOT NULL,
  `nocaptcha_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nocaptcha_sitekey` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_tags_status` tinyint(4) NOT NULL,
  `google_tags_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_analytics_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_facebook_status` tinyint(4) NOT NULL,
  `login_facebook_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_facebook_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_twitter_status` tinyint(4) NOT NULL,
  `login_twitter_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_twitter_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_google_status` tinyint(4) NOT NULL,
  `login_google_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_google_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_linkedin_status` tinyint(4) NOT NULL,
  `login_linkedin_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_linkedin_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_github_status` tinyint(4) NOT NULL,
  `login_github_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_github_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_bitbucket_status` tinyint(4) NOT NULL,
  `login_bitbucket_client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_bitbucket_client_secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dashboard_link_status` tinyint(4) NOT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smartend_webmaster_settings`
--

INSERT INTO `smartend_webmaster_settings` (`id`, `ar_box_status`, `en_box_status`, `jp_box_status`, `seo_status`, `analytics_status`, `banners_status`, `inbox_status`, `calendar_status`, `settings_status`, `newsletter_status`, `members_status`, `orders_status`, `shop_status`, `shop_settings_status`, `default_currency_id`, `languages_ar_status`, `languages_en_status`, `languages_jp_status`, `languages_by_default`, `latest_news_section_id`, `header_menu_id`, `footer_menu_id`, `home_banners_section_id`, `home_text_banners_section_id`, `side_banners_section_id`, `contact_page_id`, `newsletter_contacts_group`, `new_comments_status`, `links_status`, `register_status`, `permission_group`, `api_status`, `api_key`, `home_content1_section_id`, `home_content2_section_id`, `home_content3_section_id`, `home_contents_per_page`, `mail_driver`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_encryption`, `mail_no_replay`, `nocaptcha_status`, `nocaptcha_secret`, `nocaptcha_sitekey`, `google_tags_status`, `google_tags_id`, `google_analytics_code`, `login_facebook_status`, `login_facebook_client_id`, `login_facebook_client_secret`, `login_twitter_status`, `login_twitter_client_id`, `login_twitter_client_secret`, `login_google_status`, `login_google_client_id`, `login_google_client_secret`, `login_linkedin_status`, `login_linkedin_client_id`, `login_linkedin_client_secret`, `login_github_status`, `login_github_client_id`, `login_github_client_secret`, `login_bitbucket_status`, `login_bitbucket_client_id`, `login_bitbucket_client_secret`, `dashboard_link_status`, `timezone`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 'jp', 3, 1, 2, 1, 2, 0, 0, 1, 1, 1, 0, 3, 0, '80437949309442', 7, 0, 0, 20, 'smtp', '', '', '', '', '', 'noreplay@site.com', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 1, 'UTC', 1, 1, '2018-04-21 22:02:49', '2019-09-10 13:13:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `smartend_analytics_pages`
--
ALTER TABLE `smartend_analytics_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_analytics_visitors`
--
ALTER TABLE `smartend_analytics_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_attach_files`
--
ALTER TABLE `smartend_attach_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_banners`
--
ALTER TABLE `smartend_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_comments`
--
ALTER TABLE `smartend_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_contacts`
--
ALTER TABLE `smartend_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_contacts_groups`
--
ALTER TABLE `smartend_contacts_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_countries`
--
ALTER TABLE `smartend_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_events`
--
ALTER TABLE `smartend_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_ltm_translations`
--
ALTER TABLE `smartend_ltm_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_maps`
--
ALTER TABLE `smartend_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_menus`
--
ALTER TABLE `smartend_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_migrations`
--
ALTER TABLE `smartend_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_password_resets`
--
ALTER TABLE `smartend_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `smartend_payments`
--
ALTER TABLE `smartend_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_permissions`
--
ALTER TABLE `smartend_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_photos`
--
ALTER TABLE `smartend_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_related_topics`
--
ALTER TABLE `smartend_related_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_sections`
--
ALTER TABLE `smartend_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_settings`
--
ALTER TABLE `smartend_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_tags`
--
ALTER TABLE `smartend_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_topics`
--
ALTER TABLE `smartend_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_topic_categories`
--
ALTER TABLE `smartend_topic_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_topic_fields`
--
ALTER TABLE `smartend_topic_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_users`
--
ALTER TABLE `smartend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `smartend_webmails`
--
ALTER TABLE `smartend_webmails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmails_files`
--
ALTER TABLE `smartend_webmails_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmails_groups`
--
ALTER TABLE `smartend_webmails_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmaster_banners`
--
ALTER TABLE `smartend_webmaster_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmaster_sections`
--
ALTER TABLE `smartend_webmaster_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmaster_section_fields`
--
ALTER TABLE `smartend_webmaster_section_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smartend_webmaster_settings`
--
ALTER TABLE `smartend_webmaster_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `smartend_analytics_pages`
--
ALTER TABLE `smartend_analytics_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=791;
--
-- AUTO_INCREMENT for table `smartend_analytics_visitors`
--
ALTER TABLE `smartend_analytics_visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `smartend_attach_files`
--
ALTER TABLE `smartend_attach_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_banners`
--
ALTER TABLE `smartend_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `smartend_comments`
--
ALTER TABLE `smartend_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `smartend_contacts`
--
ALTER TABLE `smartend_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `smartend_contacts_groups`
--
ALTER TABLE `smartend_contacts_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `smartend_countries`
--
ALTER TABLE `smartend_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT for table `smartend_events`
--
ALTER TABLE `smartend_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `smartend_ltm_translations`
--
ALTER TABLE `smartend_ltm_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_maps`
--
ALTER TABLE `smartend_maps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_menus`
--
ALTER TABLE `smartend_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `smartend_migrations`
--
ALTER TABLE `smartend_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `smartend_payments`
--
ALTER TABLE `smartend_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `smartend_permissions`
--
ALTER TABLE `smartend_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `smartend_photos`
--
ALTER TABLE `smartend_photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `smartend_related_topics`
--
ALTER TABLE `smartend_related_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_sections`
--
ALTER TABLE `smartend_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `smartend_settings`
--
ALTER TABLE `smartend_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `smartend_tags`
--
ALTER TABLE `smartend_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `smartend_topics`
--
ALTER TABLE `smartend_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `smartend_topic_categories`
--
ALTER TABLE `smartend_topic_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `smartend_topic_fields`
--
ALTER TABLE `smartend_topic_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_users`
--
ALTER TABLE `smartend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `smartend_webmails`
--
ALTER TABLE `smartend_webmails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `smartend_webmails_files`
--
ALTER TABLE `smartend_webmails_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_webmails_groups`
--
ALTER TABLE `smartend_webmails_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `smartend_webmaster_banners`
--
ALTER TABLE `smartend_webmaster_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `smartend_webmaster_sections`
--
ALTER TABLE `smartend_webmaster_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `smartend_webmaster_section_fields`
--
ALTER TABLE `smartend_webmaster_section_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `smartend_webmaster_settings`
--
ALTER TABLE `smartend_webmaster_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
